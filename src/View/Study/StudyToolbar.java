package View.Study;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;




import EventHandler.ExperimentFrameToolBarListener;
import EventHandler.StudyFrameToolBarListener;


public class StudyToolbar extends JToolBar{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int width = 800;
	private int height = 32;
	
	private StudyFrameToolBarListener listener;
	private StudyImagePanel imagePanel;
	
	private JToggleButton btnShowGazes;
	private JToggleButton btnShowFixations;
	private JToggleButton btnShowSaccades;
	private JToggleButton btnShowAois;
	
	public StudyToolbar(StudyImagePanel panel){
		
		setFloatable(false);
		this.setSize(width, height);
		listener = new StudyFrameToolBarListener(this);
		this.imagePanel = panel;
		
		btnShowGazes = new JToggleButton("");
		btnShowGazes.setToolTipText("Display Gazes");
		btnShowGazes.setIcon(new ImageIcon("C:\\Uni\\Hiwi\\SAFT\\images\\showGazes.png"));
		add(btnShowGazes);
		btnShowGazes.setActionCommand("Show Gazes");
		btnShowGazes.addActionListener(listener);
		
		btnShowFixations = new JToggleButton("");
		btnShowFixations.setToolTipText("Display Fixations");
		btnShowFixations.setIcon(new ImageIcon("C:\\Uni\\Hiwi\\SAFT\\images\\showFixations.png"));
		add(btnShowFixations);
		btnShowFixations.setActionCommand("Show Fixations");
		btnShowFixations.addActionListener(listener);
		
		btnShowSaccades = new JToggleButton("");
		btnShowSaccades.setSelectedIcon(new ImageIcon("C:\\Uni\\Hiwi\\SAFT\\images\\showSaccades.png"));
		btnShowSaccades.setToolTipText("Display Saccades");
		btnShowSaccades.setIcon(new ImageIcon("C:\\Uni\\Hiwi\\SAFT\\images\\showSaccades.png"));
		add(btnShowSaccades);
		btnShowSaccades.setActionCommand("Show Saccades");
		btnShowSaccades.addActionListener(listener);
		
		btnShowAois = new JToggleButton("");
		btnShowAois.setIcon(new ImageIcon("C:\\Uni\\Hiwi\\SAFT\\images\\showAois.png"));
		btnShowAois.setSelectedIcon(new ImageIcon("C:\\Uni\\Hiwi\\SAFT\\images\\showAois.png"));
		btnShowAois.setActionCommand("Show Aois");
		btnShowAois.setToolTipText("Display AOIs");
		add(btnShowAois);
		btnShowAois.addActionListener(listener);
		
		
	}
	
	public void setEyeDataVisibility() {
		
		imagePanel.setEyeDataVisibility(btnShowGazes.isSelected(), btnShowFixations.isSelected(), btnShowSaccades.isSelected());
		
	}
	
	public void setAoiVisibility(){
		
		imagePanel.setAoiVisibility(btnShowAois.isSelected());
	}
	
}
