package View.Study;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JPanel;

import Data.AreaOfInterest;
import Data.Study.Study;
import Data.Subject.Experiment;
import Data.Subject.Screen;
import Data.Subject.Trial;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.Saccade;
import View.Experiment.ExperimentFrame;
import View.Experiment.ExperimentImagePanel;
import View.Experiment.ImageLayer;

public class StudyImagePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public float scaleX = 1.0f;
	public float scaleY = 1.0f;
	
	private ImageLayer background;
	private ImageLayer heatMap;
	private ImageLayer cluster;
	private ImageLayer aoi;

	private boolean showGazes = false;
	private boolean showFixations = false;
	private boolean showSaccades = false;
	private boolean showAois = false;
	
	private StudyFrame frame;
	private ArrayList<Experiment> experiments = new ArrayList<Experiment>();
	private int screen = 0;
	private int trial = 0;
	private ArrayList<Trial> studyTrials;
	
	
	public StudyImagePanel(StudyFrame frame){
		
		setBackground(Color.WHITE);
		this.frame = frame;
		
		
		
		// create mouse listener
		this.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				
				scaleX = ((float) StudyImagePanel.this.getWidth()/(float) (background.getUnscaled().getWidth()));
				scaleY = ((float) StudyImagePanel.this.getHeight()/(float) (background.getUnscaled().getHeight()));
				repaint();
				
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void nextTrial(){
		
		if (trial+1 < studyTrials.size()){
			
			trial++;
			try {
				loadScreen(studyTrials.get(trial).getScreens().get(screen).getFilename());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (showAois)
			drawAoi();
		
	}
	
	public void nextScreen(){
		
		if (screen+1 < studyTrials.get(trial).getScreens().size()){
			
			screen++;
			try {
				loadScreen(studyTrials.get(trial).getScreens().get(screen).getFilename());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (showAois)
			drawAoi();
	}
	
	public void previousTrial(){
		
		if (trial-1 >= 0){
			
			trial--;
			try {
				loadScreen(studyTrials.get(trial).getScreens().get(screen).getFilename());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (showAois)
			drawAoi();
	}
	
	public void previousScreen(){
		
		if (screen-1 >=0){
			
			screen++;
			
			try {
				loadScreen(studyTrials.get(trial).getScreens().get(screen).getFilename());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (showAois)
			drawAoi();
	}
	
	
	public void setEyeDataVisibility(boolean gazes, boolean fix, boolean sac){
		
		showFixations = fix;
		showGazes = gazes;
		showSaccades = sac;
		
		// redraw eyedata layer
		repaint();
	}
	
	public void setAoiVisibility(boolean selected) {
		
		showAois = selected;
		aoi.enableLayer(selected);
		if (showAois)
			drawAoi();
		
		repaint();
	}
	
	public void drawAoi(){
		
		BufferedImage newImage = new BufferedImage(aoi.getWidth(), aoi.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = newImage.createGraphics();
		
		Screen currentScreen = studyTrials.get(trial).getScreens().get(screen);
		
		for (AreaOfInterest a : currentScreen.getAois())
			a.draw(g2d, scaleX, scaleY);
		
		
		aoi.setBufferedImage(newImage);
		aoi.resizeLayer(this.getWidth(), this.getHeight());	
	}
	
	public void paintComponent(Graphics g){
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		try {
	    	g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                RenderingHints.VALUE_ANTIALIAS_ON);
	    	g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	    			RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	       
	    	
	    	if (background != null){
	    		
	    		g2d.drawImage(background.getScaled(), 0, 0, this.getWidth(), this.getHeight(), null);
	    		
	    		// which layers should be drawn on the background layer?
	    		if (experiments.size() > 0){
	    			drawEyeMovementData(g2d);
	    		}
				if(showAois){
					g2d.drawImage(aoi.getScaled(), 0, 0, this.getWidth(),  this.getHeight(), null);
				}
				
	    		
	    		//if(showSelectionRectangle)
					//drawSelectionRectangle(g2d);
	    		
	    	}
	    	
	    } finally {
	        g2d.dispose();
	    }
	}
	
	public void initLayers(int experimentHeight, int experimentWidth) throws FileNotFoundException, IOException{
		
		Screen start = studyTrials.get(0).getScreens().get(0);
		background = new ImageLayer(experimentWidth, experimentHeight, start.getFilename());
		cluster = new ImageLayer(experimentWidth, experimentHeight, BufferedImage.TYPE_INT_ARGB, false);
		heatMap = new ImageLayer(experimentWidth, experimentHeight, BufferedImage.TYPE_INT_ARGB, false);
		aoi = new ImageLayer(experimentWidth, experimentHeight, BufferedImage.TYPE_INT_ARGB, true);
		scaleX = ((float) this.getWidth()/(float) (background.getUnscaled().getWidth()));
		scaleY = ((float) this.getHeight()/(float) (background.getUnscaled().getHeight()));
		repaint();
	}

	private void loadScreen(String path) throws FileNotFoundException, IOException{
		
		background.loadImage(path);
		repaint();
	}
	
	
	public void drawEyeMovementData(Graphics2D g2d) {
		
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
    			RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
    			RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		
		for (Experiment ex : experiments){
		
			Screen currentScreen = ex.getTrials().get(trial).getScreens().get(screen);
						
			if (showFixations && currentScreen.getFixations().size() > 0){	
				
				Fixation old = null;
				int fixCounter = 0;
				for (Fixation i : currentScreen.getFixations()){
							
					i.draw(g2d, scaleX, scaleY);
					
					// draw scanpath
					if (fixCounter > 0){
							
						g2d.drawLine((int) (old.getX()*scaleX ),
									 (int) (old.getY()*scaleY ),
									 (int) (i.getX()*scaleX ),
									 (int) (i.getY()*scaleY));
					}
					old = i;
					++fixCounter;
				}
			}
				
			// draw saccades	
			if (showSaccades && currentScreen.getSaccades().size() > 0){
				
				for (Saccade s : currentScreen.getSaccades()){
					s.draw(g2d, scaleX, scaleY);
				}
			}
			// draw gazes		
			if (showGazes && currentScreen.getGazes().size() > 0){
				
				for (Gaze g : currentScreen.getGazes()){	
						g.draw(g2d, scaleX, scaleY);
				}
			}
			
			
		}
	}

	public void updateActiveExperiments(ArrayList<Experiment> activeExperiments) {
		experiments = activeExperiments;
		repaint();
	}
	
	public void initExperiments(Study study){
		
		/*
		this.studyTrials = study.getStudyTrials();
		study.getExperiments().stream().filter(exp -> exp.isEnabled())
							  .forEach(exp -> experiments.add(exp));
							  */
	}
	
}
