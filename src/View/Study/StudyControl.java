package View.Study;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import EventHandler.StudyControlListener;


public class StudyControl extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int width = 800;
	private int height = 32;
	
	public StudyControl(StudyImagePanel imagePanel){
		
		StudyControlListener listener = new StudyControlListener(imagePanel, this);
		this.setSize(width, height);
	    setLayout(null);
	    
	    JButton btnNextTrial = new JButton();
	    btnNextTrial.setToolTipText("Go To Next Trial");
	    btnNextTrial.setIcon(new ImageIcon("C:\\Uni\\Hiwi\\SAFT\\images\\forward_green.png"));
	    btnNextTrial.setBounds(130, 1, 29, 29);
	    btnNextTrial.setBackground(Color.white);
	    btnNextTrial.setActionCommand("next trial");
	    this.add(btnNextTrial);
	    btnNextTrial.addActionListener(listener);
	    
	    JButton btnBackTrial = new JButton();
	    btnBackTrial.setToolTipText("Go To Previous Trial");
	    btnBackTrial.setBackground(Color.white);
	    btnBackTrial.setIcon(new ImageIcon("C:\\Uni\\Hiwi\\SAFT\\images\\back_green.png"));
	    btnBackTrial.setActionCommand("previous trial");
	    btnBackTrial.setBounds(10, 1, 29, 29);
	    this.add(btnBackTrial);
	    btnBackTrial.addActionListener(listener);
	    
	    JButton btnNextScreen = new JButton("");
	    btnNextScreen.setToolTipText("Go To Next Screen");
	    btnNextScreen.setIcon(new ImageIcon("C:\\Uni\\Hiwi\\SAFT\\images\\forward_blue.png"));
	    btnNextScreen.setBounds(90, 1, 29, 29);
	    btnNextScreen.setActionCommand("next screen");
	    btnNextScreen.addActionListener(listener);
	    add(btnNextScreen);
	    
	    JButton btnBackScreen = new JButton("");
	    btnBackScreen.setToolTipText("Go To Previous Screen");
	    btnBackScreen.setIcon(new ImageIcon("C:\\Uni\\Hiwi\\SAFT\\images\\back_blue.png"));
	    btnBackScreen.setBounds(50, 1, 29, 29);
	    btnBackScreen.setActionCommand("previous screen");
	    btnBackScreen.addActionListener(listener);
	    add(btnBackScreen);
		
		
	}
	
	
	public void resize(int y, int width, int height){
		
		this.setBounds(0,y,width, height);
		
	}
	
}
