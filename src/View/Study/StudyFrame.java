package View.Study;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyVetoException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import View.Experiment.ExperimentControl;
import View.Experiment.ExperimentToolbar;
import Data.Study.Study;
import Data.Subject.Experiment;

public class StudyFrame extends JInternalFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static StudyFrame instance = new StudyFrame();
	private StudyImagePanel imagePanel;
	private StudyToolbar toolBar;
	private StudyControl controls;
	
	
	public static StudyFrame getInstance(){
		return instance;
	}
	
	private StudyFrame(){
		
		super("Study ",
		          true, //resizable
		          false, //closable
		          true, //maximizable
		          true);//iconifiable
		
		 
		this.setSize(new Dimension(800, 600));
		setLocation(10,10);
		getContentPane().setLayout(null);    
		
		this.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				
				resizeElements();
				
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		imagePanel = new StudyImagePanel(this);
		toolBar = new StudyToolbar(imagePanel);
		controls = new StudyControl(imagePanel);
	}
	
	public void initExperiments(Study study){
		imagePanel.initExperiments(study);
	}
	
	public void resizeElements(){
		
		controls.resize(this.getContentPane().getHeight()-toolBar.getHeight(),this.getContentPane().getWidth(), toolBar.getHeight());
		imagePanel.setBounds(0, toolBar.getHeight(), this.getContentPane().getWidth(), this.getContentPane().getHeight()-toolBar.getHeight()-controls.getHeight());
	}
	
	
	public void initImagePanel(int experimentHeight, int experimentWidth) throws FileNotFoundException, IOException{
		imagePanel.initLayers(experimentHeight,experimentWidth);
	}
	
	
	public void display(Study study, int maxWidth, int maxHeight) throws FileNotFoundException, IOException{
		
		this.setMaximumSize(new Dimension(maxWidth,maxHeight));
		
		
		getContentPane().add(toolBar);
		toolBar.setBounds(0,0,toolBar.getWidth(), toolBar.getHeight());
		getContentPane().add(controls);
		controls.setBounds(0,this.getContentPane().getHeight()-toolBar.getHeight(),toolBar.getWidth(), toolBar.getHeight());
		getContentPane().add(imagePanel);
		imagePanel.setBounds(0, toolBar.getHeight(), this.getContentPane().getWidth(), this.getContentPane().getHeight()-toolBar.getHeight()-controls.getHeight());
		this.setMinimumSize(new Dimension(toolBar.getWidth(),600));
		
		//initImagePanel(study.getExperiments().get(0).getScreenHeight(), study.getExperiments().get(0).getScreenWidth());
		
		
		 try {
				setSelected(true);
			} catch (PropertyVetoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 moveToFront(); 
		 this.setVisible(true);
	}

	public void updateExperiments(ArrayList<Experiment> activeExperiments) {
		
		if (imagePanel != null){
			imagePanel.updateActiveExperiments(activeExperiments);
		}
	}
	

}
