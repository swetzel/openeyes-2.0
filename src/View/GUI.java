package View;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.KeyboardFocusManager;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import View.Experiment.CLView;
import View.Experiment.ExperimentFrame;
import View.Experiment.PopUpExperimentFrame;
import View.Menu.ExperimentPanel;
import View.Menu.Menu;
import View.Menu.PopUpMenu;
import View.Menu.SubjectPanel;
import View.Study.StudyFrame;
import Analysis.DBScan;
import Analysis.FixationDetection;
import Analysis.HeatMap;
import Analysis.KMeans;
import Data.AreaOfInterest;
import Data.BoundingBox;
import Data.Cluster;
import Data.Study.Study;
import Data.Subject.Experiment;
import Data.Subject.Screen;
import Data.Subject.Subject;
import Data.Subject.Trial;
import EventHandler.MenuActionListener;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.Saccade;

import java.awt.Color;
import java.beans.PropertyVetoException;

/**
 * 
 * @author Stefanie Wetzel <stefanie.wetzel@gmail.com>
 *
 */
public class GUI extends JFrame{
	
	private static GUI instance = new GUI();

	private static final long serialVersionUID = 1L;
	private JMenuBar menuBar;
	private JMenu menuFile;
	private JMenu menuHelp;
	private JMenuItem loadStudyELearning;
	private JMenuItem loadStudyBTF;
	private JMenuItem loadStudyPC;
	private JMenuItem loadStudyHex;
	private JMenuItem helpButton;
	private JDesktopPane desktop;
	private ArrayList<ExperimentFrame> experimentFrames = new ArrayList<ExperimentFrame>();
	
	public Experiment experiment;
	public Study study;
	public boolean experimentMode = true;
	public int screenNumber = 0;
	public int trialNumber = 0;

	public Mode mode = null;
	
	//private SAFTActionListener actionListener = new SAFTActionListener();
	public final static JFileChooser fc = new JFileChooser();

	public DBScan dbScan;
	public KMeans kMeans;
	public ClusterMethod clusterMethod = ClusterMethod.KMEANS;
	
	public AreaOfInterest selfDefinedAreaOfInterest;
	
	private enum Mode{
		STUDY,
		EXPERIMENT,
	}
	
	public enum ClusterMethod{
		KMEANS,
		DBSCAN,
	}

	public static GUI getInstance() {
        return instance;
    }
	
	private GUI(){
		
		getContentPane().setBackground(Color.WHITE);
		getContentPane().setForeground(Color.WHITE);

		this.setTitle("OpenEyes - Eye Data Analyser");
		
		menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		
		menuFile = new JMenu("File");
		menuBar.add(menuFile);

		loadStudyELearning = new JMenuItem("Load eLearning Study");
		loadStudyELearning.setActionCommand("load elearning");
		menuFile.add(loadStudyELearning);
		
		loadStudyBTF = new JMenuItem("Load BTF Study");
		loadStudyBTF.setActionCommand("load btf");
		menuFile.add(loadStudyBTF);
		
		loadStudyPC = new JMenuItem("Load PC Study");
		loadStudyPC.setActionCommand("load pc");
		menuFile.add(loadStudyPC);
		
		loadStudyHex = new JMenuItem("Load Hex Study");
		loadStudyHex.setActionCommand("load hex");
		menuFile.add(loadStudyHex);
		
		menuHelp = new JMenu("Help");
		//menuBar.add(menuHelp);
		
		helpButton = new JMenuItem("How to");
		///menuHelp.add(helpButton);
		
		//this.getContentPane().setPreferredSize(new Dimension(1000, 700));
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		desktop = new JDesktopPane();
		desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		desktop.setBackground(Color.LIGHT_GRAY);
		desktop.add(StudyFrame.getInstance());
			
		getContentPane().setLayout(null);
		getContentPane().add(desktop);
		getContentPane().add(Menu.getInstance());
		this.setMinimumSize(new Dimension(1000, 600));
	       
		loadStudyELearning.addActionListener(MenuActionListener.getInstance());
		loadStudyBTF.addActionListener(MenuActionListener.getInstance());
		loadStudyPC.addActionListener(MenuActionListener.getInstance());
		loadStudyHex.addActionListener(MenuActionListener.getInstance());
		
		
		this.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				
				Menu.getInstance().resize(GUI.this.getContentPane().getWidth(), GUI.this.getContentPane().getHeight());
				desktop.setMaximumSize(new Dimension(GUI.this.getContentPane().getWidth()-Menu.getInstance().getWidth(), GUI.this.getContentPane().getHeight()));
				desktop.setSize(GUI.this.getContentPane().getWidth()-Menu.getInstance().getWidth(), GUI.this.getContentPane().getHeight());
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
	}
	
	public Study getStudy(){
		return study;
	}
	
	/**
	 * Creates icons for all subjects and experiments in the menu bar.
	 */
	public void displayExperiments(){
		
		Menu menu = Menu.getInstance();
		menu.clearPanels();
		
		int subjectPanelHeight = 0;
		
		for (Subject sub: study.getSubjects()){
					
			SubjectPanel newSubject = new SubjectPanel(sub);	
			//subjectPanelHeight = 0;
			
			int experimentOffset = newSubject.getOffset();
			//System.out.println("experiments "+sub.getExperiments().size());
			for (Experiment ex: sub.getExperiments()){
				
				ExperimentPanel newExperiment = new ExperimentPanel(ex);
				newExperiment.setComponentPopupMenu(new PopUpMenu(sub.getId(),ex.getId(), study.useLoad()));
				newExperiment.setLocation(2, experimentOffset);
				newSubject.addExperimentPanel(newExperiment);
				experimentOffset = experimentOffset + newExperiment.getHeight();
				
			}
			
			newSubject.setLocation(0,subjectPanelHeight );
			newSubject.updateSize();
			
			subjectPanelHeight += experimentOffset;
			menu.addSubjectPanel(newSubject);
			
		}
		
		menu.updateMenu(study, subjectPanelHeight);
		menu.revalidate();
		menu.repaint();
	}
	
	public void showExperiment(int subjectIndex, int experimentIndex) throws FileNotFoundException, IOException, PropertyVetoException{
		
		
		Subject sub = study.getSubjects().get(subjectIndex);
		Experiment ex = sub.getExperiments().get(experimentIndex);
		
		Menu menu = Menu.getInstance();
		
		ExperimentFrame frame = new ExperimentFrame(ex, this.getContentPane().getWidth()- menu.getWidth(), this.getContentPane().getHeight());
		
		if (study.useLoad()){
			frame.setComponentPopupMenu(new PopUpExperimentFrame(sub.getId(),ex.getId()));
		}
		frame.setVisible(true); 
		desktop.add(frame);
		frame.resizeElements();
		frame.initImagePanel();
		frame.setSelected(true);
	

	}
	
	public void showCLView(int subjectIndex, int experimentIndex) throws PropertyVetoException{
		
		Subject sub = study.getSubjects().get(subjectIndex);
		Experiment ex = sub.getExperiments().get(experimentIndex);
		
		CLView clView = new CLView(ex);
		clView.setVisible(true);
		desktop.add(clView);
		clView.setSelected(true);
	}
	
	public void updateStudy(int experimentID, boolean enable) {
		
//		study.getExperiments().stream().filter(exp -> experimentID == exp.getId())
//									   .forEach(exp -> exp.setEnabled(enable));
//		
//		ArrayList<Experiment> activeExperiments = new ArrayList<Experiment>();
//		
//		study.getExperiments().stream().filter(exp -> exp.isEnabled())
//									   .forEach(exp -> activeExperiments.add(exp));
//		StudyFrame.getInstance().updateExperiments(activeExperiments);
	}
	
	public void calculateFixations(int duration, int dispersion){
			
		
		for (Subject s : study.getSubjects()){
			for (Experiment ex : s.getExperiments()){
				ex.calculateFixations(duration, dispersion, ex.getScreenWidth(), ex.getScreenHeight());
			}
		}
		/*
		for (JInternalFrame frame :desktop.getAllFrames()){
			if (frame.getClass().toString().contains("Experiment"))
				((ExperimentFrame) frame).calculateFixations(duration, dispersion);
			
		}*/
	}
	public void calculateBlinks(int threshold){
			
		
		for (Subject s : study.getSubjects()){
			for (Experiment ex : s.getExperiments()){
				ex.calculateBlinks(threshold, ex.getScreenWidth(), ex.getScreenHeight());
			}
		}
	}
	
	
	public void recalculateHeatmaps(){
		
		for (JInternalFrame frame :desktop.getAllFrames()){
			if (frame.getClass().toString().contains("ExperimentFrame"))
				((ExperimentFrame) frame).recalculateHeatMap();
			
		}
	}
	
	public void repaintHeatmaps() {
		
		for (JInternalFrame frame :desktop.getAllFrames()){
			
			if (frame.getClass().toString().contains("ExperimentFrame"))
				((ExperimentFrame) frame).updateHeatmap();
			
		}
	}
	
	
	public void initValues(){
		
		
		
		/*
		ExperimentMenu.getInstance().initValues(this.getContentPane().getWidth(), this.getContentPane().getHeight());
		
		
		ImagePanel.getInstance().initValues(this.getContentPane().getWidth() - ExperimentMenu.getInstance().getWidth(), 
									this.getContentPane().getHeight());
		
		ImagePanel.getInstance().minTimeRange = ExperimentMenu.getInstance().rangeSlider.getValue();
		ImagePanel.getInstance().maxTimeRange = ExperimentMenu.getInstance().rangeSlider.getUpperValue();
		
		
		getContentPane().add(ExperimentMenu.getInstance());

		// to catch key events
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(new OEDispatcher());
		
		
		//clusterMode = ClusterMode.KMEANS;
		
		// handle resize
		this.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				
				ImagePanel imagePanel = ImagePanel.getInstance();
				GUI gui = GUI.getInstance();
				
				ExperimentMenu.getInstance().resize(GUI.this.getContentPane().getWidth(), GUI.this.getContentPane().getHeight());
				imagePanel.resize(GUI.this.getContentPane().getWidth() - ExperimentMenu.getInstance().getWidth(), GUI.this.getContentPane().getHeight());
				//if (gui.experiment != null)	
					//imagePanel.heatMap = new HeatMap(imagePanel.getWidth(), imagePanel.getHeight(), gui.experiment.getTrials().get(gui.trialNumber).getScreens().get(gui.screenNumber).getGazes(), imagePanel.scaleX, imagePanel.scaleY);
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				
			}
		});	
		
		loadExperiment.addActionListener(actionListener);
		loadStudy.addActionListener(actionListener);
		*/
	}


	
	
	/*
	public void displayStudy(int trial, int screen, int exp) throws FileNotFoundException, IOException{
		
		screenNumber = screen;
		trialNumber = trial;
		ImagePanel imagePanel= ImagePanel.getInstance();
		imagePanel.initLayers(study.getExperiments().get(exp).getScreenWidth(), study.getExperiments().get(exp).getScreenHeight(), study.getExperiments().get(exp).getTrials().get(trial).getScreens().get(screen).getFilename());
		updateEyeMovementData();
		imagePanel.drawAoi(study.getExperiments().get(0).getTrials().get(trialNumber).getScreens().get(screenNumber).getAois());
		setRange(trial,screen, exp);
		updateLabel();
	}
	
	public void updateEyeMovementData(){
	
		ExperimentMenu menu = ExperimentMenu.getInstance();
		ArrayList<Fixation> fixations = null;
		ArrayList<Gaze> gazes = null;
		ArrayList<Saccade> saccades = null;
		
		
		if (menu.showGazes.getState()){
			
			if (experimentMode)
				gazes = experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getGazes();
			else 
				gazes = study.getActiveGazes();
		}
		
		if(menu.showFixations.getState()){
			
			if (experimentMode)
				fixations = experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getFixations();
			else 
				fixations = study.getActiveFixations();
		}
		
		if(menu.showSaccades.getState()){
			
			if (experimentMode)
				saccades = experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getSaccades();
			else 
				saccades = study.getActiveSaccades();
		}
				
		ImagePanel.getInstance().drawEyeMovementData(gazes, fixations, saccades, experimentMode);
		ImagePanel.getInstance().repaint();
	}
	
	
	public void displayExperiment(int trial, int screen) throws FileNotFoundException, IOException{
		
		
		screenNumber = screen;
		trialNumber = trial;
		ImagePanel imagePanel= ImagePanel.getInstance();
		
		// init image layers
		imagePanel.initLayers(experiment.getScreenWidth(), experiment.getScreenHeight(), experiment.getTrials().get(trial).getScreens().get(screen).getFilename());
		updateEyeMovementData();
		imagePanel.drawAoi(experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getAois());
		setRange(trial,screen, 0);
		updateLabel();
		
		//System.out.println(experiment.getQuality());
		
		ExperimentMenu menu = ExperimentMenu.getInstance();
		if (experiment.getQuality().toString().contains("1"))
			menu.qualityCombobox.setSelectedItem("1_Perfect");
		else if (experiment.getQuality().toString().contains("2"))
			menu.qualityCombobox.setSelectedItem("2_Good_with_Drifts");
		else if (experiment.getQuality().toString().contains("3"))
			menu.qualityCombobox.setSelectedItem("3_Not_all_Screens_usable");
		else if (experiment.getQuality().toString().contains("4"))
			menu.qualityCombobox.setSelectedItem("4_Crap");
		else
			menu.qualityCombobox.setSelectedItem("Unrated");
		
	}
		
	public void clusterExperiment(){

		ExperimentMenu menu = ExperimentMenu.getInstance();
		ArrayList<Cluster> cluster = new ArrayList<Cluster>();
		
		
		Vector<Gaze> clusterInput = new Vector<Gaze>();
		
		if (experimentMode){
			for (Fixation f : experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getFixations())
				clusterInput.add(f);
		}
		else{
			for (Experiment ex : study.getExperiments()){
				if (ex.isEnabled()){
					for (Fixation f : ex.getTrials().get(trialNumber).getScreens().get(screenNumber).getFixations())
						clusterInput.add(f);
				}
			}
		}
				
		switch (clusterMethod) {
		
		case KMEANS:

			kMeans = new KMeans();
			cluster = kMeans.kMeans(Integer.valueOf(menu.clusterNumber.getValue().toString()).intValue(), clusterInput);
			break;

		case DBSCAN:
			
			dbScan = new DBScan(false);
			dbScan.updateValues();
			cluster = dbScan.cluster(clusterInput);
			break;
			
		default:
			break;
		}
			
		//System.out.println(cluster.size());
		ImagePanel.getInstance().clusters = cluster;
		//ImagePanel.getInstance().repaint();
		
	}
	

	public void updateRange(){
		
		ExperimentMenu menu = ExperimentMenu.getInstance();
		
		ArrayList<Gaze> gazes;
		if (experimentMode)
			gazes = experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getGazes();
		else
			gazes = study.getExperiments().get(0).getTrials().get(trialNumber).getScreens().get(screenNumber).getGazes();
		
		ImagePanel panel = ImagePanel.getInstance();
		panel.stopAnimation();
		
		menu.rangeSlider.setMinimum(gazes.get(0).getTimestampStart());
		menu.rangeSlider.setMaximum(gazes.get(gazes.size()-1).getTimestampStart());
		menu.rangeSlider.setValue(menu.rangeSlider.getMinimum());
		menu.rangeSlider.setUpperValue(menu.rangeSlider.getMaximum());
		
		panel.minTimeRange = menu.rangeSlider.getValue();
		panel.maxTimeRange = menu.rangeSlider.getUpperValue();
		updateEyeMovementData();
		
	}
	
	private void setRange(int trial, int screen, int ex){
		
		ExperimentMenu menu = ExperimentMenu.getInstance();
		ArrayList<Gaze> gazes;
		if (experimentMode)
			gazes = experiment.getTrials().get(trial).getScreens().get(screen).getGazes();
		else
			gazes = study.getExperiments().get(ex).getTrials().get(trial).getScreens().get(screen).getGazes();
		
		ImagePanel panel = ImagePanel.getInstance();
		panel.stopAnimation();
		
		//System.out.println(gazes.get(0).getTimestampStart() + " " + gazes.get(gazes.size()-1).getTimestampStart());
		menu.rangeSlider.setMinimum(gazes.get(0).getTimestampStart());
		menu.rangeSlider.setMaximum(gazes.get(gazes.size()-1).getTimestampStart());
		menu.rangeSlider.setValue(menu.rangeSlider.getMinimum());
		menu.rangeSlider.setUpperValue(menu.rangeSlider.getMaximum());
		
		panel.minTimeRange = menu.rangeSlider.getValue();
		panel.maxTimeRange = menu.rangeSlider.getUpperValue();	
		
	}
	
	public void playScreen(){
		
		ExperimentMenu menu = ExperimentMenu.getInstance();
		ImagePanel panel = ImagePanel.getInstance();
		ArrayList<Gaze> gazes = new ArrayList<Gaze>();
		
		if (menu.showGazes.getState())
			for (Gaze g: experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getGazes())
				if (g.isInRange(panel.minTimeRange, panel.maxTimeRange))
					gazes.add(g);
		if (menu.showFixations.getState())
			for (Fixation f: experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getFixations())
				if (f.isInRange(panel.minTimeRange, panel.maxTimeRange))
					gazes.add(f);
		if (menu.showSaccades.getState())
			for (Saccade s: experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getSaccades())
				if (s.isInRange(panel.minTimeRange, panel.maxTimeRange))
					gazes.add(s);
		
		Collections.sort(gazes);
		panel.startAnimation(gazes);
	}
	
	
	public void updateLabel(){
		
		if (experiment != null){
			ExperimentMenu.getInstance().trialLabel.setText("Trial: " + (trialNumber+1) + "/" + experiment.getTrials().size());
			ExperimentMenu.getInstance().screenLabel.setText("Screen: " + (screenNumber+1) + "/" + experiment.getTrials().get(trialNumber).getScreens().size());
		}
		else{
			ExperimentMenu.getInstance().trialLabel.setText("Trial: " + (trialNumber+1) + "/" + study.getExperiments().get(0).getTrials().size());
			ExperimentMenu.getInstance().screenLabel.setText("Screen: " + (screenNumber+1) + "/" +  study.getExperiments().get(0).getTrials().get(trialNumber).getScreens().size());
		}
		clearSelection();
	}
	
	
	public void rangeSearch(BoundingBox bb){
				
		int gazeCounter = 0;
		int fixCounter = 0;
		int sacCounter = 0;
			
		ImagePanel panel = ImagePanel.getInstance();
		ExperimentMenu menu = ExperimentMenu.getInstance();
		
		if (GUI.getInstance().experimentMode){
		
			Screen screen = experiment.getTrials().get(trialNumber).getScreens().get(screenNumber);
			
			if (menu.showFixations.getState()){
				for (Gaze g :screen.getFixations()){
					
					g.deselect();
					
					if (bb.contains(g, panel.scaleX, panel.scaleY) && 
						panel.checkTimestamp(g.getTimestampStart(), g.getTimestampEnd())){
						
						fixCounter++;
						g.select();
					}
				}
			}
			else
				fixCounter = 0;
			
			if(menu.showSaccades.getState()){	
				for (Saccade s :screen.getSaccades()){
					
					s.deselect();
					
					if (bb.contains(s, panel.scaleX, panel.scaleY) && 
						panel.checkTimestamp(s.getTimestampStart(), s.getTimestampEnd())){
						
						sacCounter++;
						s.select();
					}
				}
			}
			else
				sacCounter = 0;
			
			if(menu.showGazes.getState())
				for (Gaze g :screen.getGazes()){
					
					g.deselect();
					
					if (bb.contains(g, panel.scaleX, panel.scaleY) && 
						panel.checkTimestamp(g.getTimestampStart(), g.getTimestampEnd())){
					
						gazeCounter++;
						g.select();
					}
				}
			else
				gazeCounter = 0;
		}
		
		// study
		else{
			
			if (menu.showFixations.getState()){

				for (Fixation f :GUI.getInstance().study.getActiveFixations()){
					
					f.deselect();
					
					if (bb.contains(f, panel.scaleX, panel.scaleY)){ //&& 
						//panel.checkTimestamp(f.getTimestampStart(), f.getTimestampEnd())){
						
						fixCounter++;
						f.select();
					}
				}
			}
			else
				fixCounter = 0;
			
			if (menu.showGazes.getState()){

				for (Gaze g :GUI.getInstance().study.getActiveGazes()){
					
					g.deselect();
					
					if (bb.contains(g, panel.scaleX, panel.scaleY)){ //&& 
						//panel.checkTimestamp(f.getTimestampStart(), f.getTimestampEnd())){
						
						gazeCounter++;
						g.select();
					}
				}
			}
			else
				gazeCounter = 0;
			
			if (menu.showSaccades.getState()){

				for (Saccade s :GUI.getInstance().study.getActiveSaccades()){
					
					s.deselect();
					
					if (bb.contains(s, panel.scaleX, panel.scaleY)){ //&& 
						//panel.checkTimestamp(f.getTimestampStart(), f.getTimestampEnd())){
						
						sacCounter++;
						s.select();
					}
				}
			}
			else
				sacCounter = 0;
		}
			
		
		
		menu.selectedFixations.setText("Fixations: " + fixCounter);
		menu.selectedGazes.setText("Gaze: " + gazeCounter);
		menu.selectedSaccades.setText("Saccades: " + sacCounter);
		updateEyeMovementData();
	}
	
	
	private void clearSelection(){
		
		if (experiment != null){
			for (Gaze g :experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getFixations())
				g.deselect();
			for (Gaze g :experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getGazes())
				g.deselect();
			for (Gaze g :experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getSaccades())
				g.deselect();
		}
		
		else if (study != null){
			for (Gaze g : study.getActiveFixations())
				g.deselect();
			for (Gaze g : study.getActiveGazes())
				g.deselect();
			for (Gaze g : study.getActiveSaccades())
				g.deselect();
		}
		
		ExperimentMenu menu = ExperimentMenu.getInstance();
		
		menu.selectedFixations.setText("Fixations: -" );
		menu.selectedGazes.setText("Gaze: -");
		menu.selectedSaccades.setText("Saccades: -");

	}
	
	public void updateAoiLables(int fixations, int transitions, int gazes){
		
		ExperimentMenu menu = ExperimentMenu.getInstance();
		
		menu.lblFixationsInAoi.setText("Fixations: " + fixations);
		menu.lblGazesInAoi.setText("Gazes: " + gazes);
		menu.lblTransitionsInAoi.setText("Transitions: " + transitions);
		
	}
	
	public void updateAoiComboBox(){
		
		ExperimentMenu menu = ExperimentMenu.getInstance();
		
		menu.model.removeAllElements();
		
		for (AreaOfInterest aoi : study.getExperiments().get(0).getTrials().get(trialNumber).getScreens().get(screenNumber).getAois())
			menu.model.addElement(aoi.getName());
		
	}
	
	public void driftEyeData(int x, int y){
				
		experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).updateDriftCorrection(x, y);
		updateDriftLables();
	}
	
	public void resetEyeData(){

		experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).resetDriftCorrection();
		updateDriftLables();
	}
	
	public void updateDriftLables(){
		
		ExperimentMenu menu = ExperimentMenu.getInstance();
		
		menu.driftX.setText("X: " + experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getDriftCorrection().getDriftX() );
		menu.driftY.setText("Y: " + experiment.getTrials().get(trialNumber).getScreens().get(screenNumber).getDriftCorrection().getDriftY() );
	}
	*/
}
