package View.Experiment;

import java.awt.Dimension;
import java.beans.PropertyVetoException;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import Analysis.CognitiveLoad;
import Data.Subject.Experiment;
import EventHandler.CognitiveLoadPanelListener;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JCheckBox;

public class CLView extends JInternalFrame {

	private Experiment experiment;
	private int width;
	private int height;
	private JPanel CLPanel;
	private JLabel lblMentalDemand;
	private JLabel lblNasatlx;
	private JLabel lblPhysicalDemand;
	private JLabel lblEffort;
	private JLabel lblFrustration;
	private JLabel lblTemporalDemand;
	private JLabel lblPerformance;
	private CognitiveLoadPanel clPanel;
	private JCheckBox chckbxTlx;
	private JCheckBox chckbxMentalDemand;
	private JCheckBox chckbxPhysicalDemand;
	private JCheckBox chckbxTemporalDemand;
	private JCheckBox chckbxPerformance;
	private JCheckBox chckbxEffort;
	private JCheckBox chckbxFrustration;
	private JCheckBox chckbxLeftPupil;
	private JCheckBox chckbxRightPupil;
	
	public CLView(Experiment experiment){

		 super("Cognitive Load " + experiment.getName(),
		          true, //resizable
		          true, //closable
		          true, //maximizable
		          true);//iconifiable
		 getContentPane().setBackground(Color.WHITE);
		 try {
			setSelected(true);
		} catch (PropertyVetoException e) {
			
			e.printStackTrace();
		}
		
		 
		this.experiment = experiment;
		width = 800;
		height = 500;
		this.setSize(new Dimension(width, height));
		setLocation(10,10);
		getContentPane().setLayout(null);    
		
		clPanel = new CognitiveLoadPanel(700, 300, experiment);
		add(clPanel);
		
		
		lblNasatlx = new JLabel("NASA-TLX:");
		lblNasatlx.setBounds(32, 11, 200, 18);
		getContentPane().add(lblNasatlx);
		
		lblMentalDemand = new JLabel("Mental Demand:");
		lblMentalDemand.setBounds(32, 28, 200, 18);
		getContentPane().add(lblMentalDemand);
		
		lblPhysicalDemand = new JLabel("Physical Demand:");
		lblPhysicalDemand.setBounds(32, 45, 200, 18);
		getContentPane().add(lblPhysicalDemand);
		
		lblTemporalDemand = new JLabel("Temporal Demand:");
		lblTemporalDemand.setBounds(32, 62, 200, 18);
		getContentPane().add(lblTemporalDemand);
		
		lblPerformance = new JLabel("Performance:");
		lblPerformance.setBounds(32, 79, 200, 18);
		getContentPane().add(lblPerformance);
		
		lblEffort = new JLabel("Effort:");
		lblEffort.setBounds(32, 96, 200, 18);
		getContentPane().add(lblEffort);
		
		lblFrustration = new JLabel("Frustration:");
		lblFrustration.setBounds(32, 111, 200, 18);
		getContentPane().add(lblFrustration);
		
		CognitiveLoadPanelListener listener = new CognitiveLoadPanelListener(this);
		
		chckbxTlx = new JCheckBox("");
		chckbxTlx.setBackground(Color.WHITE);
		chckbxTlx.setBounds(10, 11, 21, 18);
		chckbxTlx.setSelected(true);
		getContentPane().add(chckbxTlx);
		
		
		chckbxMentalDemand = new JCheckBox("");
		chckbxMentalDemand.setBackground(Color.WHITE);
		chckbxMentalDemand.setBounds(10, 28, 21, 18);
		getContentPane().add(chckbxMentalDemand);
		
		chckbxPhysicalDemand = new JCheckBox("");
		chckbxPhysicalDemand.setBackground(Color.WHITE);
		chckbxPhysicalDemand.setBounds(10, 45, 21, 18);
		getContentPane().add(chckbxPhysicalDemand);
		
		chckbxTemporalDemand = new JCheckBox("");
		chckbxTemporalDemand.setBackground(Color.WHITE);
		chckbxTemporalDemand.setBounds(10, 62, 21, 18);
		getContentPane().add(chckbxTemporalDemand);
		
		chckbxPerformance = new JCheckBox("");
		chckbxPerformance.setBackground(Color.WHITE);
		chckbxPerformance.setBounds(10, 79, 21, 18);
		getContentPane().add(chckbxPerformance);
		
		chckbxEffort = new JCheckBox("");
		chckbxEffort.setBackground(Color.WHITE);
		chckbxEffort.setBounds(10, 96, 21, 18);
		getContentPane().add(chckbxEffort);
		
		chckbxFrustration = new JCheckBox("");
		chckbxFrustration.setBackground(Color.WHITE);
		chckbxFrustration.setBounds(10, 111, 21, 18);
		getContentPane().add(chckbxFrustration);
		
		chckbxLeftPupil = new JCheckBox("Left Pupil");
		chckbxLeftPupil.setBackground(Color.WHITE);
		chckbxLeftPupil.setBounds(10, 431, 100, 18);
		getContentPane().add(chckbxLeftPupil);
		
		chckbxRightPupil = new JCheckBox("Right Pupil");
		chckbxRightPupil.setBackground(Color.WHITE);
		chckbxRightPupil.setBounds(10, 446, 100, 18);
		getContentPane().add(chckbxRightPupil);
		
		chckbxTlx.addItemListener(listener);
		chckbxEffort.addItemListener(listener);
		chckbxFrustration.addItemListener(listener);
		chckbxMentalDemand.addItemListener(listener);
		chckbxPerformance.addItemListener(listener);
		chckbxPhysicalDemand.addItemListener(listener);
		chckbxTemporalDemand.addItemListener(listener);
		chckbxRightPupil.addItemListener(listener);
		chckbxLeftPupil.addItemListener(listener);
		
		
		moveToFront(); 
		updateLabels(experiment.getCognitiveLoad());
		
	}
	
	public void updateLabels(CognitiveLoad load){
		
		lblNasatlx.setText("NASA-TLX: " + load.getTLXScore());
		lblMentalDemand.setText("Mental Demand: " + load.getMentalDemand());
		lblPhysicalDemand.setText("Physical Demand: " + load.getPhysicalDemand());
		lblTemporalDemand.setText("Temporal Demand: " + load.getTemporalDemand());
		lblEffort.setText("Effort: " + load.getEffort());
		lblPerformance.setText("Performance: " + load.getPerformance());
		lblFrustration.setText("Frustration: " + load.getFrustration());
		this.repaint();
		
	}

	public void updateCheckboxes() {
		
		clPanel.setDrawEffort(chckbxEffort.isSelected());
		clPanel.setDrawTemporalDemand(chckbxTemporalDemand.isSelected());
		clPanel.setDrawFrustration(chckbxFrustration.isSelected());
		clPanel.setDrawMentalDemand(chckbxMentalDemand.isSelected());
		clPanel.setDrawPerformance(chckbxPerformance.isSelected());
		clPanel.setDrawPhysicalDemand(chckbxPhysicalDemand.isSelected());
		clPanel.setDrawTLXScore(chckbxTlx.isSelected());
		clPanel.setDrawLeftPupil(chckbxLeftPupil.isSelected());
		clPanel.setDrawRightPupil(chckbxRightPupil.isSelected());
		
		clPanel.repaint();
	}
}
