package View.Experiment;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class ImageLayer {

	private int width;
	private int height;
	private int type;
	private BufferedImage scaled;
	private BufferedImage unscaled;
	private boolean enabled;
	
	public ImageLayer(int width, int height, int type, boolean enabled) {
		
		this.type = type;
		this.height = height;
		this.width = width;
		this.enabled = enabled;
		
	}
		
	public void enableLayer(boolean b){
		enabled = b;
	}
	
	public ImageLayer (int width, int height, String path) throws FileNotFoundException, IOException{
			
		type = BufferedImage.TYPE_INT_RGB;
		loadImage(path);
		resizeLayer(width, height);
	}
	
	public void loadImage(String path) throws FileNotFoundException, IOException{
				
		unscaled = ImageIO.read(new FileInputStream(path));
		scaled = unscaled;
	}
	
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}
	
	public void setBufferedImage(BufferedImage b){
		unscaled = b;
	}
	
	public BufferedImage getUnscaled(){
		return unscaled;
	}
	
	public BufferedImage getScaled(){
		return scaled;
	}
	
	public void resizeLayer(int width, int height){
	
		if (enabled){
			BufferedImage newImage = new BufferedImage(width, height,
		            type);
			Graphics2D g = newImage.createGraphics();
		 
		    try {
		    	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		    			RenderingHints.VALUE_ANTIALIAS_ON);
		    	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
		    			RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		       
		    	g.drawImage(unscaled, 0, 0, width, height, null);
		    	
		    } finally {
		        g.dispose();
		    }
			
			scaled = newImage;
		}
	}

	
	
}
