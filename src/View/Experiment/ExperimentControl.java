package View.Experiment;

import java.awt.Color;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import EventHandler.ExperimentControlListener;
import EventHandler.ExperimentFrameListener;

public class ExperimentControl extends JPanel{

	private int width = 800;
	private int height = 32;
	private SAFTTimeLine timeLine;
	
	public ExperimentControl(ExperimentImagePanel imagePanel){
		
		ExperimentControlListener listener = new ExperimentControlListener(imagePanel, this);
		this.setSize(width, height);
		  
	    JButton btnPlay = new JButton();
	    btnPlay.setToolTipText("Play Experiment");
	    btnPlay.setMargin(new Insets(0, 0, 0, 0));
	    btnPlay.setBackground(Color.white);
	    setLayout(null);
	    btnPlay.setIcon(new ImageIcon(getClass().getResource("/images/play.png")));
	    btnPlay.setActionCommand("play");
	    btnPlay.setBounds(10, 1, 29, 29);
	    this.add(btnPlay);
	    btnPlay.addActionListener(listener);
	    
	    JButton btnNextTrial = new JButton();
	    btnNextTrial.setToolTipText("Go To Next Trial");
	    btnNextTrial.setIcon(new ImageIcon(getClass().getResource("/images/forward_green.png")));
	    btnNextTrial.setBounds(253, 1, 29, 29);
	    btnNextTrial.setBackground(Color.white);
	    btnNextTrial.setActionCommand("next trial");
	    this.add(btnNextTrial);
	    btnNextTrial.addActionListener(listener);
	    
	    JButton btnBackTrial = new JButton();
	    btnBackTrial.setToolTipText("Go To Previous Trial");
	    btnBackTrial.setBackground(Color.white);
	    btnBackTrial.setIcon(new ImageIcon(getClass().getResource("/images/back_green.png")));
	    btnBackTrial.setActionCommand("previous trial");
	    btnBackTrial.setBounds(133, 1, 29, 29);
	    this.add(btnBackTrial);
	    btnBackTrial.addActionListener(listener);
	   	    
	    JButton btnPause = new JButton("");
	    btnPause.setToolTipText("Pause Experiment");
	    btnPause.setIcon(new ImageIcon(getClass().getResource("/images/pause.png")));
	    btnPause.setBounds(45, 1, 29, 29);
	    btnPause.setActionCommand("pause");
	    btnPause.addActionListener(listener);
	    add(btnPause);
	    
	    JButton btnStop = new JButton("");
	    btnStop.setToolTipText("Stop Experiment");
	    btnStop.setIcon(new ImageIcon(getClass().getResource("/images/stop.png")));
	    btnStop.setBounds(82, 1, 29, 29);
	    btnStop.setActionCommand("stop");
	    btnStop.addActionListener(listener);
	    add(btnStop);
	    
	    JButton btnNextScreen = new JButton("");
	    btnNextScreen.setToolTipText("Go To Next Screen");
	    btnNextScreen.setIcon(new ImageIcon(getClass().getResource("/images/forward_blue.png")));
	    btnNextScreen.setBounds(213, 1, 29, 29);
	    btnNextScreen.setActionCommand("next screen");
	    btnNextScreen.addActionListener(listener);
	    add(btnNextScreen);
	    
	    JButton btnBackScreen = new JButton("");
	    btnBackScreen.setToolTipText("Go To Previous Screen");
	    btnBackScreen.setIcon(new ImageIcon(getClass().getResource("/images/back_blue.png")));
	    btnBackScreen.setBounds(173, 1, 29, 29);
	    btnBackScreen.setActionCommand("previous screen");
	    btnBackScreen.addActionListener(listener);
	    add(btnBackScreen);
		
	    timeLine = new SAFTTimeLine(imagePanel);
	    timeLine.setLocation(307, 0);
	    add(timeLine);
	    
	}
	
	public void resize(int y, int width, int height){
		
		this.setBounds(0,y,width, height);
		timeLine.resize(width-350);
	}
	
	public void setTimeLine(long timeStamp){
		
		timeLine.setSlider(timeStamp);
	}
}
