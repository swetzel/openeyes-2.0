package View.Experiment;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.util.Vector;
import java.util.spi.TimeZoneNameProvider;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import View.GUI;
import View.Menu.Menu;
import Analysis.HeatMap;
import Data.AreaOfInterest;
import Data.Cluster;
import Data.Point2D;
import Data.Study.Study;
import Data.Subject.Experiment;
import Data.Subject.Screen;
import Data.Subject.Trial;
import Events.LocationEvent;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.MousePosition;
import Events.Default.Saccade;



/**
 * 
 * @author Stefanie Wetzel <stefanie.wetzel@gmail.com>
 *
 * Panel that displays the eye movement data. Singleton
 *
 */
public class ExperimentImagePanel extends JPanel implements Runnable{


	private static final long serialVersionUID = 1L;
	public float scaleX = 1.0f;
	public float scaleY = 1.0f;
	
	private ImageLayer background;
	private ImageLayer heatMap;
	private ImageLayer cluster;
	private ImageLayer aoi;
	
	private boolean showGazes = false;
	private boolean showFixations = false;
	private boolean showSaccades = false;
	private boolean showAois = false;
	private boolean showHeatmap = false;
	private boolean showMousePosition = false;
	private boolean showMouseClick = false;
	
	private ExperimentFrame frame;
	private Experiment experiment;
	private int screen = 0;
	private int trial = 0;
	
	private int trialPlayIndex = 0;
	private int screenPlayIndex = 0;
	private int gazeIndex = 0;
	private long currentTimeStamp = 0;
	private ArrayList<LocationEvent> playableGazes = new ArrayList<LocationEvent>();
	private PlayMode playMode = PlayMode.STOP;
	
	private volatile Thread th;
	private HeatMap map;
	private boolean playFixations = true;

	public enum PlayMode{
		
		PAUSE,
		STOP,
		PLAY,
	}
	
	public Experiment getExperiment(){
		
		return experiment;
	}
	
	public ExperimentImagePanel(Experiment experiment, ExperimentFrame frame) throws FileNotFoundException, IOException{
		
		setBackground(Color.WHITE);
		this.experiment = experiment;
		this.frame = frame;
		currentTimeStamp = experiment.getFirstTimeStamp();
		
		// create mouse listener
		this.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				
				scaleX = ((float) ExperimentImagePanel.this.getWidth()/(float) (background.getUnscaled().getWidth()));
				scaleY = ((float) ExperimentImagePanel.this.getHeight()/(float) (background.getUnscaled().getHeight()));
				repaint();
				
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public Trial getActiveTrial(){
		
		return experiment.getTrials().get(trial);
	}
	
	public Screen getActiveScreen(){
		
		return experiment.getTrials().get(trial).getScreens().get(screen);
	}
	
	public void nextTrial(){
		
		if (trial+1 < experiment.getTrials().size()){
			
			trial++;
			trialPlayIndex = trial;
			playableGazes.clear();
		    gazeIndex = 0;
		    screen = 0;
			try {
				loadScreen(experiment.getTrials().get(trial).getScreens().get(screen).getFilename());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (showAois)
			drawAoi();
		
	}
	
	public void nextScreen(){
		
		if (screen+1 < experiment.getTrials().get(trial).getScreens().size()){
			
			screen++;
			screenPlayIndex = screen;
			playableGazes.clear();
		    gazeIndex = 0;
			try {
				loadScreen(experiment.getTrials().get(trial).getScreens().get(screen).getFilename());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (screen+1 >= experiment.getTrials().get(trial).getScreens().size()){
		
			if (trial+1 < experiment.getTrials().size()){
				
				trial++;
				trialPlayIndex = trial;
				playableGazes.clear();
			    gazeIndex = 0;
			    screen = 0;
				try {
					loadScreen(experiment.getTrials().get(trial).getScreens().get(screen).getFilename());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if (showAois)
			drawAoi();
	}
	
	public void previousTrial(){
		
		if (trial-1 >= 0){
			
			trial--;
			trialPlayIndex = trial;
			playableGazes.clear();
		    gazeIndex = 0;
		    screen = 0;
			try {
				loadScreen(experiment.getTrials().get(trial).getScreens().get(screen).getFilename());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (showAois)
			drawAoi();
	}
	
	public void previousScreen(){
		
		if (screen-1 >=0){
			
			screen--;
			screenPlayIndex = screen;
			playableGazes.clear();
		    gazeIndex = 0;
			try {
				loadScreen(experiment.getTrials().get(trial).getScreens().get(screen).getFilename());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (screen-1 < 0){
			
			if (trial-1 >= 0){
				
				trial--;
				trialPlayIndex = trial;
				playableGazes.clear();
			    gazeIndex = 0;
			    screen = experiment.getTrials().get(trial).getScreens().size()-1;
				try {
					loadScreen(experiment.getTrials().get(trial).getScreens().get(screen).getFilename());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
		}
		
		
		if (showAois)
			drawAoi();
	}
	
	public void setExperimentPlayMode(PlayMode mode){
		
		this.playMode = mode;
	}
	
	public void setEyeDataVisibility(boolean gazes, boolean fix, boolean sac){
		
		showFixations = fix;
		showGazes = gazes;
		showSaccades = sac;
		
		// redraw eyedata layer
		repaint();
	}
	
	public void setAoiVisibility(boolean selected) {
		
		showAois = selected;
		aoi.enableLayer(selected);
		if (showAois)
			drawAoi();
		
		repaint();
	}
	
	
	
	public void setHeatmapVisibility(boolean selected) {
		
		showHeatmap = selected;
		heatMap.enableLayer(selected);
		if (showHeatmap)
			computeHeatmap();
		else
			repaint();
		
	}

	public void computeHeatmap(){
		
		if (showHeatmap){
			Menu menu = Menu.getInstance();
			ArrayList<LocationEvent> gazes = getGazesForHeatMap();
			map = new HeatMap(heatMap.getWidth(), heatMap.getHeight(), gazes, scaleX, scaleY,  menu.getKernelSize(), menu.getGridSize());
		}
	}
	
	public void drawHeatmap() {
		
		BufferedImage newImage = new BufferedImage(heatMap.getWidth(),heatMap.getHeight(),BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = newImage.createGraphics();

		map.draw(g2d);
		heatMap.setBufferedImage(newImage);
		heatMap.resizeLayer(this.getWidth(), this.getHeight());	
		repaint();
	}

	private ArrayList<LocationEvent> getGazesForHeatMap() {

		ArrayList<LocationEvent> gazes = new ArrayList<LocationEvent>();
		Menu menu = Menu.getInstance();
		
		
		if (menu.getInputTypeForHeatmap() == 1){	//fixation
			
			
			if (menu.computeForAllScreens()){
				
				for(Trial t : experiment.getTrials()){
					for (Screen s : t.getScreens()){
						for (Fixation f : s.getFixations()){
							gazes.add(f);
						}
					}
				}
			}
			
			for (Fixation f : getActiveScreen().getFixations()){
				gazes.add(f);
			}
		}
		else if (menu.getInputTypeForHeatmap() == 2){	//Gazes
			
			if (menu.computeForAllScreens()){
				
				for(Trial t : experiment.getTrials()){
					for (Screen s : t.getScreens()){
						for (Gaze g : s.getGazes()){
							gazes.add(g);
					}
				}
				
				}
				
			}
			else {	
				for (Gaze g : getActiveScreen().getGazes()){
					gazes.add(g);
				}
			}
		}
		return gazes;
	}

	public void drawAoi(){
		
		BufferedImage newImage = new BufferedImage(aoi.getWidth(), aoi.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = newImage.createGraphics();
		
		Screen currentScreen = experiment.getTrials().get(trial).getScreens().get(screen);
		
		for (AreaOfInterest a : currentScreen.getAois())
			a.draw(g2d, scaleX, scaleY);
		
		
		aoi.setBufferedImage(newImage);
		aoi.resizeLayer(this.getWidth(), this.getHeight());	
	}
	
	
	public void paintComponent(Graphics g){
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		try {
			
	    	g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                RenderingHints.VALUE_ANTIALIAS_ON);
	    	g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	    			RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	       
	    	
	    	if (background != null){
	    		
	    		g2d.drawImage(background.getScaled(), 0, 0, this.getWidth(), this.getHeight(), null);
	    		
	    		// which layers should be drawn on the background layer?
	    		switch (playMode) {
	    		
				case STOP:
					
					drawEyeMovementData(g2d);
					if(showAois)
						g2d.drawImage(aoi.getScaled(), 0, 0, this.getWidth(),  this.getHeight(), null);
					if (showHeatmap)
						g2d.drawImage(heatMap.getScaled(), 0, 0, this.getWidth(),  this.getHeight(), null);
					if (showMousePosition)
						drawMousePosition(g2d);
					break;
					
				case PLAY:
					playExperiment(g2d);
				
				case PAUSE:
					drawPlayableGazes(g2d);
//				case AOI:		
//					//g2d.drawImage(eyeData.getScaled(), 0, 0, this.getWidth(),  this.getHeight(), null);
//					g2d.drawImage(cluster.getScaled(), 0, 0, this.getWidth(),  this.getHeight(), null);
//					g2d.drawImage(aoi.getScaled(), 0, 0, this.getWidth(),  this.getHeight(), null);
//					
//					/*
//					if (showAoiPoints){
//						
//						if (gui.selfDefinedAreaOfInterest.getShape().toString().equalsIgnoreCase("Polygon")){
//							for (Point2D p: aoiPoints)
//								p.draw(g2d, scaleX, scaleY);
//						}
//						else {
//							
//							
//							if(startAoi != null && endAoi != null){
//								
//								if (gui.selfDefinedAreaOfInterest.getShape().toString().equalsIgnoreCase("Ellipse")){
//									drawAoiRectangle(g2d, false);	
//									drawAoiEllipse(g2d);
//								}
//								else{
//									drawAoiRectangle(g2d, true);	
//								}
//							}
//						}
//					}*/
//					break;
//					
//				case HEATMAP:
//					//g2d.drawImage(eyeData.getScaled(), 0, 0, width, height, null);
//					g2d.drawImage(heatMap.getScaled(), 0, 0,  this.getWidth(),  this.getHeight(), null);
//					break;
//					
				default:
					break;
				}
	    		
	    		//if(showSelectionRectangle)
					//drawSelectionRectangle(g2d);
	    		
	    	}
	    	
	    } finally {
	        g2d.dispose();
	    }
	}
	
	private void drawMousePosition(Graphics2D g2d) {
		
		Screen currentScreen = experiment.getTrials().get(trial).getScreens().get(screen);
	
		for (MousePosition pos : currentScreen.getMousePositions()){
			pos.draw(g2d, scaleX, scaleY);
		}
	
	}

	public void initLayers() throws FileNotFoundException, IOException{
		
		Screen start = experiment.getTrials().get(0).getScreens().get(0);
		int screenWidth = experiment.getScreenWidth();
		int screenHeight = experiment.getScreenHeight();
		background = new ImageLayer(screenWidth, screenHeight, start.getFilename());
		cluster = new ImageLayer(screenWidth, screenHeight, BufferedImage.TYPE_INT_ARGB, false);
		heatMap = new ImageLayer(screenWidth, screenHeight, BufferedImage.TYPE_INT_ARGB, false);
		aoi = new ImageLayer(screenWidth, screenHeight, BufferedImage.TYPE_INT_ARGB, true);
		scaleX = ((float) this.getWidth()/(float) (background.getUnscaled().getWidth()));
		scaleY = ((float) this.getHeight()/(float) (background.getUnscaled().getHeight()));
		
		
		repaint();
	}
	
	public PlayMode getPlayMode(){
		return playMode;
	}
	
	public void setCurrentTimeStamp(int timeStamp){
		currentTimeStamp = timeStamp;
	}
	
	
	public void loadScreen(int timeStamp){

		outerloop:
		for (int t = 0; t < experiment.getTrials().size(); ++t){
			
			for (int s = 0; s < experiment.getTrials().get(t).getScreens().size(); ++s){
				
							
					if (timeStamp >= experiment.getTrials().get(t).getScreens().get(s).getTimestampStart() && 
						timeStamp <= experiment.getTrials().get(t).getScreens().get(s).getTimestampEnd()){
						
						try {
							loadScreen(experiment.getTrials().get(t).getScreens().get(s).getFilename());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						trial = t;
						screen = s;
						break outerloop;
					}
			}
		}
		currentTimeStamp = timeStamp;
		
	}
	
	private void loadScreen(String path) throws FileNotFoundException, IOException{
				
		background.loadImage(path);
		repaint();
	}
	
	public void drawEyeMovementData(Graphics2D g2d) {
				
		
		Screen currentScreen = experiment.getTrials().get(trial).getScreens().get(screen);
						
		if (showFixations && currentScreen.getFixations().size() > 0){	
			
			Fixation old = null;
			int fixCounter = 0;
			for (Fixation i : currentScreen.getFixations()){
						
				i.draw(g2d, scaleX, scaleY);
				
				// draw scanpath
				if (fixCounter > 0){
						
					g2d.drawLine((int) (old.getX()*scaleX ),
								 (int) (old.getY()*scaleY ),
								 (int) (i.getX()*scaleX ),
								 (int) (i.getY()*scaleY));
				}
				old = i;
				++fixCounter;
			}
		}
			
		// draw saccades	
		if (showSaccades && currentScreen.getSaccades().size() > 0)
			
			for (Saccade s : currentScreen.getSaccades())
				s.draw(g2d, scaleX, scaleY);
			
		
		// draw gazes		
		if (showGazes && currentScreen.getGazes().size() > 0){
			
			for (Gaze g : currentScreen.getGazes())			
					g.draw(g2d, scaleX, scaleY);
			
		}
	}


//	public void drawHeatMap(ArrayList<Gaze> gazes){
//		
//		BufferedImage newImage = new BufferedImage(heatMap.getWidth(),heatMap.getHeight(),BufferedImage.TYPE_INT_ARGB);
//		Graphics2D g2d = newImage.createGraphics();
//		
//		//ExperimentMenu experimentMenu = ExperimentMenu.getInstance();
//		HeatMap map = new HeatMap(heatMap.getWidth(), heatMap.getHeight(), gazes, scaleX, scaleY, (int) experimentMenu.heatMapRadius.getValue(),(int) experimentMenu.heatMapGridSize.getValue());
//		map.draw(g2d);
//			
//		heatMap.setBufferedImage(newImage);
//		heatMap.resizeLayer(width, height);	
//	}
//	
//	
//	private void playGazes(Graphics2D g2d) throws FileNotFoundException, IOException{
//		
//		Gaze gaze = null;
//		ExperimentMenu menu = ExperimentMenu.getInstance();
//		GUI gui = GUI.getInstance();
//		
//		//System.out.println("currentGazeIndex " + currentGazeIndex + " gazesToPlay"+ gazesToPlay.get(currentScreenIndex).size());
//		if(currentGazeIndex < videoGazes.get(currentScreenIndex).size()-1){
//			
//			gaze = videoGazes.get(currentScreenIndex).get(currentGazeIndex);
//			gaze.drawInVideo(g2d, scaleX, scaleY);
//			currentGazeIndexOverall++;
//		}
//		else if (currentGazeIndex >= videoGazes.get(currentScreenIndex).size()-1){
//			
//			currentGazeIndex = 0;
//			++currentScreenIndex;
//			currentGazeIndexOverall++;
//			loadScreen(gui.experiment.getTrials().get(currentTrialIndex).getScreens().get(currentScreenIndex).getFilename());
//		}
//		
//		menu.timeSlider.setValue(currentGazeIndexOverall);
//		if (gaze != null){
//			
//			int minTimestamp = videoGazes.getFirst().firstElement().getTimestampStart();
//			menu.videoTime.setText(""+ (Math.abs(gaze.getTimestampStart() - minTimestamp)/1000.0));
//			
//		}
//	}
//	
//	
//	
//	public void drawClusters() {
//		
//		Random randomGenerator = new Random();
//		int red;
//		int green;
//		int blue;
//		BufferedImage newImage = new BufferedImage(cluster.getWidth(),cluster.getHeight(),BufferedImage.TYPE_INT_ARGB);
//		Graphics2D g2d = newImage.createGraphics();
//		
//		if (cluster != null){
//						
//			for (Cluster v : clusters){
//			
//				// generate random color for every cluster
//				red = randomGenerator.nextInt(255);
//				green = randomGenerator.nextInt(255);
//				blue = randomGenerator.nextInt(255);
//				g2d.setColor(new Color(red,green,blue));
//				v.draw(g2d, scaleX, scaleY);
//			}
//		}		
//		
//		cluster.setBufferedImage(newImage);
//		cluster.resizeLayer(width, height);
//	}
//
//	
//	
//	public void drawAoi(ArrayList<AreaOfInterest> aois){
//		
//		BufferedImage newImage = new BufferedImage(aoi.getWidth(), aoi.getHeight(), BufferedImage.TYPE_INT_ARGB);
//		Graphics2D g2d = newImage.createGraphics();
//		
//		for (AreaOfInterest a : aois)
//			a.draw(g2d, scaleX, scaleY);
//		
//		
//		aoi.setBufferedImage(newImage);
//		aoi.resizeLayer(width, height);
//		
//	}
//
//	public void drawEyeMovementData(ArrayList<Gaze> gazes, ArrayList<Fixation> fixations, ArrayList<Saccade> saccades, boolean checkTimestamp) {
//		
//		ExperimentMenu experimentMenu = ExperimentMenu.getInstance();
//		BufferedImage newImage = new BufferedImage(eyeData.getWidth(), eyeData.getHeight(), BufferedImage.TYPE_INT_ARGB);
//		Graphics2D g2d = newImage.createGraphics();
//		
//		// draw fixations
//		Color c = new Color(255, 0, 0, 255);
//		g2d.setColor(c);
//				
//		if (fixations != null && fixations.size() > 0){	
//			
//			Fixation old = null;
//			int fixCounter = 0;
//			for (Fixation i : fixations){
//						
//				//g2d.setStroke(new BasicStroke(1));
//				
//				// check timestamp in experiment mode
//				if (checkTimestamp){
//					if (checkTimestamp(i.getTimestampStart(), i.getTimestampEnd())){
//							
//						i.draw(g2d, scaleX, scaleY);
//				
//						if (experimentMenu.showScanpaths.getState()){
//							if (fixCounter > 0){
//						
//								g2d.drawLine((int) (old.getX() ),
//											 (int) (old.getY() ),
//											 (int) (i.getX() ),
//											 (int) (i.getY()));
//								}
//							old = i;
//							++fixCounter;
//						}
//					}
//				}
//				// no timestamp check in study mode
//				else {
//					i.draw(g2d, scaleX, scaleY);
//					
//					if (experimentMenu.showScanpaths.getState()){
//						if (fixCounter > 0){
//					
//							g2d.drawLine((int) (old.getX() ),
//										 (int) (old.getY() ),
//										 (int) (i.getX() ),
//										 (int) (i.getY()));
//							}
//						old = i;
//						++fixCounter;
//					}
//				}
//			}
//		}
//			
//		// draw saccades	
//		g2d.setColor(Color.BLACK);
//		if (saccades != null)
//			for (Saccade s : saccades){
//					
//				if(checkTimestamp){
//				
//					//saccade between timestamps? 
//					if (checkTimestamp(s.getTimestampStart(), s.getTimestampEnd()))	
//						s.draw(g2d, scaleX, scaleY);
//				}
//				else 
//					s.draw(g2d, scaleX, scaleY);
//			}
//		
//		// draw gazes	
//		g2d.setColor(new Color(0, 255, 0, 100));	
//		if (gazes != null){
//			for (Gaze g : gazes){
//						
//				if (checkTimestamp){
//					
//					//gaze between timestamps?
//					if (checkTimestamp(g.getTimestampStart(), g.getTimestampEnd()))
//						g.draw(g2d, scaleX, scaleY);
//					
//				}
//				else 
//					g.draw(g2d, scaleX, scaleY);
//			}
//		}
//		eyeData.setBufferedImage(newImage);
//		eyeData.resizeLayer(width, height);
//	}
//
//	
//	private void playScreen(Graphics2D g2d){
//		
//		int fixCounter = 0;
//		Fixation old = null;
//		
//		for (int i = 0; i < animationCounter; ++i){
//			
//			if (gazesToAnimate.get(i).getClass().getName().contains("Fixation")){	
//				        	
//	        	if (ExperimentMenu.getInstance().showScanpaths.getState()){
//	        		
//	        		g2d.setColor(new Color(255, 0, 0));
//	        		
//	        		if (fixCounter > 0){
//	        			
//	        			g2d.drawLine((int) (old.getX() * scaleX),
//								 	 (int) (old.getY() * scaleY),
//								 	 (int) (((Fixation) gazesToAnimate.get(i)).getX() * scaleX),
//								 	 (int) (((Fixation) gazesToAnimate.get(i)).getY() * scaleY));
//	        		}
//	        		
//	        		old = (Fixation) gazesToAnimate.get(i);
//	        	}
//	        	
//	        	((Fixation) gazesToAnimate.get(i)).draw(g2d, scaleX, scaleY);
//	        	++fixCounter;
//	        	
//			}
//			
//			else if (gazesToAnimate.get(i).getClass().getName().contains("Saccade")){
//	        	
//	    		 ((Saccade) gazesToAnimate.get(i)).draw(g2d, scaleX, scaleY);
//	         }
//	         else if (gazesToAnimate.get(i).getClass().getName().contains("Gaze")){
//	        	 
//	    		 gazesToAnimate.get(i).draw(g2d, scaleX, scaleY);
//	         }
//			
//		}
//		
//	}
//	
//	private void drawSelectionRectangle(Graphics2D g2d){
//		
//		Color c = new Color(255, 0, 0, 255);
//		g2d.setColor(c);
//	
//		g2d.drawLine((int)startPositionCursor.getX(),(int) startPositionCursor.getY(),(int) currentPositionCursor.getX(),(int) startPositionCursor.getY());
//		g2d.drawLine((int)startPositionCursor.getX(),(int) startPositionCursor.getY(),(int) startPositionCursor.getX(),(int) currentPositionCursor.getY());
//		g2d.drawLine((int)currentPositionCursor.getX(),(int) startPositionCursor.getY(),(int) currentPositionCursor.getX(),(int) currentPositionCursor.getY());
//		g2d.drawLine((int)startPositionCursor.getX(),(int)currentPositionCursor.getY(),(int) currentPositionCursor.getX(),(int) currentPositionCursor.getY());
//	}
//	
//	private void drawAoiRectangle(Graphics2D g2d, boolean fill){
//
//		
//		Color c = new Color(255, 0, 0, 155);
//		g2d.setColor(c);
//	
//		if (fill)
//			g2d.fillRect((int)startAoi.getX(), (int)startAoi.getY(), (int)(endAoi.getX() - startAoi.getX()),(int) (endAoi.getY() - startAoi.getY()));
//		else 
//			g2d.drawRect((int)startAoi.getX(), (int)startAoi.getY(), (int)(endAoi.getX() - startAoi.getX()),(int)(endAoi.getY() - startAoi.getY()));
//		
//	}
//	
//	private void drawAoiEllipse(Graphics2D g2d){
//
//		
//		
//		Color c = new Color(255, 0, 0, 155);
//		g2d.setColor(c);
//	
//		g2d.fillOval((int)startAoi.getX(), (int)startAoi.getY(), (int)Math.abs(startAoi.getX() - endAoi.getX()),(int) Math.abs(startAoi.getY() - endAoi.getY()));
//		
//		
//	}
//	
//	public void startAnimation(ArrayList<Gaze> gazes){
//		
//		if (visMode == VisualizationMode.SCREEN_PLAY)
//			stopAnimation();
//		
//		visMode = VisualizationMode.SCREEN_PLAY;
//		
//		gazesToAnimate = gazes;
//		th = new Thread(this);
//		th.start();
//	}
//	
//	public void stopAnimation(){
//		
//		visMode = VisualizationMode.MAIN;
//		th = null;
//	    animationCounter = 0;
//	    repaint();
//	}
//	
//	public void pause(){
//		
//		visMode = VisualizationMode.SCREEN_PAUSE;
//		th = null;
//		repaint();
//	} 
//	
//	public void startVideo(){
//		
//		System.out.println("start video");
//	    th = new Thread(this);
//	    th.start();
//	}
//	
//	public void stopVideo() throws FileNotFoundException, IOException{
//		
//		System.out.println("stop video");
//        th = null;
//        currentGazeIndex = 0;
//        currentScreenIndex = 0;
//        currentGazeIndexOverall = 0;
//        loadScreen(GUI.getInstance().experiment.getTrials().get(currentTrialIndex).getScreens().get(currentScreenIndex).getFilename());
//        ExperimentMenu.getInstance().videoTime.setText("0");
//        ExperimentMenu.getInstance().timeSlider.setValue(ExperimentMenu.getInstance().timeSlider.getMinimum());
//        repaint();
//	}
//	
//	public boolean checkTimestamp(int timestampStart, int timestampEnd){
//		
//		return timestampStart <= maxTimeRange &&
//			   timestampStart >= minTimeRange &&
//			   timestampEnd <= maxTimeRange &&
//			   timestampEnd >= minTimeRange;
//	}
//	
//	
//	@Override
//	public void run() {
//		Thread thisThread = Thread.currentThread();
//		
//		switch (visMode) {
//		
//		case SCREEN_PLAY:
//			
//			while (animationCounter < gazesToAnimate.size()  && th == thisThread) {
//				
//				if (ExperimentMenu.getInstance().showGazes.getState()){
//					
//					if (gazesToAnimate.get(animationCounter).getClass().getName().contains("Gaze")){	
//						try {
//							Thread.sleep(4);
//							
//						} catch (InterruptedException e) {}	        	 
//					}	
//				}
//				else {
//					if (gazesToAnimate.get(animationCounter).getClass().getName().contains("Fixation")){	
//						try {
//							Thread.sleep((long)((Fixation) gazesToAnimate.get(animationCounter)).getDuration());
//						} catch (InterruptedException e) {}	        	 
//					}
//				}
//				
//				repaint();
//				++animationCounter;
//			}
//			break;
//
//		case VIDEO_PLAY:
//			
//			while(currentGazeIndexOverall < maxGazesToPlay && th == thisThread){
//				
//				//System.out.println(currentGazeIndex + " " + currentScreenIndex);
//				if(currentGazeIndex < videoGazes.get(currentScreenIndex).size()-1){
//					
//					try {
//						Thread.sleep(4);
//					} catch (InterruptedException e) {}	      
//				
//					++currentGazeIndex;
//				}
//				repaint();
//			}
//			visMode = VisualizationMode.VIDEO_PAUSE;
//			break;
//			
//		default:
//			break;
//		}
//	}

	public void stop(){
		
		th = null;
		trial = trialPlayIndex ;
	    screen = screenPlayIndex ;
	    //currentTimeStamp = experiment.getFirstTimeStamp();
	    playableGazes.clear();
	    gazeIndex = 0;
	    //frame.setTimeLine(experiment.getFirstTimeStamp());
	    
	    try {
			loadScreen(experiment.getTrials().get(trial).getScreens().get(screen).getFilename());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void pause(){
		
		
		th = null;
		repaint();
	} 
	
	public void play(){
		
		trialPlayIndex = trial;
	    screenPlayIndex = screen;
	    th = new Thread(this);
	    th.start();
	    
	    playFixations = experiment.isFixationAvailble();
	}
	
	private void playExperiment(Graphics2D g2d) {
		
		Screen currentScreen = experiment.getTrials().get(trialPlayIndex).getScreens().get(screenPlayIndex);

		
		if (playFixations){
			if (gazeIndex < currentScreen.getFixations().size()-1){
				
				if (playableGazes.size() == 4)
					playableGazes.remove(0);
				
				playableGazes.add(currentScreen.getFixations().get(gazeIndex));
				
				drawPlayableGazes(g2d);
				gazeIndex++;
				
			}
			else{
				
				gazeIndex = 0;
				if (screenPlayIndex < experiment.getTrials().get(trialPlayIndex).getScreens().size()-1)
					++screenPlayIndex;
				else if (trialPlayIndex < experiment.getTrials().size()-1)
					++trialPlayIndex;
				else 
					stop();
					
				try {
					loadScreen(experiment.getTrials().get(trialPlayIndex).getScreens().get(screenPlayIndex).getFilename());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		// play gazes
		else{
			if (gazeIndex < currentScreen.getGazes().size()-1){
				
				
				playableGazes.clear();
				playableGazes.add(currentScreen.getGazes().get(gazeIndex));
				
				drawPlayableGazes(g2d);
				gazeIndex++;
				
			}
			else{
				
				gazeIndex = 0;
				if (screenPlayIndex < experiment.getTrials().get(trialPlayIndex).getScreens().size()-1)
					++screenPlayIndex;
				else if (trialPlayIndex < experiment.getTrials().size()-1)
					++trialPlayIndex;
				else 
					stop();
					
				try {
					loadScreen(experiment.getTrials().get(trialPlayIndex).getScreens().get(screenPlayIndex).getFilename());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		
		// set timeline
		frame.setTimeLine(playableGazes.get(playableGazes.size()-1).getTimestampStart());
	}

	private void drawPlayableGazes(Graphics2D g2d) {
		
		LocationEvent old = null;
		int fixCounter = 0;
		for (LocationEvent g: playableGazes){
					
			g.draw(g2d, scaleX, scaleY);
			
			// draw scanpath
			if (playFixations){
				
				if (fixCounter > 0){
						
					g2d.drawLine((int) (old.getX()*scaleX ),
								 (int) (old.getY()*scaleY ),
								 (int) (g.getX()*scaleX ),
								 (int) (g.getY()*scaleY));
				}
				old = g;
				++fixCounter;
			}
		}
	}
	
	@Override
	public void run() {
		
		Thread thisThread = Thread.currentThread();
	
		while(currentTimeStamp < experiment.getLastTimeStamp() && th == thisThread){
			
			Screen currentScreen = experiment.getTrials().get(trialPlayIndex).getScreens().get(screenPlayIndex);
			try {
				if (playFixations)
					Thread.sleep(currentScreen.getFixations().get(gazeIndex).getDuration());
				else 
					Thread.sleep(currentScreen.getGazes().get(gazeIndex).getDuration());
			} catch (InterruptedException e) {}	      

			repaint();
		}
	}

	public void setMouseClickVisibility(boolean selected) {
		showMouseClick = selected;
		repaint();
	}
	
	public void setMousePositionVisibility(boolean selected) {
		showMousePosition = selected;
		repaint();
	}
	
}
