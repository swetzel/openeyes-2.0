package View.Experiment;

import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

import EventHandler.ExperimentFrameListener;
import EventHandler.ExperimentFrameToolBarListener;

public class ExperimentToolbar extends JToolBar {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int width = 800;
	private int height = 32;
	private ExperimentFrameToolBarListener listener;
	private ExperimentImagePanel imagePanel;
	
	private JToggleButton btnShowGazes;
	private JToggleButton btnShowFixations;
	private JToggleButton btnShowSaccades;
	private JToggleButton btnShowAois;
	private JToggleButton btnShowHeatmap;
	private JToggleButton btnShowMouseClick;
	private JToggleButton btnShowMouseMove;
	
	public ExperimentToolbar(ExperimentImagePanel imagePanel) {
		
		setFloatable(false);
		this.setSize(width, height);
		listener = new ExperimentFrameToolBarListener(this,imagePanel);
		this.imagePanel = imagePanel;
		
		
		btnShowGazes = new JToggleButton("");
		btnShowGazes.setToolTipText("Display Gazes");
		btnShowGazes.setIcon(new ImageIcon(getClass().getResource("/images/showGazes.png")));
		add(btnShowGazes);
		btnShowGazes.setActionCommand("Show Gazes");
		btnShowGazes.addActionListener(listener);
		
		btnShowFixations = new JToggleButton("");
		btnShowFixations.setToolTipText("Display Fixations");
		btnShowFixations.setIcon(new ImageIcon(getClass().getResource("/images/showFixations.png")));
		add(btnShowFixations);
		btnShowFixations.setActionCommand("Show Fixations");
		btnShowFixations.addActionListener(listener);
		
		btnShowSaccades = new JToggleButton("");
		//btnShowSaccades.setSelectedIcon(new ImageIcon(getClass().getResource("/images/showSaccades.png")));
		btnShowSaccades.setToolTipText("Display Saccades");
		btnShowSaccades.setIcon(new ImageIcon(getClass().getResource("/images/showSaccades.png")));
		add(btnShowSaccades);
		btnShowSaccades.setActionCommand("Show Saccades");
		btnShowSaccades.addActionListener(listener);
		
		btnShowAois = new JToggleButton("");
		btnShowAois.setIcon(new ImageIcon(getClass().getResource("/images/showAois.png")));
		//btnShowAois.setSelectedIcon(new ImageIcon(getClass().getResource("/images/showAois.png")));
		btnShowAois.setActionCommand("Show Aois");
		btnShowAois.setToolTipText("Display AOIs");
		add(btnShowAois);
		
		btnShowHeatmap = new JToggleButton("");
		btnShowHeatmap.setIcon(new ImageIcon(getClass().getResource("/images/heatmap.png")));
		add(btnShowHeatmap);
		btnShowHeatmap.setActionCommand("Show Heatmap");
		
		btnShowMouseClick = new JToggleButton("");
		btnShowMouseClick.setIcon(new ImageIcon(getClass().getResource("/images/showMouseClick.png")));
		btnShowMouseClick.setActionCommand("Show MouseClick");
		add(btnShowMouseClick);
		
		btnShowMouseMove = new JToggleButton("");
		btnShowMouseMove.setIcon(new ImageIcon(getClass().getResource("/images/showMouseMove.png")));
		btnShowMouseMove.setActionCommand("Show MouseMove");
		add(btnShowMouseMove);
		
		btnShowMouseClick.addActionListener(listener);
		btnShowMouseMove.addActionListener(listener);
		btnShowHeatmap.addActionListener(listener);
		btnShowAois.addActionListener(listener);
		
		
		
	}

	public void setEyeDataVisibility() {
		
		imagePanel.setEyeDataVisibility(btnShowGazes.isSelected(), btnShowFixations.isSelected(), btnShowSaccades.isSelected());
		
	}
	
	public void setAoiVisibility(){
		
		imagePanel.setAoiVisibility(btnShowAois.isSelected());
	}

	public void setHeatmapVisibility() {
		imagePanel.setHeatmapVisibility(btnShowHeatmap.isSelected());
	}

	public void setMouseClickVisibility() {
		imagePanel.setMouseClickVisibility(btnShowMouseClick.isSelected());
	}
	
	public void setMousePositionVisibility() {
		imagePanel.setMousePositionVisibility(btnShowMouseMove.isSelected());
	}
}
