package View.Experiment;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import EventHandler.PopUpListener;

public class PopUpExperimentFrame extends JPopupMenu{

	
	JMenuItem cognitiveLoad;
	
    public PopUpExperimentFrame(int subjectIndex, int experimentIndex){
    	
    	PopUpListener listener = new PopUpListener();
    	
    	cognitiveLoad = new JMenuItem("Display Cognitive Load");
        add(cognitiveLoad);
        cognitiveLoad.addActionListener(listener);
        cognitiveLoad.setActionCommand("load "+subjectIndex+" "+experimentIndex);
            
    }
}
