package View.Experiment;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyVetoException;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JInternalFrame;

import Data.Subject.Experiment;
import EventHandler.ExperimentFrameListener;

public class ExperimentFrame extends JInternalFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static int openFrameCount = 0;
	static final int xOffset = 30, yOffset = 30;
	
	private Experiment experiment;
	private ExperimentToolbar toolBar;
	private ExperimentControl controls;
	private ExperimentImagePanel imagePanel;
	
	//private ExperimentFrameListener listener;
	
	
	public ExperimentFrame(Experiment experiment, int maxWidth, int maxHeight) throws FileNotFoundException, IOException{

		 super("Experiment " + experiment.getName(),
		          true, //resizable
		          true, //closable
		          true, //maximizable
		          true);//iconifiable
		 try {
			setSelected(true);
		} catch (PropertyVetoException e) {
			
			e.printStackTrace();
		}
		 
		this.setMaximumSize(new Dimension(maxWidth,maxHeight));
		this.setSize(new Dimension(800, 600));
		setLocation(10,10);
		getContentPane().setLayout(null);    
		moveToFront(); 
		 
		 
		this.experiment = experiment;
		
		imagePanel = new ExperimentImagePanel(experiment, this);
		//listener = new ExperimentFrameListener(imagePanel);
		toolBar = new ExperimentToolbar(imagePanel);
		controls = new ExperimentControl(imagePanel);
		
		getContentPane().add(toolBar);
		toolBar.setBounds(0,0,toolBar.getWidth(), toolBar.getHeight());
		getContentPane().add(controls);
		controls.setBounds(0,this.getContentPane().getHeight()-toolBar.getHeight(),toolBar.getWidth(), toolBar.getHeight());
		getContentPane().add(imagePanel);
		imagePanel.setBounds(0, toolBar.getHeight(), this.getContentPane().getWidth(), this.getContentPane().getHeight()-toolBar.getHeight()-controls.getHeight());
		
		
		this.setMinimumSize(new Dimension(toolBar.getWidth(),600));
	    this.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				
				resizeElements();
				
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
	    
	}
	
	/*
	public void calculateFixations(int duration, int dispersion){
		
		experiment.calculateFixations(duration, dispersion);
		imagePanel.repaint();
	}*/
	
	public void recalculateHeatMap(){
		imagePanel.computeHeatmap();
	}
	
	public void updateHeatmap(){
		imagePanel.drawHeatmap();
	}

	public void initImagePanel() throws FileNotFoundException, IOException{
		imagePanel.initLayers();
	}
	
	public void setTimeLine(long timeStamp){
		controls.setTimeLine(timeStamp);
	}
	
	public void resizeElements(){
		
		controls.resize(this.getContentPane().getHeight()-toolBar.getHeight(),this.getContentPane().getWidth(), toolBar.getHeight());
		imagePanel.setBounds(0, toolBar.getHeight(), this.getContentPane().getWidth(), this.getContentPane().getHeight()-toolBar.getHeight()-controls.getHeight());
	}
	
}
