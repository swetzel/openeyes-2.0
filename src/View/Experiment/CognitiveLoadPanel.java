package View.Experiment;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

import Analysis.CognitiveLoad;
import Data.Subject.Experiment;

public class CognitiveLoadPanel extends JPanel {

	private int width;
	private int height;
	private int diagramHeight;
	private int diagramWidth;
	private int heightOffset = 5;
	private int widthOffset = 20;
	
	private Experiment experiment = null;
	private boolean drawTLXScore = true;
	private boolean drawMentalDemand = false;
	private boolean drawPhysicalDemand = false;
	private boolean drawTemporalDemand = false;
	private boolean drawEffort = false;
	private boolean drawPerformance = false;
	private boolean drawFrustration = false;
	private boolean drawLeftPupil = false;
	private boolean drawRightPupil = false;
	
	public CognitiveLoadPanel(int width, int height, Experiment experiment){
		
		this.setBackground(Color.white);
		this.width = width;
		this.height = height;
		diagramHeight = height - heightOffset;
		diagramWidth = width - widthOffset;
		this.setBounds(10, 130, width, height);
		this.experiment = experiment;
	}
	
	public void paintComponent(Graphics g){
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		

    	g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
    	g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
    			RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		
    	
    	drawDiagram(g2d);
    	
    	if (experiment != null){
	    	CognitiveLoad load = experiment.getCognitiveLoad();
	    	
	    	if (drawEffort){
	    		g2d.drawLine(widthOffset,(int) (load.getEffortNormalized()*diagramHeight), diagramWidth+widthOffset,(int) (load.getEffortNormalized()*diagramHeight));
	    	}
	    	if (drawFrustration){
	    		g2d.drawLine(widthOffset,(int) (load.getFrustrationNormalized()*diagramHeight), diagramWidth+widthOffset,(int) (load.getFrustrationNormalized()*diagramHeight));
	    	}
	    	if (drawMentalDemand){
	    		g2d.drawLine(widthOffset,(int) (load.getMentalDemandNormalized()*diagramHeight), diagramWidth+widthOffset,(int) (load.getMentalDemandNormalized()*diagramHeight));
	    	}
	    	if (drawPerformance){
	    		g2d.drawLine(widthOffset,(int) (load.getPerformanceNormalized()*diagramHeight), diagramWidth+widthOffset,(int) (load.getPerformanceNormalized()*diagramHeight));
	    	}
	    	if (drawPhysicalDemand){
	    		g2d.drawLine(widthOffset,(int) (load.getPhysicalDemandNormalized()*diagramHeight), diagramWidth+widthOffset,(int) (load.getPhysicalDemandNormalized()*diagramHeight));
	    	}
	    	if (drawTemporalDemand){
	    		g2d.drawLine(widthOffset,(int) (load.getTemporalDemandNormalized()*diagramHeight), diagramWidth+widthOffset,(int) (load.getTemporalDemandNormalized()*diagramHeight));
	    	}
	    	if (drawTLXScore){
	    		g2d.drawLine(widthOffset,(int) (load.getTLXScoreNormalized()*diagramHeight), diagramWidth+widthOffset,(int) (load.getTLXScoreNormalized()*diagramHeight));
	    	}
	    	
	    	if (drawLeftPupil){
	    		g2d.setColor(Color.red);
	    		experiment.drawPupilSize(g2d, diagramWidth, diagramHeight, widthOffset, heightOffset,1);
	    		//experiment.drawPupilAverage(g2d, diagramWidth, diagramHeight, widthOffset, heightOffset,1);
	    	}
	    	
	    	if (drawRightPupil){
	    		g2d.setColor(Color.green);
	    		experiment.drawPupilSize(g2d, diagramWidth, diagramHeight, widthOffset, heightOffset,2);
	    	}
	    	
	    	g2d.setColor(Color.blue);
	    	experiment.drawBlinks(g2d, diagramWidth, diagramHeight, widthOffset, heightOffset);
	    	
	    	g2d.setColor(Color.black);
	    	experiment.drawEvents(g2d, diagramWidth, diagramHeight, widthOffset, heightOffset);
	    	//experiment.drawPupil(g2d, diagramWidth, diagramHeight, widthOffset, heightOffset, 1);
    	}	
		g2d.dispose();
    	
	}
	
	private void drawDiagram(Graphics2D g2d){
		
		g2d.drawLine(widthOffset, 5, widthOffset, height-heightOffset);
		g2d.drawLine(widthOffset, height-heightOffset, width+widthOffset, height-heightOffset);
		g2d.drawString("1", 0, 10);
		g2d.drawString("0.5", 0, diagramHeight/2+heightOffset);
		g2d.drawString("0", 0, diagramHeight+heightOffset);
		
	}

	public void setDrawTLXScore(boolean drawTLXScore) {
		this.drawTLXScore = drawTLXScore;
	}
	
	public void setDrawRightPupil(boolean drawRightPupil) {
		this.drawRightPupil = drawRightPupil;
	}
	
	public void setDrawLeftPupil(boolean drawLeftPupil) {
		this.drawLeftPupil = drawLeftPupil;
	}
	
	public void setDrawMentalDemand(boolean drawMentalDemand) {
		this.drawMentalDemand = drawMentalDemand;
	}

	public void setDrawPhysicalDemand(boolean drawPhysicalDemand) {
		this.drawPhysicalDemand = drawPhysicalDemand;
	}

	public void setDrawTemporalDemand(boolean drawTemporalDemand) {
		this.drawTemporalDemand = drawTemporalDemand;
	}

	public void setDrawEffort(boolean drawEffort) {
		this.drawEffort = drawEffort;
	}

	public void setDrawPerformance(boolean drawPerformance) {
		this.drawPerformance = drawPerformance;
	}

	public void setDrawFrustration(boolean drawFrustration) {
		this.drawFrustration = drawFrustration;
	}
	
	
	
}
