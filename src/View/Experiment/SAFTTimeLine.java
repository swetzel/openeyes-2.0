package View.Experiment;

import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JSlider;

import Data.Subject.Experiment;
import Data.Subject.Screen;
import Data.Subject.Trial;
import EventHandler.TimeLineListener;

import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class SAFTTimeLine extends JSlider{
	
	private static final long serialVersionUID = 1L;
	private Hashtable labelTable;
	private int width = 480;
	
	
	public SAFTTimeLine(ExperimentImagePanel panel) {
		
		setPaintLabels(true);
		this.setSize(width, 30);
		createLables(panel.getExperiment());
		this.addChangeListener(new TimeLineListener(panel));
	}

	
	private void createLables(Experiment experiment){
		
		Screen start = experiment.getStartScreen();
		Screen end = experiment.getLastScreen();
		
		this.setMinimum((int) start.getTimestampStart());
		this.setMaximum((int) end.getTimestampEnd());

		//Create the label table
		labelTable = new Hashtable();
		
		int trials = 0;
		Font trialFont = new Font("Tahoma", Font.PLAIN, 10);
		Font screenFont = new Font("Tahoma", Font.PLAIN, 8);
		
		for (Trial t : experiment.getTrials()){
			
			JLabel newTrialLabel = new JLabel("T"+trials);
			newTrialLabel.setFont(trialFont);
			newTrialLabel.setBorder(BorderFactory.createEmptyBorder ( -1, 0, 0, 0 ) );
			labelTable.put( new Integer( (int) t.getStartTime()), newTrialLabel);
			
			int screens = 0;
			for(Screen s : t.getScreens()){
				
				JLabel newScreenLabel = new JLabel("S"+screens);
				newScreenLabel.setFont(screenFont);
				newScreenLabel.setBorder(BorderFactory.createEmptyBorder ( -8, 0, 0, 0 ) );
				labelTable.put( new Integer( (int) (s.getTimestampStart()+1) ), newScreenLabel);
				screens++;
			}
			trials++;
		}
		this.setLabelTable( labelTable );
	}
	
	public void resize(int width){
	
		this.setSize(width, 30);
	}
	
	public void setSlider(long timestamp){
		
		if (timestamp != 0)
			this.setValue((int) timestamp);
	}
	
}
