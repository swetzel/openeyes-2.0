package View.Menu;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;

import Data.FilteringProperty;
import Data.Study.Property;

import Data.Subject.Experiment;
import Data.SortingProperty;

import java.awt.FlowLayout;

import javax.swing.border.BevelBorder;
import javax.swing.JCheckBox;

import View.GUI;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExperimentPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Experiment experiment;
	private String name;
	
	
	public ExperimentPanel(Experiment ex){
		
		setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
	
		experiment = ex;
		this.name = ex.getName();
		this.setSize(173, 30 + ex.getProperties().size()*17);
		setLayout(null);
		JLabel label = new JLabel(name);
		label.setFont(new Font("Tahoma", Font.BOLD, 11));
		label.setLocation(10, 9);
		label.setSize(80, 14);
		label.setBackground(Color.WHITE);
		this.add(label);	
		setBackground(Color.white);
		
		JCheckBox chckbxActive = new JCheckBox("active");
		chckbxActive.setBackground(Color.WHITE);
		chckbxActive.setBounds(119, 5, 55, 23);
		chckbxActive.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				//experiment.setEnabled(chckbxActive.isSelected());
				
				GUI.getInstance().updateStudy(experiment.getId(),chckbxActive.isSelected());
				
			}
		});
		add(chckbxActive);
		
		int y = 30;
		for (Property a : ex.getProperties()){
			
			JLabel attLabel;
			
			/*
			if (a.getType() == PropertyType.FILTER)
				attLabel = new JLabel(a.getName() + ": "+((FilteringProperty) a).getValue());
			else 
				attLabel = new JLabel(a.getName() + ": "+((SortingProperty) a).getValue());
			
			attLabel.setBounds(10, y, 173, 14);
			this.add(attLabel);*/
			y = y + 16;
		}
		
		
	}
	
	
	
	public String getName(){
		return name;
	}
	
	
	public Experiment getExperiment(){
		return experiment;
	}
	
}
