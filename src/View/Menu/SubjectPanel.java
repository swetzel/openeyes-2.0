package View.Menu;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import Data.Subject.Subject;

public class SubjectPanel extends JPanel{

	private Subject subject;
	private ArrayList<ExperimentPanel> experimentPanels = new ArrayList<ExperimentPanel>();
	private ArrayList<ExperimentPanel> filteredExperimentPanels = new ArrayList<ExperimentPanel>();
	private int offset = 30;
	
	public SubjectPanel(Subject s){
		
		subject = s;
		setLayout(null);
		JLabel label = new JLabel(s.getName());
		//System.out.println(s.getName());
		label.setFont(new Font("Tahoma", Font.BOLD, 11));
		label.setLocation(3, 3);
		this.setSize(173,30);
		this.add(label);
		this.setBackground(Color.lightGray);
		
		
	}
	
	public int getOffset(){
		return offset;
	}
	
	public void updateSize(){
		
		int height = 0;
		
		for (ExperimentPanel exPanel: experimentPanels){
			height += exPanel.getHeight();
		}
		
		this.setSize(173, offset + height);
	}
	
	public void addExperimentPanel(ExperimentPanel newExperiment){
		
		this.add(newExperiment);
		experimentPanels.add(newExperiment);
		filteredExperimentPanels.add(newExperiment);
	}
}
