package View.Menu;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.swing.JTabbedPane;

import View.GUI;
import Data.FilteringProperty;
import Data.PropertyFilterValues;
import Data.ExperimentFilter.Quality;
import Data.Study.Property;
import Data.Study.Study;
import Data.Subject.Experiment;
import Data.PropertySortingValues;
import EventHandler.MenuActionListener;

import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import java.awt.Font;

import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;

public class Menu extends JPanel{

	private static Menu instance = new Menu();
	private int height = 100;
	private int width = 200;
	private JPanel studyPanel;
	private JTabbedPane tabbedPane;
	private JPanel heatmap;
	private JPanel cluster;
	private ArrayList<SubjectPanel> subjectPanels = new ArrayList<SubjectPanel>();
	private JComboBox<String> filterNameBox;
	private JComboBox<String> filterBox;
	private JComboBox<String> sortingBox;
	private ImageIcon icon1;
	private ImageIcon icon2;
	private JToggleButton orderToggleButton;
	private boolean reverseOrder = false;
	private JRadioButton rdbtnFixations;
	private JRadioButton rdbtnGazes;
	private JSpinner spinnerKernelSize;
	private JSpinner spinnerGridSize;
	private JProgressBar progressBar;
	private JCheckBox chckbxComputeForAll;
	private JPanel aois;
	private JButton btnExport;
	private JPanel fixationDetection;
	private JButton btnCalc;
	private JSpinner dispersionThreshold;
	private JSpinner durationThreshold;
	
	
	private Menu(){

		setLayout(null);
		
		studyPanel = new JPanel();
		studyPanel.setBounds(0, 0, 190, 500);
		studyPanel.setAutoscrolls(true);
		studyPanel.setLayout(null);
		
		JScrollPane scroller = new JScrollPane(studyPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroller.setSize(198, 395);
		scroller.setLocation(1, 80);
		scroller.setPreferredSize(new Dimension(200, 395));
		this.add(scroller);
		
		filterNameBox = new JComboBox<>();
		filterNameBox.setToolTipText("Filter");
		filterNameBox.setBounds(29, 5, 169, 20);
		filterNameBox.setActionCommand("prop");
		this.add(filterNameBox);
		
		filterBox = new JComboBox<>();
		filterBox.setToolTipText("Filter");
		filterBox.setBounds(69, 30, 129, 20);
		filterBox.setActionCommand("filter");
		this.add(filterBox);
		
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(1, 480, 198, 289);
		add(tabbedPane);
		
		
		heatmap = new JPanel();
		heatmap.setToolTipText("Heatmap Settings");
		heatmap.setBackground(Color.white);
		tabbedPane.addTab("", new ImageIcon(getClass().getResource("/images/heatmap2.png")), heatmap, "Heatmap Settings");
		heatmap.setLayout(null);
		
		spinnerKernelSize = new JSpinner();
		spinnerKernelSize.setModel(new SpinnerNumberModel(new Integer(25), new Integer(1), null, new Integer(1)));
		spinnerKernelSize.setBounds(70, 40, 41, 20);
		heatmap.add(spinnerKernelSize);
		
		JLabel lblHeatmapSettings = new JLabel("Heatmap Settings");
		lblHeatmapSettings.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblHeatmapSettings.setBounds(10, 11, 121, 14);
		heatmap.add(lblHeatmapSettings);
		
		JLabel lblKernelSize = new JLabel("Kernel Size");
		lblKernelSize.setBounds(10, 43, 64, 14);
		heatmap.add(lblKernelSize);
		
		JLabel lblGridSize = new JLabel("Grid Size");
		lblGridSize.setBounds(10, 71, 46, 14);
		heatmap.add(lblGridSize);
		
		spinnerGridSize = new JSpinner();
		spinnerGridSize.setModel(new SpinnerNumberModel(new Integer(5), new Integer(1), null, new Integer(1)));
		spinnerGridSize.setBounds(70, 68, 41, 20);
		heatmap.add(spinnerGridSize);
		
		JLabel lblInputData = new JLabel("Input Data:");
		lblInputData.setBounds(10, 116, 74, 14);
		heatmap.add(lblInputData);
		
		rdbtnGazes = new JRadioButton("Gazes");
		rdbtnGazes.setBounds(10, 137, 109, 23);
		rdbtnGazes.setBackground(Color.white);
		heatmap.add(rdbtnGazes);
		
		rdbtnFixations = new JRadioButton("Fixations");
		rdbtnFixations.setBackground(Color.white);
		rdbtnFixations.setSelected(true);
		rdbtnFixations.setBounds(10, 163, 109, 23);
		heatmap.add(rdbtnFixations);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnFixations);
		group.add(rdbtnGazes);
		
		JButton btnApplyHeatMap = new JButton("Apply");
		btnApplyHeatMap.setBounds(10, 227, 89, 23);
		btnApplyHeatMap.setActionCommand("apply heatmap");
		heatmap.add(btnApplyHeatMap);
		
		chckbxComputeForAll = new JCheckBox("Compute for all screens");
		chckbxComputeForAll.setBackground(Color.WHITE);
		chckbxComputeForAll.setBounds(10, 189, 173, 23);
		heatmap.add(chckbxComputeForAll);
		
		btnApplyHeatMap.addActionListener(MenuActionListener.getInstance());
		
		cluster = new JPanel();
		cluster.setBackground(Color.WHITE);
		tabbedPane.addTab("", new ImageIcon(getClass().getResource("/images/cluster.png")), cluster, null);
		cluster.setLayout(null);
		
		
		
		aois = new JPanel();
		aois.setToolTipText("Area of Interest Settings");
		aois.setBackground(Color.WHITE);
		tabbedPane.addTab("", new ImageIcon(getClass().getResource("/images/aois.png")), aois, null);
		aois.setLayout(null);
		
		JLabel lblAreasOfInterest = new JLabel("Areas of Interest");
		lblAreasOfInterest.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAreasOfInterest.setBounds(10, 11, 111, 14);
		aois.add(lblAreasOfInterest);
		
		JPanel exportPanel = new JPanel();
		tabbedPane.addTab("", new ImageIcon(getClass().getResource("/images/aois.png")), exportPanel, null);
		exportPanel.setLayout(null);
		
		btnExport = new JButton("Export");
		btnExport.addActionListener(MenuActionListener.getInstance());
		btnExport.setBounds(60, 108, 65, 23);
		btnExport.setActionCommand("export");
		exportPanel.add(btnExport);
		
		fixationDetection = new JPanel();
		tabbedPane.addTab("", null, fixationDetection, null);
		fixationDetection.setLayout(null);
		
		btnCalc = new JButton("Calculate");
		btnCalc.setBounds(10, 217, 77, 23);
		fixationDetection.add(btnCalc);
		btnCalc.setActionCommand("detect fixations");
		
		JPanel fixationMethodPanel = new JPanel();
		fixationMethodPanel.setBounds(10, 60, 173, 114);
		fixationDetection.add(fixationMethodPanel);
		fixationMethodPanel.setLayout(null);
		
		durationThreshold = new JSpinner();
		durationThreshold.setModel(new SpinnerNumberModel(150, 70, 300, 10));
		durationThreshold.setBounds(0, 11, 43, 20);
		fixationMethodPanel.add(durationThreshold);
		
		JLabel lblDurationThreshold = new JLabel("min duration in ms");
		lblDurationThreshold.setBounds(53, 14, 95, 14);
		fixationMethodPanel.add(lblDurationThreshold);
		
		dispersionThreshold = new JSpinner();
		dispersionThreshold.setModel(new SpinnerNumberModel(new Integer(36), null, null, new Integer(1)));
		dispersionThreshold.setBounds(0, 36, 43, 20);
		fixationMethodPanel.add(dispersionThreshold);
		
		JLabel lblDispersionThresholdIn = new JLabel("min dispersion in pixel");
		lblDispersionThresholdIn.setBounds(53, 39, 128, 14);
		fixationMethodPanel.add(lblDispersionThresholdIn);
		
		JComboBox comboBoxFixationMethod = new JComboBox();
		comboBoxFixationMethod.setModel(new DefaultComboBoxModel(new String[] {"Dispersion-Threshold Identification"}));
		comboBoxFixationMethod.setBounds(10, 29, 173, 20);
		fixationDetection.add(comboBoxFixationMethod);
		
		JLabel lblFixationDetection = new JLabel("Fixation Detection");
		lblFixationDetection.setBounds(10, 11, 109, 14);
		fixationDetection.add(lblFixationDetection);
		
		JCheckBox chckbxCurrentScreen = new JCheckBox("only current screen");
		chckbxCurrentScreen.setSelected(true);
		chckbxCurrentScreen.setBounds(6, 181, 151, 23);
		fixationDetection.add(chckbxCurrentScreen);
		btnCalc.addActionListener(MenuActionListener.getInstance());
				
		sortingBox = new JComboBox<String>();
		sortingBox.setToolTipText("Sort Experiments");
		sortingBox.setBounds(29, 55, 169, 20);
		sortingBox.setActionCommand("sort");;
		add(sortingBox);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(getClass().getResource("/images/filter.png")));
		label.setBounds(3, 5, 21, 21);
		add(label);
		
		icon1 = new ImageIcon(getClass().getResource("/images/sort.png"));
		icon2 = new ImageIcon(getClass().getResource("/images/sort2.png"));
		orderToggleButton = new JToggleButton("");
		orderToggleButton.setIcon(icon2);
		orderToggleButton.setActionCommand("toggle order");
		orderToggleButton.setBounds(2, 54, 25, 23);
		add(orderToggleButton);
		
		progressBar = new JProgressBar(0,100);
		progressBar.setBounds(0, 770, 198, 14);
		add(progressBar);
		orderToggleButton.addActionListener(MenuActionListener.getInstance());
	}
	
	
	
	public void updateMenu(Study study, int panelHeight){
		
		studyPanel.setPreferredSize(new Dimension(195, panelHeight));
		
		// update filter
		for (PropertyFilterValues prop: study.getPropertyFilterValues())
			filterNameBox.addItem(prop.getName());
			
		filterNameBox.setSelectedIndex(-1);
		filterNameBox.addActionListener(MenuActionListener.getInstance());
		
		// update sorting
		for (PropertySortingValues prop : study.getPropertySortingValues())
			sortingBox.addItem(prop.getName());
			
		sortingBox.setSelectedIndex(-1);
		sortingBox.addActionListener(MenuActionListener.getInstance());
		
	}
	
	
	
	public void updateFilter(String filter){
		
		for(ActionListener act : filterBox.getActionListeners()) {
		    filterBox.removeActionListener(act);
		}
		
		filterBox.removeAllItems();
		filterBox.addItem("no filter");
		for (PropertyFilterValues props : GUI.getInstance().getStudy().getPropertyFilterValues()){
		
			if (props.getName() == filter){
				for (String s : props.getValues())
					filterBox.addItem(s);
			}
		}
		filterBox.setSelectedIndex(-1);
		filterBox.addActionListener(MenuActionListener.getInstance());
	}
	
	public String getFilterName(){
		return (String)filterNameBox.getSelectedItem();
	}
	
	public String getFilterValue(){
		return (String)filterBox.getSelectedItem();
	}
	
	public static Menu getInstance() {
        return instance;
    }
	
	public void resize(int width, int height){
		
		this.height = height;
		setBounds(width - this.width, 0, this.width, this.height);
	}
	
	public void clearPanels(){
		studyPanel.removeAll();
		subjectPanels.clear();
	}
	
	public void addSubjectPanel(SubjectPanel newSubject){
		
		studyPanel.add(newSubject);
		subjectPanels.add(newSubject);
		//filteredExperimentPanels.add(newExperiment);
	}
	
	/*
	public void addExperimentPanel(ExperimentPanel newExperiment){
		
		studyPanel.add(newExperiment);
		experimentPanels.add(newExperiment);
		filteredExperimentPanels.add(newExperiment);
	}
	 */
	public boolean isFilterActive(){
		if (filterBox.getSelectedIndex() == -1)
			return false;
		else return true;
	}
	
	public void filterExperiments(String filter, String value) {
		
		/*
		filteredExperimentPanels.clear();
		
		if (value.equals("no filter"))
			experimentPanels.forEach(panel -> filteredExperimentPanels.add(panel));
		
		else{
			for (ExperimentPanel panel : experimentPanels){
			
				for (FilteringProperty prop :panel.getExperiment().getFilters()){
					if (prop.getName().equals(filter)){
						
						//if(((FilteringProperty) prop).getValue().equals(value)){
							//filteredExperimentPanels.add(panel);
							
						//}
						
					}	
				}	
			}
		}
		if (sortingBox.getSelectedIndex() != -1){
			sortExperiments((String)sortingBox.getSelectedItem());
			
		}

		else{
			displayPanels();
			this.revalidate();
			this.repaint();
		}
		*/
	}
	
	private void displayPanels() {
		
		studyPanel.removeAll();
		int offset = 2;
		
		//for (ExperimentPanel panel : filteredExperimentPanels){
		for (SubjectPanel panel : subjectPanels){
			studyPanel.add(panel);
			panel.setLocation(2, offset);
			offset = offset + panel.getHeight();
		}
		studyPanel.setPreferredSize(new Dimension(195, offset));
	}

	public void sortExperiments(String prop){
		
		/*
		Collections.sort(filteredExperimentPanels, (p1,p2) -> 
		new Float(p1.getExperiment().getSortingProperty(prop).getValue()).compareTo(new Float(p2.getExperiment().getSortingProperty(prop).getValue())));
		
		if (reverseOrder)	
			Collections.reverse(filteredExperimentPanels);
		*/
		displayPanels();
		this.revalidate();
		this.repaint();
	}


	public void toogleSortingOrder() {
		
		if (orderToggleButton.isSelected()){
			orderToggleButton.setIcon(icon1);
			reverseOrder = true;
		}
		else{
			orderToggleButton.setIcon(icon2);
			reverseOrder = false;
		}
		
		if (sortingBox.getSelectedIndex() != -1)
			sortExperiments((String)sortingBox.getSelectedItem());

		
	}
	
	public boolean computeForAllScreens(){
		
		return chckbxComputeForAll.isSelected();
	}
	
	public int getInputTypeForHeatmap(){
		
		if (rdbtnFixations.isSelected())
			return 1;
		else if (rdbtnGazes.isSelected())
			return 2;
		else 
			return 0;
	}
	
	public int getGridSize() {
		
		return (int) spinnerGridSize.getValue();
	}
	

	public int getKernelSize() {
		
		return (int) spinnerKernelSize.getValue();
	}

	

	public void setUpProgressBar(int min, int max) {
		
		progressBar.setMinimum(min);
		progressBar.setMaximum(max);
		
	}
	
	public void setProgress(int progress){
		progressBar.setValue(progress);
		
	}
	
	public int getDurationThreshold(){
		
		return (int) durationThreshold.getValue();
	}

	public int getDispersionThreshold(){
		
		return (int) dispersionThreshold.getValue();
	}
}
