package View.Menu;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import EventHandler.PopUpListener;

public class PopUpMenu extends JPopupMenu {
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JMenuItem experiment;
	JMenuItem study;
	JMenuItem cognitiveLoad;
	
    public PopUpMenu(int subjectIndex, int experimentIndex, boolean useLoad){
    	
    	PopUpListener listener = new PopUpListener();
    	
        experiment = new JMenuItem("Display Experiment");
        add(experiment);
        experiment.addActionListener(listener);
        experiment.setActionCommand("experiment "+ subjectIndex+" "+experimentIndex);
        
        study = new JMenuItem("Display Study");
        add(study);
        study.addActionListener(listener);
        study.setActionCommand("study");
            	
        if (useLoad){
	    	cognitiveLoad = new JMenuItem("Display Cognitive Load");
	        add(cognitiveLoad);
	        cognitiveLoad.addActionListener(listener);
	        cognitiveLoad.setActionCommand("load "+subjectIndex+" "+experimentIndex);
  
        }
    }
}
