/**
 * 
 */
package Data;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import Data.Cluster;
import Events.Default.Fixation;
import Events.Default.Gaze;


/**
 * @author Stefanie Wetzel
 *
 */
public class ConvexHull {
	
	public Vector<Gaze> hull;
	
	public ConvexHull(){
		
	}

	/**
	 * Implementation of Andrew's monotone chain 2D convex hull algorithm.
	 * 
	 * @param gazes from one cluster
	 * @return convex hull of the cluster
	 */
	public void calculateConvexHull(Cluster cluster){
		
		
		
		/*
		Collections.sort(cluster.getGazes(), new Comparator<Gaze>() {
			   
			@Override
			public int compare(Gaze arg0, Gaze arg1) {
				
				if (arg0.getX() < arg1.getX())
					return -1;
				else if (arg0.getX() > arg1.getX())
					return 1;
				return 0;
			}
			
		});
		*/
		//Collator collator = Collator.getInstance();
		//cluster.getGazes().sort(comparing(getX()));
		// FIXME sort gazes by x-coordinate
		
		
		int n = cluster.getGazes().size();
		int k = 0;
	    
		
		Vector<Gaze> output = new Vector<Gaze>(n);
		
		// Build lower hull
	    for (int i = 0; i < n; i++) {
	         while (k >= 2 && ccw(output.elementAt(k-2), output.elementAt(k-1), cluster.getGazes().elementAt(i)) <= 0) 
	        	 k--;       
	         output.add(k++, cluster.getGazes().elementAt(i));
	        }
	 
        // Build upper hull
        for (int i = n-2, t = k+1; i >= 0; i--) {
                while (k >= t && ccw(output.elementAt(k-2), output.elementAt(k-1), cluster.getGazes().elementAt(i)) <= 0) 
                	k--;
                output.add(k++, cluster.getGazes().elementAt(i));
        }
		
		output.setSize(k);
		
       hull = output;
	}
	
	private double ccw(Gaze p1, Gaze p2, Gaze p3){
	
	    return (p2.getX() - p1.getX())*(p3.getY() - p1.getY()) - (p2.getY() - p1.getY())*(p3.getX() - p1.getX());
	}
	
	public void draw(Graphics2D g2d, double scaleX, double scaleY){
		
		g2d.setStroke(new BasicStroke(5));
		
		for (int i = 0; i < hull.size(); ++i){
			
			if (i+1 >= hull.size())
				g2d.drawLine((int) (hull.get(i).getX() * scaleX), 
							 (int) (hull.get(i).getY() * scaleY), 
							 (int) (hull.get(0).getX() * scaleX), 
							 (int) (hull.get(0).getY() * scaleY));
			else
				g2d.drawLine((int) (hull.get(i).getX() * scaleX), 
							 (int) (hull.get(i).getY() * scaleY), 
							 (int) (hull.get(i+1).getX() * scaleX), 
							 (int) (hull.get(i+1).getY() * scaleY));
		}
		g2d.setStroke(new BasicStroke(1));
	}
	
}
