package Data;

import java.awt.Graphics2D;
import java.util.Vector;

import Events.Default.Gaze;

public class Cluster {

	private Vector<Gaze> gazes = new Vector<Gaze>();
	private ConvexHull hull = new ConvexHull();
	
	public Cluster(Vector<Gaze> gazes){
		this.gazes = gazes;
	}
	
	public Cluster(){
		
	}
	
	public void clear(){
		gazes.clear();
	}
	
	public void add(Gaze g){
		gazes.add(g);
	}
	
	public Vector<Gaze> getGazes(){
		return gazes;
	}
	
	public void draw(Graphics2D g2d, double scaleX, double scaleY){
		
		for (Gaze g : gazes){
			g2d.fillRect((int) (g.getX() * scaleX),(int) (g.getY() * scaleY),2, 2);	
		}
		//System.out.println(gazes.size());
		hull.calculateConvexHull(this);
		hull.draw(g2d, scaleX, scaleY);
	}
	
}
