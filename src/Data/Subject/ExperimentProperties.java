package Data.Subject;

public class ExperimentProperties {
	
	public enum ExperimentType{
		
		BTF,
		PC,
		HEX,
		ELEARNING
	}
	
	protected ExperimentType experimentType;

	public ExperimentType getExperimentType() {
		return experimentType;
	}

	public void setExperimentType(ExperimentType experimentType) {
		this.experimentType = experimentType;
	}
	
	
}
