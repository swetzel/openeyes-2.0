package Data.Subject;

public class PropertyValue {

	private String name;
	private double value;
	
	public PropertyValue(String name, double value){
		
		this.name = name;
		this.value = value;
	}
	
	
	public String getName(){
		return name;
	}
	
	public double getValue(){
		return value;
	}
}
