package Data.Subject;

public class DriftCorrection {

	private int driftX = 0;
	private int driftY = 0;
	
	public DriftCorrection(){
		
	}
	
	public DriftCorrection(int x, int y){
		
		driftX = x;
		driftY = y;
	}
	
	public int getDriftX() {
		return driftX;
	}

	public void setDriftX(int driftX) {
		this.driftX = driftX;
	}

	public int getDriftY() {
		return driftY;
	}

	public void setDriftY(int driftY) {
		this.driftY = driftY;
	}
	
	public void addDriftX(int driftX){
		this.driftX += driftX;
	}
	
	public void addDriftY(int driftY){
		this.driftY += driftY;
	}
		
}
