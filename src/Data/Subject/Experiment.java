package Data.Subject;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.File;
import java.util.ArrayList;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import Analysis.BlinkDetection;
import Analysis.CognitiveLoad;
import Analysis.FixationDetection;
import Data.AreaOfInterest;
import Data.FilteringProperty;
import Data.SortingProperty;
import Data.ExperimentFilter.Quality;
import Data.Study.Property;
import Events.Default.Blink;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.Saccade;

/**
 * 
 * @author Stefanie Wetzel <stefanie.wetzel@gmail.com>
 *
 */
public class Experiment {

	private ArrayList<Trial> trials = new ArrayList<Trial>();
	private String path;
	private String name;
	private boolean enabled;
	private int duration;
	private int screenHeight;
	private int screenWidth;
	private int id;
	private ArrayList<Property> properties = new ArrayList<Property>();
	private boolean fixationAvailble;
	private CognitiveLoad cognitiveLoad = new CognitiveLoad();
	private PCCondition condition;
	private int PCTask;
	private DescriptiveStatistics pupilSizes = new DescriptiveStatistics();
	private ExperimentProperties experimentProperties; 

	
	public enum PCCondition{
		
		SINGLE_AXIS_ZOOM,
		MULTI_AXIS_ZOOM,
		SINGLE_AXIS_ZOOM_OVERVIEW,
		MULTI_AXIS_ZOOM_OVERVIEW,
	}
	

	public Experiment (String path, boolean enable, int id, boolean fix){
		enabled = enable;
		this.path = path;
		this.id = id;
		setFixationAvailble(fix);
	}

	public int getCondition() {
		
		switch (condition) {
		
		case SINGLE_AXIS_ZOOM:
			return 1;
		
		case MULTI_AXIS_ZOOM:
			return 2;
		case SINGLE_AXIS_ZOOM_OVERVIEW:
			return 3;
		case MULTI_AXIS_ZOOM_OVERVIEW:
			return 4;
		default:
			return -1;
		}
		
	}

	public void setCondition(PCCondition condition) {
		this.condition = condition;
	}

	
	public int getId(){
		return id;
	}

	public void addProperty(Property a){
		properties.add(a);
	}
	
	public ArrayList<Property> getProperties(){
		return properties;
	}
	
	/**
	 * 
	 * @return Experiment duration in ms
	 */
	public long getExperimentDuration(){
		
		return (getLastTimeStamp() - getFirstTimeStamp());
	}
	
	public long getElapsedTime(long timeStamp){
		
		return timeStamp - getFirstTimeStamp();
	}
	
	public long getLastTimeStamp(){
		
		return getLastScreen().getTimestampEnd();
	}
	
	public long getFirstTimeStamp(){
		
		return getStartScreen().getTimestampStart();
	}
	
	public Screen getStartScreen(){
		
		return trials.get(0).getScreens().get(0);
	}
	
	public Screen getLastScreen(){
		
		return trials.get(trials.size()-1).getScreens().get(trials.get(trials.size()-1).getScreens().size()-1);
	}
	/*
	public Quality getQuality(){
		return quality;
	}
	
	public void setQuality(Quality q){
		quality = q;
	}
	*/
	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public int getDuration(){
		return duration;
	}
	

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void addTrial(Trial s){
		trials.add(s);
		updateDuration();
	}
	
	private void updateDuration(){
		
		int newDuration = 0;
		for (Trial s : trials)
			newDuration += s.getDuration();
		
		duration = newDuration;
	}
	
	public void addAoi(AreaOfInterest aoi, int trial, int screen){
		
		trials.get(trial).getScreens().get(screen).addAoi(aoi);
	}
	
	public AreaOfInterest getAoi(String name, int trial, int screen){
		
		for (AreaOfInterest aoi : trials.get(trial).getScreens().get(screen).getAois()){
			if (aoi.getName().equals(name))
				return aoi;
		}
		return null;
	}
	
	public void deselectAois(int trial, int screen){
		
		
		for (AreaOfInterest aoi : trials.get(trial).getScreens().get(screen).getAois()){
			
			aoi.setSelected(false);
		}
	
		
	}
	
	public ArrayList<Trial> getTrials(){
		return trials;
	}
		
	public int getScreenHeight() {
		return screenHeight;
	}

	public void setScreenHeight(int screenHeight) {
		this.screenHeight = screenHeight;
	}

	public int getScreenWidth() {
		return screenWidth;
	}

	public void setScreenWidth(int screenWidth) {
		this.screenWidth = screenWidth;
	}
	
	public boolean hasDrift(String path){
		
		File f = new File(path);
		if(f.exists()) return true;
		
		else return false;
	}
	
	public SortingProperty getSortingProperty(String name){
		
		for (Property p : properties){
			if (p.getName().equals(name))
				return (SortingProperty) p;
		}
		
		return null;
	}
	
	public ArrayList<FilteringProperty> getFilters() {
		
		ArrayList<FilteringProperty> filters = new ArrayList<FilteringProperty>();
		
		//properties.stream().filter(prop -> prop.type == PropertyType.FILTER)
			//			   .forEach(prop -> filters.add((FilteringProperty) prop));

		return filters;
	}

	public boolean isFixationAvailble() {
		return fixationAvailble;
	}

	public void setFixationAvailble(boolean fixationAvailble) {
		this.fixationAvailble = fixationAvailble;
	}

	public CognitiveLoad getCognitiveLoad() {
		return cognitiveLoad;
	}

	public void setCognitiveLoad(CognitiveLoad cognitiveLoad) {
		this.cognitiveLoad = cognitiveLoad;
	}
	
	public void drawEvents(Graphics2D g2d, int width, int height, int widthOffset, int heightOffset){
		
		for (Trial t : trials){
			
			for (Screen s : t.getScreens()){
		
				
				float factor =  (float)getElapsedTime(s.getTimestampStart())/(float)getExperimentDuration();
				Color c = ((EventScreen) s).getTriggerEvent().getColor();
				//g2d.setColor(c);
				g2d.setColor(Color.black);
				g2d.drawLine((int)((width*factor))+widthOffset, height,(int) ((width*factor))+widthOffset, height-heightOffset);
				
			}
		}
		
	}
	
	private int getNumberOfGazes(){
		
		int count = 0;
		
		for (Trial t : trials){
			
			for (Screen s : t.getScreens()){
				count += s.getGazes().size();
			}
		}
		return count;
	}
	
	private float getMaxPupilSize(int pupil){
		
		float maxSize = 0;
		
		if (pupil == 1){
		
			for (Trial t : trials){
				
				for (Screen s : t.getScreens()){
					
					for (Gaze g : s.getGazes()){
						
						maxSize = Math.max(g.getPupilSizeLeft(),maxSize);
						
					}
				}
			}
		}
		else {
			for (Trial t : trials){
				
				for (Screen s : t.getScreens()){
					
					for (Gaze g : s.getGazes()){
						
						
							maxSize = Math.max(g.getPupilSizeRight(),maxSize);
					}
				}
			}
		}
		
		
		return maxSize;
	}
	
	public float getAveragePupilSizePerScreen(Screen s){
		
		float sum = 0;
		int counter = 0;
		
		
		for (Gaze g : s.getGazes()){
					
			sum += g.getPupilSizeLeft();				
		}
		
		return sum/s.getGazes().size();
	}
	
	public float getAveragePupilSize(){
		
		float sum = 0;
		int counter = 0;
		
		for (Trial t : trials){
			
			for (Screen s : t.getScreens()){
				
				for (Gaze g : s.getGazes()){
					
					sum += g.getPupilSizeLeft();
					
									
				}
				counter += s.getGazes().size();
			}
		}
		
		return sum/counter;
		
	}
	
	public void drawPupilSize(Graphics2D g2d, int width, int height, int widthOffset, int heightOffset, int pupil){
		
		int nPoints = getNumberOfGazes();
		int[] xPoints = new int[nPoints];
		int[] yPoints = new int[nPoints];
		
		float max = Math.min(getMaxPupilSize(pupil),25);
		float averagePupilSize = getAveragePupilSize()*1.2f;
		System.out.println(max + " " + averagePupilSize);
		int counter = 0;
		float factorY = 0;
		
		
		for (Trial t : trials){
			
			for (Screen s : t.getScreens()){
				
				for (Gaze g : s.getGazes()){
					
						float factorX = (float)getElapsedTime(g.getTimestampStart())/(float)getExperimentDuration();
						
						if (pupil == 1){
						
							if (g.getPupilSizeLeft() <= max && g.getPupilSizeLeft() > 0)
								factorY =  g.getPupilSizeLeft()/averagePupilSize;
							
							//System.out.println(factorY + " " + g.getPupilSizeLeft());
						}
						else{
							if (g.getPupilSizeRight() <= max && g.getPupilSizeRight() > 0)
								factorY =  g.getPupilSizeRight()/averagePupilSize;
						}

						
						xPoints[counter] = (int)((width*factorX)+widthOffset);
						yPoints[counter] = (int)(height*(factorY))+heightOffset;
						counter++;
				}
			}
		}
		
		g2d.drawPolyline(xPoints, yPoints, nPoints);
	}
	
	public void drawBlinks(Graphics2D g2d, int width, int height, int widthOffset, int heightOffset){
		
		for (Trial t : trials){
			
			for (Screen s : t.getScreens()){
				
				for(Blink b : s.getBlinks()){
					
					float factor =  (float)getElapsedTime(b.getTimestampStart())/(float)getExperimentDuration();
					g2d.drawLine((int)((width*factor))+widthOffset, height-50,(int) ((width*factor))+widthOffset, height-heightOffset-50);
				}
			}
		}
		
		
	}

	public void drawPupilAverage(Graphics2D g2d, int width, int height, int widthOffset, int heightOffset, int pupil) {

		int lastX = widthOffset;
		int lastY = 0;
	
		float max = Math.min(getMaxPupilSize(pupil),25);
		float averagePupilSize; //= getAveragePupilSize()*1.2f;
		
		for (Trial t : trials){
			
			for (Screen s : t.getScreens()){
				
				float factorY  = getAveragePupilSizePerScreen(s)/max;
				float factorX =  (float)getElapsedTime(s.getTimestampStart())/(float)getExperimentDuration();
				
				g2d.drawLine(lastX, (int)(height*(factorY))+heightOffset,(int) ((width*factorX))+widthOffset, (int)(height*(factorY))+heightOffset);
				lastX = (int)((width*factorX))+widthOffset;
			}
		}
		
	}

	public int getPCTask() {
		return PCTask;
	}

	public void setPCTask(int pCTask) {
		PCTask = pCTask;
	}

	public void calculateBlinks(int threshold, int width, int height){
		
		BlinkDetection blinkDetection = new BlinkDetection();
		
		for (Trial t : trials){
			
			for (Screen s : t.getScreens()){
				ArrayList<Blink> blinks = blinkDetection.detectBlinks(s.getGazes(), threshold, width, height, pupilSizes);
				s.setBlinks(blinks);
			}
		}
		
		/*
		for (Trial t: trials){
			
			for (Screen s: t.getScreens()){
				
				for (Gaze g: s.getGazes()){
					
					if (g.getPupilSizeLeft() > 0)
						pupilSizes.addValue(g.getPupilSizeLeft());
					if (g.getPupilSizeLeft() > 0)
						pupilSizes.addValue(g.getPupilSizeRight());
				}
				
			}
		}*/
		
	}
	
	public DescriptiveStatistics getPupilSizes(){
		return pupilSizes;
	}
	
	public void calculateFixations(int duration, int dispersion, int width, int height) {
		
		FixationDetection fixDetec = new FixationDetection();
		
		for (Trial t : trials){
			
			for (Screen s : t.getScreens()){
				ArrayList<Saccade> saccades = new ArrayList<Saccade>();
				ArrayList<Fixation> fixations = fixDetec.detectFixations(s.getGazes(),saccades, duration, dispersion, width, height);
				s.setFixations(fixations);
				s.setSaccades(saccades);
			}
		}
	}

	public ExperimentProperties getExperimentProperties() {
		return experimentProperties;
	}

	public void setExperimentProperties(ExperimentProperties experimentProperties) {
		this.experimentProperties = experimentProperties;
	}

	
}
