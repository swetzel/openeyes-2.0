package Data.Subject;

import java.util.ArrayList;

public class Trial {

	private ArrayList<Screen> screens = new ArrayList<Screen>();
	private int duration;
	private long startTime;
	private long endTime;
	private String name;
	private boolean result; //0 if answer was wrong, 1 of answer was correct
	private TrialProperties trialProperties;
	
	public Trial (long l){
		startTime = l;
	}
	
	public TrialProperties getTrialProperties() {
		return trialProperties;
	}

	public void setTrialProperties(TrialProperties trialProperties) {
		this.trialProperties = trialProperties;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String s){
		name = s;
	}
	
	public long getStartTime() {
		return startTime;
	}
	
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	
	public long getEndTime() {
		return endTime;
	}
	
	public void setEndTime(long endTime) {
		
		this.endTime = endTime;
		// set duration
		duration = (int) (this.endTime - startTime);
	}
	
	public int getDuration(){
		return duration;
	}
	public void setDuration(int d){
		duration = d;
	}
	
	public void addScreen(Screen s){
		screens.add(s);
	}

	public ArrayList<Screen> getScreens(){
		return screens;
	}


	public boolean isResult() {
		return result;
	}


	public void setResult(boolean result) {
		this.result = result;
	}
}
