package Data.Subject;

import Data.Subject.ExperimentProperties.ExperimentType;

public class BTFExperimentProperties extends ExperimentProperties{

	private int groupID;
	
	public BTFExperimentProperties(ExperimentType type, int id){
		groupID = id;
		experimentType = type;
	}

	
	public int getGroupID() {
		return groupID;
	}

	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	
	
	
}
