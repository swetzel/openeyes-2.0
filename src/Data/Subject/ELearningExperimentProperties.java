package Data.Subject;

public class ELearningExperimentProperties extends ExperimentProperties{

	private int condition; // 1 = formal, 2 = personalisiert
	private int quality; // 1 - 4
	
	public ELearningExperimentProperties(ExperimentType type, int condition, int quality){
		
		this.experimentType = type;
		this.setCondition(condition);
		this.setQuality(quality);
	}

	public int getCondition() {
		return condition;
	}

	public void setCondition(int condition) {
		this.condition = condition;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}
	
	
	
}
