package Data.Subject;

import java.util.ArrayList;

import Data.Study.Property;
import Events.Default.Gaze;

public class Subject {

	private String name;
	private ArrayList<Experiment> experiments = new ArrayList<Experiment>();
	private ArrayList<Property> properties = new ArrayList<Property>();
	private int id;
	private float meanPupilSize = -1;
	//private float meanPupilSizeRight = -1;
	private SubjectProperties subjectProperties;
	
	public Subject(String name, int id){
		
		this.name = name;
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	
	public void addExperiment(Experiment ex){
		
		experiments.add(ex);
	}
	
	public ArrayList<Experiment> getExperiments(){
		return experiments;
	}
	
	public int getId(){
		return id;
	}
	
	public void addProperty(Property p){
		properties.add(p);
	}
	
	public ArrayList<Property> getProperties(){
		
		return properties;
	}
	/*
	public float getMeanPupilSizeLeft(){
		
		// compute mean pupil size
		if (meanPupilSizeLeft == -1){
			calcMeanPupilSizes();
		}
			
		return meanPupilSizeLeft;	
	}
	
	public float getMeanPupilSizeRight(){
		
		// compute mean pupil size
		if (meanPupilSizeLeft == -1){
			calcMeanPupilSizes();
		}
			
		return meanPupilSizeRight;	
	}
	*/
	public float getMeanPupilSize(){
		
		// compute mean pupil size
		if (meanPupilSize == -1){
			calcMeanPupilSizes();
		}
			
		return meanPupilSize;	
	}
	
	public void calcMeanPupilSizes(){
		
		int counter = 0;
		double size = 0.0f;
		
		
		/*
		for (Experiment ex : experiments){
			for (Trial t : ex.getTrials()){
				for (Screen s : t.getScreens()){
					for (Gaze g : s.getGazes()){
						
						counter++;
						sizeLeft += g.getPupilSizeLeft();
						sizeRight += g.getPupilSizeRight();
					}
				}
			}
		}*/
		
		for (Experiment ex : experiments){
			
			size += ex.getPupilSizes().getMean();
		}
		
		meanPupilSize = (float) size/experiments.size();
	}

	public SubjectProperties getSubjectProperties() {
		return subjectProperties;
	}

	public void setSubjectProperties(SubjectProperties subjectProperties) {
		this.subjectProperties = subjectProperties;
	}
}
