package Data.Subject;

import java.util.ArrayList;

import Events.Event;
import Events.Custom.PCEvent;
import Events.Default.MouseClick;
import Events.Default.MousePosition;


/**
 * 
 * @author Stefanie Wetzel
 * 
 * Special screen class that can be used for dynamic experiments. 
 *
 */
public class EventScreen extends Screen{

	private PCEvent triggerEvent; // event that triggered this screen to appear

	
	
	public EventScreen(long l,String path) {
		super(l, path);
	
	}

	public void setTriggerEvent( PCEvent event){
		triggerEvent = event;
	}
	
	public void setTrigger(PCEvent trigger){
		this.triggerEvent = trigger;
	}
	
	public PCEvent getTriggerEvent(){
		return triggerEvent;
	}
	
	
}
