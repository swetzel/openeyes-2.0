package Data.Subject;

import java.util.ArrayList;









import Data.AreaOfInterest;
import Events.LocationEvent;
import Events.Default.Blink;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.MouseClick;
import Events.Default.MousePosition;
import Events.Default.Saccade;

public class Screen {
	
	private ArrayList<Fixation> fixations = new ArrayList<Fixation>();
	private ArrayList<Gaze> gazes = new ArrayList<Gaze>();
	private ArrayList<Saccade> saccades = new ArrayList<Saccade>();
	private ArrayList<AreaOfInterest> aois = new ArrayList<AreaOfInterest>();
	private ArrayList<Blink> blinks = new ArrayList<Blink>();
	private ArrayList<MouseClick> mouseClicks = new ArrayList<MouseClick>();
	private ArrayList<MousePosition> mousePositions = new ArrayList<MousePosition>();
	private DriftCorrection driftCorrection;
	private String filename;
	private long timestampStart;
	private long timestampEnd;
	private int[][] transitionMatrix;
	
	public Screen(long time, String name){
		
		filename = name;
		timestampStart = time;
		driftCorrection = new DriftCorrection();
	}
		
	public void setAois(ArrayList<AreaOfInterest> aois){
		this.aois = aois;
	}
	
	public void calculateTransitionMatrix(boolean useFixations){
		
		if (useFixations){
			//ArrayList<Gaze> input = fixations;
			getTransitionMatrix(fixations);
		}
		else{
			getTransitionMatrix(gazes);
		}
		
	}
	
	private void getTransitionMatrix(ArrayList<? extends LocationEvent> eyeData){
		
		transitionMatrix = new int[aois.size()][aois.size()];
		
		// init transition matrix with 0
		for (int row = 0; row < aois.size(); ++row){
			
			for (int col = 0; col < aois.size(); ++col){
				transitionMatrix[row][col] = 0;
			}
		}
		
		// fill matrix with concrete values
		for (int row = 0; row < aois.size(); ++row){
			
			for (int col = 0; col < aois.size(); ++col){
				
				int counter = 0;
				
				if (row !=col){
					
					for (int f = 0; f < eyeData.size(); ++f){
						
						if (f+1 < eyeData.size()){
						
							if (aois.get(row).containsLocation(eyeData.get(f)) && aois.get(col).containsLocation(eyeData.get(f+1)))
								++counter;
						}
					}
				
					transitionMatrix[row][col] = counter;
				}
				
			}
		}
	}
	
	public void printTransitionMatrix(){
		
		System.out.println("Transition Matrix for screen " + filename);
		if (transitionMatrix != null){
			
			for (int row = 0; row < aois.size(); ++row){
				
				for (int col = 0; col < aois.size(); ++col){
					System.out.print("["+transitionMatrix[row][col]+"]");
				}
				System.out.println("");
			} 
		}
	}
	
	public int getTransitionBetween(int a, int b){
		return transitionMatrix[a][b];
	}
	
	public DriftCorrection getDriftCorrection() {
		return driftCorrection;
	}

	public void enableDrift(boolean b){
		
		if (!b){
			for (Fixation f : fixations)
				f.drift(this.driftCorrection.getDriftX(), this.driftCorrection.getDriftY());
			for (Gaze g: gazes)
				g.drift(this.driftCorrection.getDriftX(), this.driftCorrection.getDriftY());
			for (Saccade s : saccades)
				s.drift(this.driftCorrection.getDriftX(), this.driftCorrection.getDriftY());
		}
		else {
			for (Fixation f : fixations)
				f.restorePosition();
			for (Gaze g: gazes)
				g.restorePosition();
			for (Saccade s : saccades)
				s.restorePosition();
		}
	}

	public void setDriftCorrection(DriftCorrection driftCorrection) {
		this.driftCorrection = driftCorrection;
		
		for (Fixation f : fixations)
			f.drift(this.driftCorrection.getDriftX(), this.driftCorrection.getDriftY());
		for (Gaze g: gazes)
			g.drift(this.driftCorrection.getDriftX(), this.driftCorrection.getDriftY());
		for (Saccade s : saccades)
			s.drift(this.driftCorrection.getDriftX(), this.driftCorrection.getDriftY());
	}
	
	public int getDuration(){
		
		return (int) (timestampEnd - timestampStart);
	}
	
	public long getTimestampStart() {
		return timestampStart;
	}


	public void setTimestampStart(int timestampStart) {
		this.timestampStart = timestampStart;
	}


	public long getTimestampEnd() {
		return timestampEnd;
	}


	public void setTimestampEnd(long timestampEnd) {
		this.timestampEnd = timestampEnd;
	}


	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public void addGaze(Gaze g){
		gazes.add(g);
	}
	
	
	public ArrayList<Gaze> getGazes() {
		return gazes;
	}

	public void setGazes(ArrayList<Gaze> gazes) {
		this.gazes = gazes;
	}

	public void addSaccade(Saccade s){
		
		saccades.add(s);
	}
	
	public ArrayList<Saccade> getSaccades() {
		return saccades;
	}

	public void setSaccades(ArrayList<Saccade> saccades) {
		this.saccades = saccades;
	}

	public void addFixation(Fixation f){
		fixations.add(f);
	}
	
	public ArrayList<Fixation> getFixations() {
		return fixations;
	}

	public void setFixations(ArrayList<Fixation> fixations) {
		this.fixations = fixations;
	}
	
	public void addAoi(AreaOfInterest aoi){
		this.aois.add(aoi);
	}
	
	public ArrayList<AreaOfInterest> getAois(){
		return aois;
	}
	
	public void removeAoi(AreaOfInterest aoi){
		aois.remove(aoi);
	}
	
	public ArrayList<Blink> getBlinks(){
		return blinks;
	}
	
	public void addBlink(Blink b){
		blinks.add(b);
	}
	
	public void setBlinks(ArrayList<Blink> blinks){
		this.blinks = blinks;
	}
	
	public void updateDriftCorrection(int x, int y){
		
		driftCorrection.addDriftX(x);
		driftCorrection.addDriftY(y);
		
		for (Fixation f : fixations)
			f.drift(x, y);
		for (Gaze g: gazes)
			g.drift(x, y);
		for (Saccade s : saccades)
			s.drift(x, y);
	}

	public void resetDriftCorrection(){
		
		driftCorrection.setDriftX(0);
		driftCorrection.setDriftY(0);
		
		for (Fixation f : fixations)
			f.restorePosition();
		for (Gaze g: gazes)
			g.restorePosition();
		for (Saccade s : saccades)
			s.restorePosition();
	}

	public void addMouseClick(MouseClick click){
		
		mouseClicks.add(click);
	}
	
	public void addMousePosition(MousePosition pos){
		
		mousePositions.add(pos);
	}
	
	public ArrayList<MouseClick> getMouseClicks(){
		
		return mouseClicks;
	}
	
	public ArrayList<MousePosition> getMousePositions(){
		
		return mousePositions;
	}
	
	public MousePosition getCurrentMousePosition(long timeStamp){
		
		for (MousePosition pos : mousePositions){
			
			if (timeStamp >= pos.getTimestampStart()){
				return pos;
			}
		}
		
		return null;
	}
	
}
