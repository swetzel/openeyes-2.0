package Data.Subject;

public class BTFSubjectProperties extends SubjectProperties{

	private int groupID;

	public BTFSubjectProperties(String name, int groupID){
		this.name = name;
		this.groupID = groupID;
	}
	
	public int getGroupID() {
		return groupID;
	}

	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	
	
	
}
