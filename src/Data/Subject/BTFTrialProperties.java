package Data.Subject;

public class BTFTrialProperties extends TrialProperties {

	//private String name;
	private String image1;
	private String image2;
	private String userInput;
	private int timeUntilDecision;
	private boolean result;
	
	
	public BTFTrialProperties(String name, String image1, String image2, String userIntput, int time, boolean result){
		
		this.name = name;
		this.image1 = image1;
		this.image2 = image2;
		this.userInput = userIntput;
		this.timeUntilDecision = time;
		this.result = result;
		
	}
	
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public int getTimeUntilDecision() {
		return timeUntilDecision;
	}
	public void setTimeUntilDecision(int timeUntilDecision) {
		this.timeUntilDecision = timeUntilDecision;
	}
	public String getUserInput() {
		return userInput;
	}
	public void setUserInput(String userInput) {
		this.userInput = userInput;
	}
	public String getImage2() {
		return image2;
	}
	public void setImage2(String image2) {
		this.image2 = image2;
	}
	public String getImage1() {
		return image1;
	}
	public void setImage1(String image1) {
		this.image1 = image1;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
