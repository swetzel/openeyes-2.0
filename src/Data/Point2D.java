package Data;

import java.awt.Color;
import java.awt.Graphics2D;

public class Point2D {

	private double x;
	private double y;
	
	public Point2D(double x, double y){
		
		this.x = x;
		this.y = y;
	}
	
	
	public double getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public float getDistance(Point2D p){
		
		return (float) Math.sqrt(Math.pow(this.getX() - p.getX(),2) + 
				   Math.pow(this.getY() - p.getY(),2));
	}
	
	public void draw(Graphics2D g2d, double scaleX, double scaleY){
		
		g2d.setColor(Color.RED);
		g2d.fillOval((int)(x ),(int) (y ), 5, 5);
		
	}
}
