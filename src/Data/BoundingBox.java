package Data;

import Events.Default.Gaze;
import Events.Default.Saccade;


public class BoundingBox {

	private Point2D min;
	private Point2D max;
	
	public BoundingBox(Point2D a, Point2D b){
		this.min = new Point2D(Math.min(a.getX(),b.getX()), Math.min(a.getY(),b.getY()));
		this.max = new Point2D(Math.max(a.getX(),b.getX()), Math.max(a.getY(),b.getY()));;
	}

	public Point2D getMin() {
		return min;
	}

	public void setMin(Point2D min) {
		this.min = min;
	}

	public Point2D getMax() {
		return max;
	}

	public void setMax(Point2D max) {
		this.max = max;
	}
	
	public boolean contains(Gaze g, double scaleX, double scaleY){
		
		
		return (g.getX() <= this.max.getX()/scaleX && g.getY() <= this.max.getY()/scaleY && 
				g.getX() >= this.min.getX()/scaleX && g.getY() >= this.min.getY()/scaleY);
	}
	
	public boolean contains(Saccade s, double scaleX, double scaleY){
		
		
		return (s.getX() <= this.max.getX()/scaleX && s.getY() <= this.max.getY()/scaleY && 
				s.getX() >= this.min.getX()/scaleX && s.getY() >= this.min.getY()/scaleY &&
				s.getEndX() <= this.max.getX()/scaleX && s.getEndY() <= this.max.getY()/scaleY && 
				s.getEndX() >= this.min.getX()/scaleX && s.getEndY() >= this.min.getY()/scaleY);
	}
}
