package Data;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Vector;

import View.GUI;
import View.Experiment.ExperimentImagePanel;
import Data.Subject.Experiment;
import Events.LocationEvent;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.Saccade;

public class AreaOfInterest {

	private Vector<Point2D> points = new Vector<Point2D>();
	private String name;
	private boolean isPredefined;
	private boolean isSelected;
	private ArrayList<Gaze> gazes = new ArrayList<Gaze>();
	private ArrayList<Saccade> saccades = new ArrayList<Saccade>();
	private ArrayList<Fixation> fixations = new ArrayList<Fixation>();
	private int trial = -1;
	private int screen = -1;
	private AOIShape shape;
	
	public enum AOIShape{
		POLYGON,
		RECTANGLE,
		ELLIPSE,
	}
	
	public AreaOfInterest(){
		
	}
	
	public AreaOfInterest(AOIShape s){
		shape = s;
	}
	
	
	public AOIShape getShape(){
		return shape;
	}
	
	public AreaOfInterest(boolean b, String name, AOIShape s){
		isPredefined = b;
		isSelected = false;
		this.name = name;
		shape = s;
		
	}
	
	public int getTrial() {
		return trial;
	}

	public void setTrial(int trial) {
		this.trial = trial;
	}

	public int getScreen() {
		return screen;
	}

	public void setScreen(int screen) {
		this.screen = screen;
	}
	
	
	public ArrayList<Gaze> getGazes() {
		return gazes;
	}

	public void setGazes(ArrayList<Gaze> gazes) {
		this.gazes = gazes;
	}

	public ArrayList<Saccade> getSaccades() {
		return saccades;
	}

	public void setSaccades(ArrayList<Saccade> saccades) {
		this.saccades = saccades;
	}

	public ArrayList<Fixation> getFixations() {
		return fixations;
	}

	public void setFixations(ArrayList<Fixation> fixations) {
		this.fixations = fixations;
	}

	
	private Polygon constructPolygon(){
		
		int[] x = new int[points.size()];
		int[] y = new int[points.size()];
		
		for (int i = 0; i < points.size(); ++i){
			x[i] =(int) points.get(i).getX();
			y[i] = (int) points.get(i).getY();
		}
		
		return new Polygon(x,y,points.size());
	}
	
	public boolean containsPoint(Point2D p){
		
		//ImagePanel panel = ImagePanel.getInstance();
		
		switch (shape) {
		
		case POLYGON:	
			return constructPolygon().contains(new Point((int) (p.getX()), (int) (p.getY())));

		case RECTANGLE:
			
			Rectangle rect = new Rectangle((int)(points.get(0).getX()), (int)(points.get(0).getY()), (int)(points.get(1).getX()- points.get(0).getX()),(int) (points.get(1).getY() - points.get(0).getY()));
			return rect.contains(new Point((int) p.getX(), (int) p.getY()));
			
		case ELLIPSE:
			
			Ellipse2D el = new Ellipse2D.Float((int)points.get(0).getX(), (int)points.get(0).getY(), (int)(points.get(1).getX() - points.get(0).getX()),(int) (points.get(1).getY() - points.get(0).getY()));
			return el.contains(new Point((int) p.getX(), (int) p.getY()));
		}
		
		return false;
		
	}
	
	public boolean containsLocation(LocationEvent loc){
		
		//return false;
		return containsPoint(new Point2D(loc.getX(), loc.getY()));
	}
	
	public void addPoint(Point2D p){
		points.add(p);
	}
	
	public Vector<Point2D> getPoints(){
		return points;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean isSelected(){
		
		return isSelected;
	}
	
	public void setSelected(boolean b){
		isSelected = b;
	}
	
	public void draw(Graphics2D g2d, double scaleX, double scaleY){
		
		g2d.setStroke(new BasicStroke(2));
		
		if (isPredefined == true && isSelected == false)
			g2d.setColor(new Color(255,0,0,50));
		else if (isPredefined == false && isSelected == false)
			g2d.setColor(new Color(255,0,0,50));
		else 
			g2d.setColor(new Color(50,205,50,50));
		
		switch (shape) {
		
		case POLYGON:
			
			int npoints = points.size();
			int xpoints[] = new int[npoints];
			int ypoints[] = new int[npoints];
			 
			for (int i = 0; i < npoints; ++i){
				Point2D p = points.get(i);
				xpoints[i] = (int) (p.getX() );
				ypoints[i] = (int) (p.getY() );
			}
			g2d.fillPolygon(xpoints, ypoints, npoints);
			
			g2d.setColor(Color.BLACK);
			g2d.drawPolygon(xpoints, ypoints, npoints);
			//g2d.setStroke(new BasicStroke(1));
			break;

		case RECTANGLE:
			
			g2d.setColor(new Color(255,0,0,50));
			g2d.fillRect((int)(points.get(0).getX()), (int)(points.get(0).getY()), (int)(points.get(1).getX() - points.get(0).getX()),(int) (points.get(1).getY() - points.get(0).getY()));
			//g2d.setStroke(new BasicStroke(1));
			g2d.setColor(Color.BLACK);
			g2d.drawRect((int)(points.get(0).getX()), (int)(points.get(0).getY()), (int)(points.get(1).getX() - points.get(0).getX()),(int) (points.get(1).getY() - points.get(0).getY()));
			break;
			
		case ELLIPSE:
		
			g2d.fillOval((int)(points.get(0).getX()), (int)(points.get(0).getY()), (int)Math.abs(points.get(1).getX() - points.get(0).getX()),(int) Math.abs(points.get(1).getY() - points.get(0).getY()));
			//g2d.setStroke(new BasicStroke(1));
			g2d.setColor(Color.BLACK);
			g2d.drawOval((int)(points.get(0).getX()), (int)(points.get(0).getY()), (int)Math.abs(points.get(1).getX() - points.get(0).getX()),(int) Math.abs(points.get(1).getY() - points.get(0).getY()));
			break;
		
		}
		g2d.setStroke(new BasicStroke(1));

	}
	
	/*
	 * Find the number of gazes, fixations and transitions in this AOI.
	 */
	public void checkContent(){
		
		int fixations = 0;
		int transitions = 0;
		int gazes = 0;
		
		GUI gui = GUI.getInstance();
		
		// highlight aoi
		
		if (gui.study != null){
			
			gui.study.getAoi(name, gui.trialNumber, gui.screenNumber).setSelected(true);
			
			/*
			for (Experiment ex : gui.study.getExperiments()){
				
				if (ex.isEnabled()){						
					if (experimentMenu.showFixations.getState()){
						
						for (Fixation f : ex.getTrials().get(gui.trialNumber).getScreens().get(gui.screenNumber).getFixations())
							if (this.containsEyeData(f))
								fixations++;
	
					}
					if (experimentMenu.showGazes.getState()){
						
						for (Gaze g : ex.getTrials().get(gui.trialNumber).getScreens().get(gui.screenNumber).getGazes())
							if (this.containsEyeData(g))
								gazes++;
					}
				}
			}*/
		}
		/*
		else if (gui.experiment != null){
			
			gui.experiment.getAoi(name, gui.trialNumber, gui.screenNumber).setSelected(true);
			
			if (experimentMenu.showFixations.getState()){
				
				for (Fixation f : gui.experiment.getTrials().get(gui.trialNumber).getScreens().get(gui.screenNumber).getFixations())
					if (this.containsEyeData(f))
						fixations++;

			}
			if (experimentMenu.showGazes.getState()){
				
				for (Gaze g : gui.experiment.getTrials().get(gui.trialNumber).getScreens().get(gui.screenNumber).getGazes())
					if (this.containsEyeData(g))
						gazes++;
			}
		}
		*/
		//gui.updateAoiLables(fixations, transitions, gazes);
	}
}
