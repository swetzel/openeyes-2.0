package Data.Study;

import java.util.ArrayList;

import Data.Subject.PropertyValue;

public interface Sortable {

	
	public ArrayList<PropertyValue> sort();
}
