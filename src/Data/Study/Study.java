package Data.Study;

import java.lang.reflect.Array;
import java.util.ArrayList;

import Data.AreaOfInterest;
import Data.PropertyFilterValues;
import Data.PropertySortingValues;
import Data.Subject.Experiment;
import Data.Subject.Screen;
import Data.Subject.Subject;
import Data.Subject.Trial;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.Saccade;

public class Study {

	
	private ArrayList<Subject> subjects = new ArrayList<Subject>();
	private ArrayList<AreaOfInterest> aois = new ArrayList<AreaOfInterest>();
	private ArrayList<Fixation> activeFixations = new ArrayList<Fixation>();
	private ArrayList<Saccade> activeSaccades = new ArrayList<Saccade>();
	private ArrayList<Gaze> activeGazes = new ArrayList<Gaze>();
	private ArrayList<PropertyFilterValues> propertyFilterValues = new ArrayList<PropertyFilterValues>();
	private ArrayList<PropertySortingValues> propertySortingValues = new ArrayList<PropertySortingValues>();

	private ArrayList<Property> properties = new ArrayList<Property>();
	private boolean cognitiveLoad = false;
	private String path;
	
	
	public Study(String path, boolean load){
		this.cognitiveLoad = load;
		this.path = path;
	}
	
	public boolean useLoad(){
		return cognitiveLoad;
	}
	
	public void addAoi(AreaOfInterest aoi){
		aois.add(aoi);
	}
	
	public void updateScreensWithStudyAois(){
		
		for (Subject sub : subjects){
			for (Experiment ex : sub.getExperiments()){
				for (Trial t : ex.getTrials()){
					for (Screen s : t.getScreens()){
						s.setAois(aois);
					}
				}
			}
		}
		
	}
	
	public ArrayList<AreaOfInterest> getAois(){
		return aois;
	}
	
	public void addPropertyFilterValue(PropertyFilterValues vals){
		propertyFilterValues.add(vals);
	}
	
	public ArrayList<PropertyFilterValues> getPropertyFilterValues(){
		return propertyFilterValues;
	}
	
	public void addPropertySortingValue(PropertySortingValues vals){
		propertySortingValues.add(vals);
	}
	
	public ArrayList<PropertySortingValues> getPropertySortingValues(){
		return propertySortingValues;
	}
	
	
	public void addSubject(Subject s){
		subjects.add(s);
	}
	
	public ArrayList<Subject> getSubjects(){
		return subjects;
	}
	
	public String getPath(){
		return path;
	}
	
	public ArrayList<Fixation> getActiveFixations(){
		return activeFixations;
	}
	
	public ArrayList<Saccade> getActiveSaccades(){
		return activeSaccades;
	}
	
	public ArrayList<Gaze> getActiveGazes(){
		return activeGazes;
	}
	
	/*
	public void updateActiveFixations(float min, float max, int trial, int screen){
		
		activeFixations.clear();
		for (Experiment e : experiments){
			
			if (e.isEnabled()){
				
				for (Fixation f : e.getTrials().get(trial).getScreens().get(screen).getFixations()){
						
					activeFixations.add(f);			
				}	
			}	
		}		
	}
	
	
	public void updateActiveSaccades(float min, float max, int trial, int screen){
		
		activeSaccades.clear();
		for (Experiment e : experiments){
			
			if (e.isEnabled()){
				
				for (Saccade s : e.getTrials().get(trial).getScreens().get(screen).getSaccades()){
						
					activeSaccades.add(s);			
				}	
			}	
		}		
	}
	
	public void updateActiveGazes(float min, float max, int trial, int screen){
		
		activeGazes.clear();
		for (Experiment e : experiments){
			
			if (e.isEnabled()){
				
				for (Gaze g : e.getTrials().get(trial).getScreens().get(screen).getGazes()){
						
					activeGazes.add(g);			
				}	
			}	
		}	
		
	}
	*/
	public void addAoi(AreaOfInterest aoi, int trial, int screen){
		
		/*
		for (Experiment ex : experiments){
			ex.getTrials().get(trial).getScreens().get(screen).addAoi(aoi);
		}
		 */
	}
	
	public void removeAoi(AreaOfInterest aoi, int trial, int screen){
	
		/*
		for (Experiment ex : experiments){
			ex.getTrials().get(trial).getScreens().get(screen).removeAoi(aoi);
		}
		*/
	}
	
	public AreaOfInterest getAoi(String name, int trial, int screen){
		
		/*
		for (AreaOfInterest aoi : experiments.get(0).getTrials().get(trial).getScreens().get(screen).getAois()){
			if (aoi.getName().equals(name))
				return aoi;
		}*/
		return null;
	}
	
	public void deselectAois(int trial, int screen){
		
		/*
		for (Experiment ex : experiments){
			for (AreaOfInterest aoi : ex.getTrials().get(trial).getScreens().get(screen).getAois()){
				System.out.println(ex.getTrials().get(trial).getScreens().get(screen).getAois().size());
				aoi.setSelected(false);
			}
		}*/
		
	}

	/*
	public ArrayList<Trial> getStudyTrials() {
		return studyTrials;
	}

	public void setStudyTrials(ArrayList<Trial> studyTrials) {
		this.studyTrials = studyTrials;
	}
	*/
}
