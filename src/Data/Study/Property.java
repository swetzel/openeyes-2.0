package Data.Study;

import java.util.Properties;

/**
 * 
 * @author Stefanie Wetzel
 *
 * Experiments can be either filtered or sorted by their properties.
 *
 */
public abstract class Property {

	protected String name;
	
	/*
	public enum PropertyType{
		
		FILTER,
		SORTING,
	}
	*/
	
	public Property(){}
	
	public String getName(){
		return name;
	}
	
	/*
	public PropertyType getType(){
		return type;
	}
	*/
	
	
}
