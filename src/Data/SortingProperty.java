package Data;

import java.util.Comparator;

import Data.Study.Property;


public abstract class SortingProperty extends Property implements Comparator<SortingProperty>{

	private float value;
	
	/*
	public SortingProperty(PropertyType type, String name, float value) {
		super();
		
		this.name = name;
		this.type = type;
		this.value = value;
		
	}
	 */
	
	public float getValue(){
		return value;
	}

	@Override
	public abstract int compare(SortingProperty o1, SortingProperty o2);
}
