package Data;

import java.util.ArrayList;

public class PropertySortingValues {

	private String name;
	private ArrayList<Float> values = new ArrayList<Float>();
	private float min;
	private float max;
	
	public PropertySortingValues(String name, float f, float g){
		
		this.name = name;
		this.min = f;
		this.max = g;
	}
	
	public ArrayList<Float> getValues(){
		return values;
	}
	
	public float getMin(){
		return min;
	}
	
	
	public float getMax(){
		return max;
	}
	
	public void addValue(float val){
		values.add(val);
	}
	
	public String getName(){
		return name;
	}
}
