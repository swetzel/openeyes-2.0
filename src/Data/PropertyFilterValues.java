package Data;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class PropertyFilterValues {

	private String name;
	private ArrayList<String> values = new ArrayList<String>();
	
	public PropertyFilterValues(String name){
		
		this.name = name;
	}
	
	public ArrayList<String> getValues(){
		return values;
	}
	
	public void addValue(String val){
		values.add(val);
	}
	
	public String getName(){
		return name;
	}
}
