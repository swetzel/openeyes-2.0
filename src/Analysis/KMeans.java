package Analysis;


import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

import Data.Cluster;
import Events.Default.Gaze;


public class KMeans {

	public KMeans(){}

	public ArrayList<Cluster> kMeans(int clusterNumber, Vector<Gaze> gazes){
		
		ArrayList<Cluster> clusters = new ArrayList<Cluster>();
		
		int iterations = 100;
		Vector<Point2D.Double> centroids = new Vector<Point2D.Double>();
		
		// generate output vectors
		for (int i = 0; i < clusterNumber; ++i){
			clusters.add(new Cluster());
		}
				
		// pick random centroids
		Random rn = new Random();
		for (int i = 0; i < clusterNumber; ++i){
			
			Gaze point = gazes.get(rn.nextInt(gazes.size()));
			centroids.add(new Point2D.Double(point.getX(),point.getY()));
		}
		
		// assign points to clusters
		for (int i = 0; i < iterations; ++i){
			
			//clear output 
			for (int j = 0; j < clusterNumber; ++j){
				clusters.get(j).clear();
			}

			int cluster = 0;
			for (Gaze g : gazes){
				double distance = 1000.0;
				for (int j = 0; j < clusterNumber; ++j){
					
					double newDistance = Math.sqrt(Math.pow(centroids.get(j).getX() - g.getX(),2) + 
												   Math.pow(centroids.get(j).getY() - g.getY(),2));
											 
					if (newDistance < distance){
						distance = newDistance;
						cluster = j;
					}
				}
				g.setClusterNumber(cluster);
				clusters.get(cluster).add(g);
			}
			
			// update centroids
			int currentCluster = 0;
			for (Cluster v : clusters){
			
				double sumX = 0;
				double sumY = 0;
				
				for (Gaze g : v.getGazes()){
					
					sumX += g.getX();
					sumY += g.getY();
				}
				centroids.get(currentCluster).setLocation(sumX/v.getGazes().size(), sumY/v.getGazes().size());
				currentCluster++;
			}
		}
		return clusters; 		
	}
	
}
