package Analysis;


import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

import Data.Point2D;
import Events.LocationEvent;
import Events.Default.Gaze;

public class GridCell {

	//private int count = 0;
	private float value = 0;
	private float addedValues = 0;
	private ArrayList<LocationEvent> gazes = new ArrayList<LocationEvent>();
	private Point2D start;
	private Point2D end;
	
	
	public GridCell(){
		
	}
	
	public GridCell(Point2D start, Point2D end){
		this.start = start;
		this.end = end;
	}
	
	public void setValues(Point2D start, Point2D end){
		this.start = start;
		this.end = end;
	}
	
	public Point2D getCenter(){
		
		int distanceX = (int) Math.abs(start.getX() - end.getX());
		int distanceY = (int) Math.abs(start.getY() - end.getY());
		
		return new Point2D(start.getX()+distanceX/2, start.getY()+distanceY/2); 
	}
	
	private boolean containsPoint(int x, int y){
		
		return (x >= start.getX() && x <= end.getX() && y >= start.getY() && y <= end.getY());
	}
	
	public void checkPoints(ArrayList<LocationEvent> gazes){
		
		for (LocationEvent g : gazes){
			if (this.containsPoint(g.getX(), g.getY())){
				this.gazes.add(g);
				//count++;
				value += g.getDuration();
				
			}
		}
		
		
	}
	
	public void setValue(float v){
		value = v; 
	}
	
	public void addValue(float value, float maxValue){
		
		//if ((this.addedValues + value + this.value) < maxValue )
			this.addedValues += value;
		
	}
	
	public float getAddedValues(){
		return addedValues;
	}
	
	public float getValue(){
		return value;
	}
	
	public void addGaze(Gaze g){
		
		gazes.add(g);
		//count++;
		value += g.getDuration();
	}
	/*
	public int getCount(){
		return count;
	}
	*/
	public void clearCell(){
		
		//count = 0;
		gazes.clear();
		value = 0;
	}
	
	public ArrayList<LocationEvent> getGazes(){
		return gazes;
	}
	
	public Color getValueBetweenTwoFixedColors(float value)
	{
		
		
		int aR = 0;   int aG = 0; int aB=255;  // RGB for our 1st color (blue in this case).
		int bR = 0; int bG = 255; int bB = 0; 
		int cR = 255; int cG = 0; int cB=0;    // RGB for our 2nd color (red in this case).
	 
		int red;
		int green;
		int blue;
		
		if (value < 0.5){
			red   = (int) ((bR - aR) * value + aR);      // Evaluated as -255*value + 255.
			green = (int) ((bG - aG) * value + aG);      // Evaluates as 0.
			blue  = (int) ((bB - aB) * value + aB); 	 // Evaluates as 255*value + 0.
		}
		else{
			red   = (int) ((cR - bR) * value + bR);      // Evaluated as -255*value + 255.
			green = (int) ((cG - bG) * value + bG);      // Evaluates as 0.
			blue  = (int) ((cB - bB) * value + bB); 	 // Evaluates as 255*value + 0.
	
		}
		return new Color(red, green, blue,200);
	}
	
	public void draw(Graphics2D g2d, int resX, int resY, float maxDataVal){
		
		float input;
		float resultingValues = value+addedValues;
		
		if (maxDataVal > 0){
			
			if (resultingValues > maxDataVal)
				resultingValues = maxDataVal;
				
			input = (float)resultingValues / (float)maxDataVal;  
		}
		else 
			input = 0;
		//System.out.println(input);
		if (input > 0.15){
			
			
			g2d.setColor(getValueBetweenTwoFixedColors(input));	
			g2d.fillRect((int) (start.getX()),(int) (start.getY()), resX, resY);
		}
		
	}
}
