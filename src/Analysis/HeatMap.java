package Analysis;


import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import java.util.Hashtable;

import Events.LocationEvent;
import Events.Default.Gaze;

public class HeatMap {

	private int width;
	private int height;
	private int gridSize;
	private Grid grid; 
	
	public HeatMap(int width, int height, ArrayList<LocationEvent> gazes, float scaleX, float scaleY, int radius, int gridSize) {
			
		this.height = height;
		this.width = width;
		this.gridSize = gridSize;
		
		grid = new Grid(width, height, gridSize, gridSize, gazes, scaleX, scaleY, radius);
	}
	
	public void draw(Graphics2D g2d){
		
		grid.draw(g2d, gridSize, gridSize);
	}
	
}
