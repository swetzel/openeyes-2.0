package Analysis;


import java.util.ArrayList;
import java.util.Vector;

import Data.Cluster;
import Events.Default.Gaze;


public class DBScan {

	public int epsilon = 40; 
	public int minPts = 3;
	public int timeDistance = 40;
	public boolean useTime = false;
	
	public DBScan(boolean time){
		useTime = time;
	}
	
	
	public void updateValues(){
		
		/*
		epsilon = Integer.valueOf(menu.epsilonSpinner.getValue().toString()).intValue();
		minPts = Integer.valueOf(menu.minPtsSpinner.getValue().toString()).intValue();
		timeDistance = Integer.valueOf(menu.spinnerTimeDistance.getValue().toString()).intValue();
		*/
	}
	
	
	public ArrayList<Cluster> cluster(Vector<Gaze> gazes){
		
		
		// reset points
		ArrayList<Gaze> toCheck = new ArrayList<>();
		for (Gaze g: gazes){
			g.visited = Gaze.Visited.UNCLASSIFIED;
			g.clusterID = -1;
		}
		toCheck.addAll(gazes);
		
		int n = gazes.size();
		int cluster = 0;
		// now scan the points in the data-set point by point 
		for (int i = 0; i < n; i++) {
			Gaze p = gazes.get(i);
			if (p.visited == Gaze.Visited.UNCLASSIFIED) {
				
				// get the nearest points to the point 'p'
				// within the range defined by radius 'epsilon'
				ArrayList<Gaze> nearest = regionQuery(toCheck, p, epsilon);
				if (nearest.size() <= minPts) {
					p.visited = Gaze.Visited.NOISE;
				} else {
					
					// the point 'p' has sufficient number of neighbors 
					// to form its own cluster, now we need to expand that cluster
					// to include the points that are directly-reachable by
					// the points in the neighborhood of 'p'
					
					cluster++;
					p.clusterID = cluster;
					p.visited = Gaze.Visited.VISITED;
					expandCluster(cluster,nearest, toCheck, epsilon, minPts);
				}
			}
		}
		
		ArrayList<Cluster> allCluster = new ArrayList<Cluster>();
		
		for (int i = 0; i < cluster; ++i){
			allCluster.add(new Cluster());
		}
		for (Gaze g: gazes){
			if (g.clusterID != -1)
				allCluster.get(g.clusterID-1).add(g);
		}
		
				
		return allCluster;
	}
	
	private void expandCluster(int id, ArrayList<Gaze> neighbors, ArrayList<Gaze> toCheck, int eps, int minPts){
		
		while (!neighbors.isEmpty()){
			Gaze currentGaze = neighbors.remove(0);
			
			if (currentGaze.visited == Gaze.Visited.UNCLASSIFIED ){
				
				ArrayList<Gaze> newNeighbors = regionQuery(toCheck, currentGaze, eps);
				if (newNeighbors.size() >= minPts){
					neighbors.addAll(newNeighbors);
				}
				currentGaze.clusterID = id;
				currentGaze.visited = Gaze.Visited.VISITED;
			}
			if (currentGaze.visited == Gaze.Visited.NOISE){
			
				currentGaze.clusterID = id;
			}
		}
	}
	
	
	private ArrayList<Gaze> regionQuery(ArrayList<Gaze> toCheck,Gaze g, int eps){
		
		ArrayList<Gaze> gazes = new ArrayList<Gaze>(); 
		
		for (Gaze gaze: toCheck ){
			
			if (useTime){
				if (g.getDistance(gaze) < eps && Math.abs(g.getTimestampStart() - gaze.getTimestampStart()) <= timeDistance){
					gazes.add(gaze);
				}
			}
			else{
				if (g.getDistance(gaze) <= eps){
					gazes.add(gaze);
				}
			}
		}
		
		return gazes;
	}

	
}
