package Analysis;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import Events.Default.Blink;
import Events.Default.Fixation;
import Events.Default.Gaze;

public class BlinkDetection {

	public BlinkDetection(){
		
	}
	
	public ArrayList<Blink> detectBlinks(ArrayList<Gaze> gazes, int threshold, int width, int height, DescriptiveStatistics pupilSizes){
		
		ArrayList<Blink> blinks = new ArrayList<Blink>();
		int counter = 0;
		boolean blinkStarted = false;
		Deque<Gaze> queue = new LinkedList<Gaze>();
		
		
		while (counter < gazes.size()){
			
			if (!gazes.get(counter).isValid(width, height)){
			
				if (!blinkStarted){
					blinkStarted = true;
				}
				queue.offerLast(gazes.get(counter));
				
			}
			else{
				if (gazes.get(counter).getPupilSizeLeft() < 40 && gazes.get(counter).getPupilSizeLeft() > 0)
					pupilSizes.addValue(gazes.get(counter).getPupilSizeLeft());
				//if (gazes.get(counter).getPupilSizeRight() < 30)
					//pupilSizes.addValue(gazes.get(counter).getPupilSizeRight());
				
				if(checkDurationThreshold(queue, threshold)){ // blink
				
					Blink blink = new Blink(queue.peek().getTimestampStart());
					blink.setEnd(queue.peekLast().getTimestampEnd());
					blinks.add(blink);
				}
				
				blinkStarted = false;
				queue.clear();
			}	
			counter++;
		}
		
		return blinks;
	}
	
	private boolean checkDurationThreshold(Deque<Gaze> queue, int threshold){
		
		if (queue.isEmpty())
			return false;
		
		int value = 0;
		for (Gaze g :queue){
			value += g.getDuration();
		}
		
		return value <= threshold;
	}
}
