package Analysis;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.Saccade;

public class FixationDetection {

	public FixationDetection(){
		
	}
	
	public ArrayList<Fixation> detectFixations(ArrayList<Gaze> gazes,ArrayList<Saccade> saccades, int duration, int dispersion, int width, int height){
				
		return idt(gazes, saccades,dispersion, duration, width, height);
	}
	
	private ArrayList<Fixation> idt(ArrayList<Gaze> gazes,ArrayList<Saccade> saccades,int dispersionThreshold, int durationThreshold, int width, int height){
		
		ArrayList<Fixation> fixations = new ArrayList<Fixation>();
		// = new ArrayList<Saccade>();
		int counter = 0;
		Deque<Gaze> queue = new LinkedList<Gaze>();
		//Deque<Gaze> saccadeQueue = new LinkedList<Gaze>();
				
		while (counter < gazes.size()){
		
			// Initialize window over first points to cover the duration threshold
			while (!isDurationThresholdReached(queue, durationThreshold) && counter < gazes.size()){
				
				//if (gazes.get(counter).isValid(width, height)){
					queue.offerLast(gazes.get(counter));
				//}
				++counter;
			}
			
			if (isDurationThresholdReached(queue, durationThreshold) && checkDispersionThreshold(queue, dispersionThreshold)){ //fixation

				// Add additional points to the window	until dispersion > threshold
				while(checkDispersionThreshold(queue, dispersionThreshold) && counter < gazes.size()){
					
					//if (gazes.get(counter).isValid(width, height)){
						queue.offerLast(gazes.get(counter));
					//}
					++counter;
					if (!checkDispersionThreshold(queue, dispersionThreshold)){
						break;
					}
				}
				if (!queue.isEmpty()){
					
					//if (!saccadeQueue.isEmpty())
					fixations.add(new Fixation(queue));
					if (fixations.size() > 1)
						saccades.add(new Saccade(fixations.get(fixations.size()-2),fixations.get(fixations.size()-1)));
				}
				queue.clear();
				//saccadeQueue.clear();
			}
			else{
				//saccadeQueue.offerLast(queue.poll());
				queue.poll();
			}
			
		}
		return fixations;
	}
	
	private boolean isDurationThresholdReached(Deque<Gaze> queue, int threshold){
		
		int value = 0;
		for (Gaze g :queue){
			value += g.getDuration();
		}
		
		return value >= threshold;
	}
	
	private boolean checkDispersionThreshold(Deque<Gaze> queue, int threshold){
		
		//dispersion D = [max(x) � min(x)] + [max(y) � min(y)]
		
		int dispersion = 0;
		int maxX = -100000;
		int minX = 100000;
		int maxY = -100000;
		int minY = 100000;
		
		for (Gaze g :queue){
			maxX =  Math.max(g.getX(), maxX);
			minX =  Math.min(g.getX(), minX);
			maxY =  Math.max(g.getY(), maxY);
			minY =  Math.min(g.getY(), minY);
		}
		
		dispersion = (maxX - minX) + (maxY - minY);
		return dispersion < threshold;
	}
	
}
