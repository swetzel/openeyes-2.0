package Analysis;

import java.awt.Graphics2D;

public class CognitiveLoad {

	private int mentalDemand;
	private int physicalDemand;
	private int temporalDemand;
	private int performance;
	private int effort;
	private int frustration;
	
	public int getMentalDemand() {
		return mentalDemand;
	}
	
	public float getMentalDemandNormalized() {
		return 1 - mentalDemand/100f;
	}
	
	public void setMentalDemand(int mentalDemand) {
		this.mentalDemand = mentalDemand;
	}
	public int getPhysicalDemand() {
		return physicalDemand;
	}
	
	public float getPhysicalDemandNormalized() {
		return 1 - physicalDemand/100f;
	}
	
	public void setPhysicalDemand(int physicalDemand) {
		this.physicalDemand = physicalDemand;
	}
	public int getTemporalDemand() {
		return temporalDemand;
	}
	
	public float getTemporalDemandNormalized() {
		return 1 - temporalDemand/100f;
	}
	
	public void setTemporalDemand(int temporalDemand) {
		this.temporalDemand = temporalDemand;
	}
	public int getPerformance() {
		return performance;
	}
	
	public float getPerformanceNormalized() {
		return 1 - performance/100f;
	}
	
	public void setPerformance(int performance) {
		this.performance = performance;
	}
	
	public int getEffort() {
		return effort;
	}
	
	public float getEffortNormalized() {
		return 1 - effort/100f;
	}
	
	public void setEffort(int effort) {
		this.effort = effort;
	}
	public int getFrustration() {
		return frustration;
	}
	
	public float getFrustrationNormalized() {
		return 1 - frustration/100f;
	}
	
	public void setFrustration(int frustration) {
		this.frustration = frustration;
	}

	public float getTLXScore() {

		return (mentalDemand + physicalDemand + temporalDemand + effort + performance + frustration)/6f;
	}
	
	public float getTLXScoreNormalized() {

		return 1 - ((mentalDemand + physicalDemand + temporalDemand + effort + performance + frustration)/6f)/100f;
	}
	
	
	
}
