package Analysis;


import java.awt.Graphics2D;
import java.util.ArrayList;

import View.GUI;
import View.Menu.Menu;
import Data.Point2D;
import Events.LocationEvent;
import Events.Default.Gaze;

public class Grid implements Runnable{

	private GridCell[][] cells;
	private int width;
	private int height;
	private int rows;
	private int cols;
	private float maxDataVal = 0;
	private int radius;
	private float maxInitialValue;
	private int progress = 0;
	private int resX;
	private int resY;
	private ArrayList<LocationEvent> gazes;
	
	public Grid(int width, int height, int resX, int resY, ArrayList<LocationEvent> gazes, double scaleX, double scaleY, int radius){
		
		updateGrid(width, height, resX, resY, gazes, scaleX, scaleY, radius);
		
	}

	public void updateGrid(int width, int height, int resX, int resY, ArrayList<LocationEvent> gazes, double scaleX, double scaleY, int radius){
		
		this.width = (int) (width);
		this.height = (int) (height);
		this.radius = radius;
		this.resX = resX;
		this.resY = resY;
		this.gazes = gazes;
		
		new Thread(this).start();
	}
	
	public void calcValue(int c, int r, int cols, int rows, float value){
		
		if (c >= 0 && c < cols && r >= 0 && r < rows){
			
			if (cells[r][c] == null)
				cells[r][c] = new GridCell();
			
			cells[r][c].addValue(value, maxInitialValue);
		}
	}
	
	
	private boolean containsGridCell(GridCell center, GridCell cell, int radius ){
		
		return distance(center.getCenter().getX(), center.getCenter().getY(), cell.getCenter().getX(), cell.getCenter().getY()) <= radius;	
	}

	private float distance(double x1, double y1, double x2, double y2){
		
		return (float) Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1,2));
	}
	
	private float calcValue(float radius){
		
		
		return (float) Math.exp(-2 * Math.pow(radius,2));		
	}
	
	
	public void findMaxDataVal(){
		
		for (int r = 0; r < rows; r++){
			
			for (int c = 0; c < cols; ++c){
				maxDataVal = Math.max(cells[r][c].getValue()/*+cells[r][c].getAddedValues()*/,maxDataVal);
				Menu.getInstance().setProgress(progress);
				progress++;
			}
		}
	}
	
	
	
	public void draw(Graphics2D g2d, int resX, int resY){
	
		for (int r = 0; r < rows; r++){
			
			for (int c = 0; c < cols; ++c)
				cells[r][c].draw(g2d, resX, resY, maxDataVal);
		}
	}

	@Override
	public void run() {
		
		rows = (int) Math.ceil((double)(this.width));
		cols = (int) Math.ceil((double)(this.height));
		
		cells = new GridCell[rows][cols];
		
		
		int max = rows*cols *3;
		Menu.getInstance().setUpProgressBar(progress,max);
		
		for (int r = 0; r < rows; r++){
			
			for (int c = 0; c < cols; c++){
				
				// create cell
				cells[r][c] = new GridCell(new Point2D(c*resX,r*resY), new Point2D( ((c+1)*resX)-1, ((r+1)*resY)-1));
				// check for gazes
				cells[r][c].checkPoints(gazes);
				maxInitialValue = Math.max(cells[r][c].getValue(), maxInitialValue);
				progress++;
				Menu.getInstance().setProgress(progress);
			}
			
		}
		// if cell contains any data use kernel to compute distribution of data
		for (int r = 0; r < rows; r++){
			for (int c = 0; c < cols; c++){
		
				if ((cells[r][c].getValue()) > 0)	{
					
					for (int r_ = r-radius; r_ < r+radius; r_++){
						for (int c_ = c-radius; c_ < c+radius; c_++){
					
							if (r_ >= 0 && r_ < rows && c_ >= 0 && c_ < cols){ 
								
								if (containsGridCell(cells[r][c], cells[r_][c_], radius)){
								
									float dis =  distance(cells[r][c].getCenter().getX(), cells[r][c].getCenter().getY(), cells[r_][c_].getCenter().getX(), cells[r_][c_].getCenter().getY());
									float value = calcValue(dis/radius);
									cells[r_][c_].addValue(value*cells[r][c].getValue(), maxInitialValue);
								}
							}
						}
					}
				}
				
				progress++;
				Menu.getInstance().setProgress(progress);
			}
			
		}
		findMaxDataVal();
		Menu.getInstance().setProgress(0);
		progress = 0;
		// repaint heatmaps
		GUI.getInstance().repaintHeatmaps();
	}

	
}
