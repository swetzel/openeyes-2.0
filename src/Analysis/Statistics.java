package Analysis;



import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import Data.Subject.BTFTrialProperties;
import Data.Subject.EventScreen;
import Data.Subject.Experiment;
import Data.Subject.Screen;
import Data.Subject.Subject;
import Data.Subject.Trial;
import Data.Subject.Experiment.PCCondition;
import Events.Custom.PCEvent.EventType;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.Saccade;
import Output.DataWriter;

public class Statistics {

	private static Statistics instance = new Statistics();
	//private DescriptiveStatistics descriptStats = new DescriptiveStatistics();
	
	
	private Statistics(){}
	
	public static Statistics getInstance() {
        return instance;
    }
	
	/******BTF STUFF***************************************************************************/
	
	public int getRightAnswersPerTrial(Subject sub,String trialGroup){
		
		int right = 0;
		//int trialCounter = 0;
		
		for (Experiment ex : sub.getExperiments()){
			for(Trial t : ex.getTrials()){
				
				if (t.getTrialProperties().getName().contains(trialGroup)){
					
				
					BTFTrialProperties prop = (BTFTrialProperties) t.getTrialProperties();
					if (prop.isResult())
						right++;
					
					//trialCounter++;
				}
			}	
		}
		
		return right;
		//return (float) 1 - errors/trialCounter;
	}
	
	public float getDecisionTimePerTrial(Subject sub,String trialGroup){
		
		float time = 0;
		int trialCounter = 0;

		for (Experiment ex : sub.getExperiments()){
			for(Trial t : ex.getTrials()){
				
				if (t.getTrialProperties().getName().contains(trialGroup)){
					
				
					BTFTrialProperties prop = (BTFTrialProperties) t.getTrialProperties();
					time += prop.getTimeUntilDecision();
					
					trialCounter++;
				}
			}	
		}		
		
		return (float) time/trialCounter;
	}
	
	public float getFixationRatePerTrial(Subject sub,String trialGroup){
		
		float screenTime = 0;

		for (Experiment ex : sub.getExperiments()){
			for(Trial t : ex.getTrials()){
				
				if (t.getTrialProperties().getName().contains(trialGroup)){
					
				
					for (Screen s : t.getScreens()){
						
						screenTime += s.getDuration();
					}
				}
			}	
		}		
		
		return (float) getAbsoluteFixationNumber(sub, trialGroup)/(screenTime/1000);
	}
	
	public int getAbsoluteFixationNumber(Subject sub,String trialGroup){
		
		int fixNumber = 0;
		

		for (Experiment ex : sub.getExperiments()){
			for(Trial t : ex.getTrials()){
				
				if (t.getTrialProperties().getName().contains(trialGroup)){
					
					for (Screen s : t.getScreens()){
						
						fixNumber += s.getFixations().size();
						
					}
				}
			}	
		}		
		return fixNumber;
	}
	
	
	/********************************************************************************************/
	
	public float getExperimentDuration(Experiment ex){
		
		float duration = 0;
	
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
				
				duration += s.getDuration();
			}
		}
		return duration/1000;
	}
	
	public float getScreenDuration(Experiment ex, int screen){	
		

		Trial t = ex.getTrials().get(screen);
		return (float) t.getScreens().get(0).getDuration()/1000f;
		
	}
	
	public float getEventRate(Experiment ex, EventType event){
		
		int number = 0;
		
		for (Trial t : ex.getTrials()){
			
			for (Screen s : t.getScreens()){
		
				EventType e =((EventScreen) s).getTriggerEvent().getEventType(); 
				if ( e == event){
					number++;
				}
					
			}
		}
		
		return (float) number/getNumberOfEvents(ex);
	}
	
	public float getEventRate(Experiment ex){
		
		return (float) getNumberOfEvents(ex)/ex.getDuration()*1000.f;
	}
	
	public int getNumberOfEvents(Experiment ex){
		
		int number = 0;
		
		for (Trial t : ex.getTrials()){
			
			for (Screen s : t.getScreens()){
		
				EventType e =((EventScreen) s).getTriggerEvent().getEventType(); 
				if ( e == EventType.Brush || e == EventType.RemovedBrush || e == EventType.ZoomIn || e == EventType.ZoomOut || e == EventType.Reset){
					number++;
				}
					
			}
		}
		
		return number;
	}
	
	public double getMeanPupilSizeRelative(Subject s,Experiment ex){
		
		System.out.println("ex " + ex.getPupilSizes().getMean() + " sub " + s.getMeanPupilSize());
		
		return (ex.getPupilSizes().getMean() - s.getMeanPupilSize());
	}
	
	public double getMeanPupilSize(Experiment ex){
			
		return ex.getPupilSizes().getMean();
	}
	
	public float getFixationRate(Experiment ex){
		
		return (float) getNumberOfFixations(ex)/getExperimentDuration(ex);
	}

	public float getTaskFixRate(Subject sub, int task){
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getPCTask() == task){
				
				return (float) getFixationRate(ex);
			}		
		}
		return (Float) null;
	}
	
	public float getDesignFixRate(Subject sub, int con){
		
		float rate = 0f;
		float counter = 0f;
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getCondition() == con){
				
				rate += getFixationRate(ex);
				counter++;
			}		
		}
		return  rate/counter;
	}
	

	
	public float getTaskFixMeanTime(Subject sub, int task){
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getPCTask() == task){
				
				return (float) getMeanFixationDuration(ex);
			}		
		}
		return (Float) null;
	}
	
	public float getDesignFixMeanTime(Subject sub, int con){
		
		float rate = 0f;
		float counter = 0f;
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getCondition() == con){
				
				rate += getMeanFixationDuration(ex);
				counter++;
			}		
		}
		return  rate/counter;
	}
	
	
	public float getTaskPupilSD(Subject sub, int task){
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getPCTask() == task){
				
				return (float) ex.getPupilSizes().getStandardDeviation();
			}		
		}
		return (Float) null;
	}
	
	
	public float getDesignPupilSD(Subject sub, int con){
		
		float rate = 0f;
		float counter = 0f;
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getCondition() == con){
				
				rate += ex.getPupilSizes().getStandardDeviation();
				counter++;
			}		
		}
		return  rate/counter;
	}
	
	
	
	public float getTaskPupilVariance(Subject sub, int task){
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getPCTask() == task){
				
				return (float) ex.getPupilSizes().getVariance();
			}		
		}
		return (Float) null;
	}
	
	public float getDesignPupilVariance(Subject sub, int con){
		
		float rate = 0f;
		float counter = 0f;
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getCondition() == con){
				
				rate += ex.getPupilSizes().getVariance();
				counter++;
			}		
		}
		return  rate/counter;
	}
	
	
	public float getTaskPupilMean(Subject sub, int task){
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getPCTask() == task){
				
				return (float) getMeanPupilSize(ex);
			}		
		}
		return (Float) null;
	}
	
	
	
	public float getDesignPupilMean(Subject sub, int con){
		
		float rate = 0f;
		float counter = 0f;
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getCondition() == con){
				
				rate += getMeanPupilSize(ex);
				counter++;
			}		
		}
		return  rate/counter;
	}
	
	
	public float getTaskBlinkRate(Subject sub, int task){
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getPCTask() == task){
				
				
				return getBlinkRate(ex);
			}		
		}
		return (Float) null;
	}
	
	public float getDesignBlinkRate(Subject sub, int con){
	
		float rate = 0f;
		float counter = 0f;
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getCondition() == con){
				
				rate += getBlinkRate(ex);
				counter++;
			}		
		}
		return  rate/counter;
	}
	
	public float getTaskDuration(Subject sub, int task){
		
		for (Experiment ex: sub.getExperiments()){
			if (ex.getPCTask() == task){
				
				return (float) ex.getDuration()/1000f;
			}		
		}
		return (Float) null;
	}
	
	
	public float getDesignDuration(Subject sub, int con){
		
		float time = 0f;
		float counter = 0f;
		
		for (Experiment ex: sub.getExperiments()){
			
			if (ex.getCondition() == con){
				
				time += ex.getDuration();
				counter++;
			}
			
		}
		return (float)time/counter/1000f;
	}
	
	
	public float getMeanFixationDuration(Experiment ex){
		
		long duration = 0;
				
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
				
				for (Fixation f : s.getFixations()){
					
					duration += f.getDuration();
					//System.out.println(f.getDuration());
				}
			}
		}
		
		//System.out.println("minutes " + duration + " rate " + getNumberOfFixations(ex));
		return (float) duration/getNumberOfFixations(ex);
	}
	
	
	
	public float getBlinkRate(Experiment ex){
		
		int rate = 0;
		
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
				
				rate += s.getBlinks().size();
			}
		}
		
		float min = (float) ex.getExperimentDuration()/1000f/60f;	
		//System.out.println(ex.getName() + " " + ex.getPCTask() + " " + min + " " +rate);
		return (float) (rate/min);
		
	}
	
	public float getSaccadeTime(Experiment ex){
		
		float duration = 0;
		float counter = 0;
		
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
									
					float timeOfSaccades = 0;
					
					for (Saccade sacc: s.getSaccades()){
						
						timeOfSaccades += sacc.getDuration();
					}
					counter += s.getSaccades().size();
					duration += timeOfSaccades;	
				}					
		}
		
		return duration/counter/1000;
	}
	
	public float getAverageFixationNumber(Experiment ex){

		float number = 0;
		float counter = 0;
		
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
				
			
				for (Fixation f : s.getFixations()){
					
					++number;
				}
				++counter;			
			}
		}
		return number/counter;
	}
	
	
	/**
	 * Gives the number of gazes for the given experiment
	 * 
	 * @param Experiment
	 * @return Number of gazes 
	 */
	public int getNumberOfGazes(Experiment ex){
		
		int gazes = 0;
		
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
		
				gazes += s.getGazes().size();
			}
		}
		
		return gazes;
	}
	
	public float getGazeRate(Experiment ex, int aoi){
		
		int gazes = 0;
		
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
				
				for (Gaze g : s.getGazes()){
						
						if (s.getAois().get(aoi).containsLocation(g))
							++gazes;
					}
				}		
			}
		return  ((float)gazes/(float)getNumberOfGazes(ex));
	}	
	
	public float getFixationRate(Experiment ex, int aoi){
		
		int fix = 0;
		
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
				
				for (Fixation f : s.getFixations()){
						
						if (s.getAois().get(aoi).containsLocation(f))
							++fix;
					}
				}		
			}
		return  ((float)fix/(float)getNumberOfFixations(ex));
	}
	
	/*
	 * Average Fixation duration
	 */
	public float getFixationTime(Experiment ex){
		
		float duration = 0;
		float counter = 0;
		
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
				
				if (s.getAois().size() > 0){
					
					float timeOfFixations = 0;
					
					for (Fixation f : s.getFixations()){
						
						timeOfFixations += f.getDuration();
					}
					counter += s.getFixations().size();
					duration += timeOfFixations;	
				}		
			}
			
		}
		return duration/counter/1000;
	}
	
	
	public float getFixationTimePerAoi(Experiment ex, int aoi){

		float timeOfFixationsOnAllAois = 0;
		float counter = 0;
		
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
				
				if (s.getAois().size() >= aoi+1){
					
					float timeOfFixationsInOneAoi = 0;
					counter++;
					
					for (Fixation f : s.getFixations()){
						
						if (s.getAois().get(aoi).containsLocation(f))
							timeOfFixationsInOneAoi += f.getDuration();
					}
					timeOfFixationsOnAllAois += timeOfFixationsInOneAoi;	
				}		
			}
			
		}
		
		return ((timeOfFixationsOnAllAois/counter)/1000); 
	}
	
	
	public float getTimePerAoiOverall(Experiment ex, int aoi){
		
		return getFixationTimePerAoi(ex, aoi)/(getExperimentDuration(ex));		
	}
	
	public int getNumberOfFixations(Experiment ex){
		
		int number = 0;
		
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
					
					number += s.getFixations().size();				
			}
		}
		return number;
	}
	
	public float getAOIDwellTime(Experiment ex, int aoi){
		
		float time = 0;
		
		for (Trial t: ex.getTrials()){
			for (Screen s: t.getScreens()){		
				for (Gaze g : s.getGazes()){
					
					if (s.getAois().get(aoi).containsLocation(g))
						time += g.getDuration();
				}	
			}
		}
		
		return time/1000;
	}
	
	public int getNumberOfSaccades(Experiment ex){
		
		int number = 0;
		
		for (Trial t: ex.getTrials()){
			for (Screen s: t.getScreens()){		
				for (Saccade sac : s.getSaccades()){
					
						++number;
				}	
			}
		}
		return number;
	}
	
	
	
	public float getSaccadeRate(Experiment ex){
		
		return (float)getNumberOfSaccades(ex)/ex.getDuration()*1000;
	}
	
	public float getTransitionRateInAoi(Experiment ex, int aoi, int aoi2){
		
		return (float) getTransitionsBetweenAOIs(ex, aoi, aoi2)/getAllTransitions(ex);
	}
	
	public int getAllTransitions(Experiment ex){
		
		// 6,3,2
		return getTransitionsBetweenAOIs(ex, 2, 3) + getTransitionsBetweenAOIs(ex, 2, 6) + getTransitionsBetweenAOIs(ex, 3, 6);
	}
	
	public float getTransitionRate(Experiment ex){
		
		
		return (float)getAllTransitions(ex)/ex.getDuration()*1000;
	}
	
	public float getSaccadeAmplitude(Experiment ex){
		
		float amp = 0;
	
		for (Trial t: ex.getTrials()){
			for (Screen s: t.getScreens()){		
				for (Saccade sac : s.getSaccades()){
					
					amp += sac.getDistance();
				}	
			}
		}
		
		return (float)amp/getNumberOfSaccades(ex);
	}
	
	
	
	
	public float getFixationRateInAoi(Experiment ex, int aoi){
		
		int number = 0;
		
		
		for (Trial t: ex.getTrials()){
			for (Screen s: t.getScreens()){		
				for (Fixation f : s.getFixations()){
					
					if (s.getAois().get(aoi).containsLocation(f))
						++number;
				}	
			}
		}
		
		return number/getAOIDwellTime(ex, aoi);
	}
	
	
	
	public int getNumberOfFixationsInAoi(Experiment ex, int aoi){
		
		int number = 0;
		
		
		for (Trial t: ex.getTrials()){
			for (Screen s: t.getScreens()){		
				for (Fixation f : s.getFixations()){
					
					if (s.getAois().get(aoi).containsLocation(f))
						++number;
				}	
			}
		}
		return number;
	}
	
	
	public float getNumberOfFixationsPerAoi(Experiment ex, int aoi){
		
		float number = 0;
		float numberOfAois =0;
		
		for (Trial t: ex.getTrials()){
			
			for (Screen s: t.getScreens()){
				
				if (s.getAois().size() >= aoi+1){
					
					for (Fixation f : s.getFixations()){
						
						if (s.getAois().get(aoi).containsLocation(f))
							++number;
					}	
					numberOfAois++;
				}		
			}
			
		}
		return number/numberOfAois;
	}

	public float getNumberOfFixationsOverall (Experiment ex, int aoi){
		
		return (float) (getNumberOfFixationsPerAoi(ex, aoi))/(float)getNumberOfFixations(ex);
	}
	
	
	public float getTransitions(Experiment ex, int aoi1, int aoi2){
		
		int transitions = 0;
		int counter = 0;
		
		for (Trial t : ex.getTrials()){
				
			Screen s = t.getScreens().get(0);
			
			if (s.getAois().size() > 1){
				
				s.calculateTransitionMatrix(true);
				transitions += s.getTransitionBetween(aoi1, aoi2); 
				transitions += s.getTransitionBetween(aoi2, aoi1);
				counter++;
			}
		}
		//System.out.println(transitions + " " + counter);
		return (float) transitions/counter;
	}
	
	public int getTransitionsBetweenAOIs(Experiment ex, int aoi1, int aoi2){
		
		int transitions = 0;
				
		for (Trial t : ex.getTrials()){
			 for (Screen s : t.getScreens()){
				
				s.calculateTransitionMatrix(true);
				transitions += s.getTransitionBetween(aoi1, aoi2); 
				transitions += s.getTransitionBetween(aoi2, aoi1);
				
			}
		}
		//System.out.println(transitions + " " + counter);
		return transitions;
	}
	
	public int getTransitionsPerScreen(int screen, Experiment ex, int aoi1, int aoi2){
		
		int transitions = 0;
		Screen s = ex.getTrials().get(screen).getScreens().get(0);
		
		if (s.getAois().size() > 1){
			
			s.calculateTransitionMatrix(true);
			transitions += s.getTransitionBetween(aoi1, aoi2); 
			transitions += s.getTransitionBetween(aoi2, aoi1);
		}
		
		return transitions;
	}
	
	public int getTimeOnScreen(int screen, Experiment ex){
		
		Screen s = ex.getTrials().get(screen).getScreens().get(0);		
		return s.getDuration()/1000;
	}
	
	public float getAvarageTimeOfFixationsInAoiPerScreen(int screen, Experiment ex, int aoi){
		
		return getTimeOnAoiPerScreen(screen, ex, aoi)/(float) getFixationNumberInAoiPerScreen(screen, ex, aoi);
		
	}
	
	public int getFixationNumberInAoiPerScreen(int screen, Experiment ex, int aoi){
		
		int number = 0;
		Screen s = ex.getTrials().get(screen).getScreens().get(0);
		
		if (aoi < s.getAois().size()){
			
			for (Fixation f : s.getFixations()){
				
				if (s.getAois().get(aoi).containsLocation(f))
					++number;
			}
		}
		return number;
	}
	
	public float getTimeOnAoiPerScreen(int screen, Experiment ex, int aoi){
		
		float time = 0;
		Screen s = ex.getTrials().get(screen).getScreens().get(0);
		
		if (aoi < s.getAois().size()){
			
			for (Fixation f : s.getFixations()){
				
				if (s.getAois().get(aoi).containsLocation(f))
					time += f.getDuration();
			}
		}
		return time/1000;
	}
	/*
	public float getAverageTimeOnScreen(int screen, Study study){
		
		int experiments = 0;
		
		for (Experiment ex : study.getExperiments()){
			
			if (ex.isEnabled()){
				experiments++;
			}
			
		}
		return (float) getTimeOnScreen(screen,study)/experiments;
		
	}
	*/
	
	
	/*
	public float getTimePerScreeninAoi(int screen,Study study, int aoi){
		
		float overallTimeonScreen = 0;
		int experimentCounter = 0;
		
		for (Experiment ex : study.getExperiments()){
			
			if (ex.isEnabled()){
		
				Screen s = ex.getTrials().get(screen).getScreens().get(0);
				overallTimeonScreen += s.getDuration();
				experimentCounter++;
			}
				
		}
	
		return 
	}*/
	
	
}
