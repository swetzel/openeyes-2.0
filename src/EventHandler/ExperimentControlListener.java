package EventHandler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Data.Subject.Experiment;
import View.Experiment.ExperimentControl;
import View.Experiment.ExperimentImagePanel;
import View.Experiment.ExperimentImagePanel.PlayMode;

public class ExperimentControlListener implements ActionListener{

	private ExperimentImagePanel imagePanel;
	private ExperimentControl control;
	
	public ExperimentControlListener(ExperimentImagePanel imagePanel, ExperimentControl control){
		
		this.imagePanel = imagePanel;
		this.control = control;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		long timeStamp = 0;
		
		//System.out.println(e.getActionCommand());
		
		if (e.getActionCommand().equals("play")){
			imagePanel.setExperimentPlayMode(PlayMode.PLAY);
			imagePanel.play();
		}
		else if (e.getActionCommand().equals("pause")){
			imagePanel.setExperimentPlayMode(PlayMode.PAUSE);
			imagePanel.pause();
		}
		else if (e.getActionCommand().equals("stop")){
			
			imagePanel.setExperimentPlayMode(PlayMode.STOP);
			imagePanel.stop();
		}
		
		if (e.getActionCommand().equals("next trial")){
			imagePanel.nextTrial();
			timeStamp = imagePanel.getActiveTrial().getStartTime();
		}
		else if (e.getActionCommand().equals("previous trial")){	
			imagePanel.previousTrial();
			timeStamp = imagePanel.getActiveTrial().getStartTime();
		}
		else if (e.getActionCommand().equals("next screen")){
			imagePanel.nextScreen();
			timeStamp = imagePanel.getActiveScreen().getTimestampStart();
		}
		else if (e.getActionCommand().equals("previous screen")){
			imagePanel.previousScreen();
			timeStamp = imagePanel.getActiveScreen().getTimestampStart();
		}
		
		// set slider
		control.setTimeLine(timeStamp);
		imagePanel.repaint();
	}

}
