package EventHandler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import View.Study.StudyToolbar;

public class StudyFrameToolBarListener implements ActionListener{

	private StudyToolbar toolBar;
	
	public StudyFrameToolBarListener(StudyToolbar toolBar){
		
		this.toolBar = toolBar;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getActionCommand().equals("Show Gazes") || e.getActionCommand().equals("Show Fixations") || e.getActionCommand().equals("Show Saccades")){
			
			toolBar.setEyeDataVisibility();
		}
		else if (e.getActionCommand().equals("Show Aois")){
			
			toolBar.setAoiVisibility();
		}

	}
	
}
