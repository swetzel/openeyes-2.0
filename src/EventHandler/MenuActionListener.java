package EventHandler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import Analysis.FixationDetection;
import Data.Study.Study;
import Data.Subject.Experiment;
import Input.BTF2Parser;
import Input.ELearningParser;
import Input.EyeTribeParserPC;
import Input.BTFParser;
import Input.HexParser;
import Input.OEParser;
import Output.DataWriter;
import Output.DriftCorrectionWriter;
import View.GUI;
import View.Experiment.ExperimentImagePanel;
import View.Menu.Menu;
import View.Study.StudyFrame;

public class MenuActionListener implements ActionListener {

	private static MenuActionListener instance = new MenuActionListener();
	
	private MenuActionListener(){
		
	}
	
	public static MenuActionListener getInstance() {
        return instance;
    }
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		GUI gui = GUI.getInstance();
		Menu menu = Menu.getInstance();
		OEParser parser;
		
		DriftCorrectionWriter driftWriter = new DriftCorrectionWriter();
		
		
		if (arg0.getActionCommand().equalsIgnoreCase("load elearning")){
						
			parser = new ELearningParser();
			
			gui.fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = gui.fc.showOpenDialog(GUI.getInstance());
			
			if (returnVal == JFileChooser.APPROVE_OPTION) { 
		            
			 	File file = gui.fc.getSelectedFile();
			 	Study study = null;
			 	gui.study = null;
			 	
			 	try {
					study = parser.loadStudy(file.getAbsolutePath());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			 	gui.study = study;
			 	gui.displayExperiments();	
			 	StudyFrame.getInstance().initExperiments(study);
			}
			
			else {
				System.out.println("Open command cancelled by user.");
	        }
		}
		else if (arg0.getActionCommand().equalsIgnoreCase("load hex")){
			
			parser = new HexParser();
			
			gui.fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = gui.fc.showOpenDialog(GUI.getInstance());
			
			if (returnVal == JFileChooser.APPROVE_OPTION) { 
		            
			 	File file = gui.fc.getSelectedFile();
			 	Study study = null;
			 	gui.study = null;
			 	
			 	try {
					study = parser.loadStudy(file.getAbsolutePath());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			 	gui.study = study;
			 	gui.displayExperiments();	
			 	StudyFrame.getInstance().initExperiments(study);
			}
			
			else {
				System.out.println("Open command cancelled by user.");
	        }
		}
		
		
		else if (arg0.getActionCommand().equalsIgnoreCase("load btf")){
			
			parser = new BTF2Parser();
			
			gui.fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = gui.fc.showOpenDialog(GUI.getInstance());
			
			if (returnVal == JFileChooser.APPROVE_OPTION) { 
		            
			 	File file = gui.fc.getSelectedFile();
			 	Study study = null;
			 	gui.study = null;
			 	
			 	try {
					study = parser.loadStudy(file.getAbsolutePath());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			 	gui.study = study;
			 	gui.displayExperiments();	
			 	StudyFrame.getInstance().initExperiments(study);
			}
			
			else {
				System.out.println("Open command cancelled by user.");
	        }
		}
		else if (arg0.getActionCommand().equalsIgnoreCase("load pc")){
			
			parser = new EyeTribeParserPC();
			
			gui.fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = gui.fc.showOpenDialog(GUI.getInstance());
			
			if (returnVal == JFileChooser.APPROVE_OPTION) { 
		            
			 	File file = gui.fc.getSelectedFile();
			 	Study study = null;
			 	gui.study = null;
			 	
			 	try {
					study = parser.loadStudy(file.getAbsolutePath());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			 	gui.study = study;
			 	gui.displayExperiments();	
			 	StudyFrame.getInstance().initExperiments(study);
			 	gui.calculateBlinks(100);
			 	gui.calculateFixations(menu.getDurationThreshold(), menu.getDispersionThreshold());
			}
			
			else {
				System.out.println("Open command cancelled by user.");
	        }
		}
		
		
		else if (arg0.getActionCommand().equalsIgnoreCase("prop")){
		
			JComboBox cb = (JComboBox)arg0.getSource();
	       menu.updateFilter((String) cb.getSelectedItem());
			//String filter = (String)cb.getSelectedItem();
	        //menu.filterExperiments(filter);
		}
		else if (arg0.getActionCommand().equalsIgnoreCase("filter")){
		
			if (menu.isFilterActive())
				menu.filterExperiments(menu.getFilterName(),menu.getFilterValue());
		}
		
		else if (arg0.getActionCommand().equalsIgnoreCase("sort")){
			
			JComboBox cb = (JComboBox)arg0.getSource();	
			if (cb.getSelectedIndex() != -1)
				menu.sortExperiments((String) cb.getSelectedItem());
		}
		
		else if (arg0.getActionCommand().equalsIgnoreCase("toggle order")){
			menu.toogleSortingOrder();
		}
		else if (arg0.getActionCommand().equalsIgnoreCase("apply heatmap")){
			
			gui.recalculateHeatmaps();
		}
		
		else if (arg0.getActionCommand().equalsIgnoreCase("export")){
			
			try {
				DataWriter.getInstance().writeData(gui.getStudy());
			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		else if (arg0.getActionCommand().equalsIgnoreCase("detect fixations")){
			
			
			gui.calculateFixations(menu.getDurationThreshold(), menu.getDispersionThreshold());
			
			
		}
		
	}

}
