package EventHandler;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import View.Experiment.CLView;

public class CognitiveLoadPanelListener implements ItemListener {

	private CLView view;
	
	public CognitiveLoadPanelListener(CLView view) {
		this.view = view;
	}
	
	@Override
	public void itemStateChanged(ItemEvent e) {
	
		view.updateCheckboxes();
	}

}
