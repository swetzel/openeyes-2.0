package EventHandler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import View.Experiment.ExperimentControl;
import View.Experiment.ExperimentImagePanel;
import View.Study.StudyControl;
import View.Study.StudyImagePanel;

public class StudyControlListener implements ActionListener {

	private StudyImagePanel imagePanel;
	private StudyControl control;
	
	
	public StudyControlListener(StudyImagePanel imagePanel, StudyControl control){
		
		this.imagePanel = imagePanel;
		this.control = control;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getActionCommand().equals("next trial")){
			imagePanel.nextTrial();
			
		}
		else if (e.getActionCommand().equals("previous trial")){	
			imagePanel.previousTrial();
			
		}
		else if (e.getActionCommand().equals("next screen")){
			imagePanel.nextScreen();
			
		}
		else if (e.getActionCommand().equals("previous screen")){
			imagePanel.previousScreen();
			
		}
		
		imagePanel.repaint();
	}
	
}
