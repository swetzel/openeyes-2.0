package EventHandler;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import View.Experiment.ExperimentImagePanel;
import View.Experiment.ExperimentImagePanel.PlayMode;

public class TimeLineListener implements ChangeListener{

	private ExperimentImagePanel panel;
	
	public TimeLineListener(ExperimentImagePanel panel){
		
		this.panel = panel;
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {

		JSlider source = (JSlider)e.getSource();
	    if (!source.getValueIsAdjusting()) {
	    	
	    	if (panel.getPlayMode() == PlayMode.PAUSE || panel.getPlayMode() == PlayMode.STOP){
	    		int value = (int)source.getValue();
	    		panel.loadScreen(value);
	    		
	    	}
	    }
		
	}

}
