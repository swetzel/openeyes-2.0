package EventHandler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JMenuItem;

import View.GUI;
import View.Menu.Menu;
import View.Menu.PopUpMenu;
import View.Study.StudyFrame;

public class PopUpListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getActionCommand().contains("study")){
		
			try {
				StudyFrame.getInstance().display(GUI.getInstance().getStudy(),GUI.getInstance().getContentPane().getWidth()- Menu.getInstance().getWidth(), GUI.getInstance().getContentPane().getHeight());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		else if (arg0.getActionCommand().contains("experiment")){	
			
			//System.out.println(arg0.getActionCommand());
			
			String[] split = arg0.getActionCommand().split(" ");
			
			try {
				GUI.getInstance().showExperiment(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
		
			} catch (NumberFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PropertyVetoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (arg0.getActionCommand().contains("load")){
			
			String[] split = arg0.getActionCommand().split(" ");
			
			try {
				GUI.getInstance().showCLView(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
			} catch (NumberFormatException | PropertyVetoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}

	
   
}
