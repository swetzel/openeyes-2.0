package EventHandler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import View.Experiment.ExperimentToolbar;
import View.Experiment.ExperimentImagePanel;
import View.Menu.Menu;

public class ExperimentFrameToolBarListener implements ActionListener{

	private ExperimentImagePanel imagePanel;
	private ExperimentToolbar toolBar;
	
	public ExperimentFrameToolBarListener(ExperimentToolbar toolBar,ExperimentImagePanel imagePanel){
		
		this.imagePanel = imagePanel;
		this.toolBar = toolBar;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getActionCommand().equals("Show Gazes") || e.getActionCommand().equals("Show Fixations") || e.getActionCommand().equals("Show Saccades")){
			
			toolBar.setEyeDataVisibility();
		}
		else if (e.getActionCommand().equals("Show Aois")){
			
			toolBar.setAoiVisibility();
		}
		else if (e.getActionCommand().equals("Show Heatmap")){
		
			toolBar.setHeatmapVisibility();			
		}
		
		else if (e.getActionCommand().equals("Show MouseMove")){
			
			toolBar.setMousePositionVisibility();
			
		}
		
		else if (e.getActionCommand().equals("Show MouseClick")){
			
			
		}
		
		
	}
	
}
