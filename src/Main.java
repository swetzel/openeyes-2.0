import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import View.GUI;

/**
 * 
 * @author Stefanie Wetzel <stefanie.wetzel@gmail.com>
 *
 *
 */
public class Main {

	public static void main(String[] args) {
		try {
	           
	        UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
	    } 
	    catch (UnsupportedLookAndFeelException e) {
	       // handle exception
	    }
	    catch (ClassNotFoundException e) {
	       // handle exception
	    }
	    catch (InstantiationException e) {
	       // handle exception
	    }
	    catch (IllegalAccessException e) {
	       // handle exception
	    }
	        
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
       	GUI.getInstance();
       }
   });
	

		
	}
}
