package Input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import Data.AreaOfInterest;
import Data.AreaOfInterest.AOIShape;
import Data.Study.Study;
import Data.Point2D;
import Data.PropertyFilterValues;
import Data.PropertySortingValues;

public class AoiReader {

	
	public Study readFile(Study study, String path) throws IOException{
		
		//ArrayList<AreaOfInterest> aois = new ArrayList<AreaOfInterest>();
		BufferedReader br = new BufferedReader(new FileReader(path));
	   
		try {
	    	
	        String strLine;
	        String[] split;
        	int screen;
        	int trial;
        	int numberOfAois;
        	//ArrayList<AttributeRange> studyAttributes = new ArrayList<AttributeRange>();
        	
	        while ((strLine = br.readLine()) != null)  {
	           	
	        	
	        	// read attribute of study
	        	if (strLine.startsWith("Prop")){
	        		
	        		split = strLine.split(" ");
	        		if (split[1].equals("1")){ //filter
	        			
	        			PropertyFilterValues newFilter = new PropertyFilterValues(split[2]);
	        			
	        			// add values
	        			for (int i = 3; i < split.length; ++i)
	        				newFilter.addValue(split[i]);
	        				
	        			study.addPropertyFilterValue(newFilter);
	        		}
	        		else if (split[1].equals("2")){ // sorting
	        			
	        			PropertySortingValues newSorting = new PropertySortingValues(split[2],Float.parseFloat(split[3]),Float.parseFloat(split[4]));
	        			study.addPropertySortingValue(newSorting);
	        			
	        		}
	        	}
	        	// start of a new aoi
	        	else if (strLine.startsWith("Trial")){
	           
	        		split = strLine.split(" ");
	        		trial = Integer.parseInt(split[1]);
	        		screen = Integer.parseInt(split[3]);
	        		numberOfAois = Integer.parseInt(split[5]);
	        		AreaOfInterest newArea = null;
	        		
	        		
	        		for (int i = 0; i < numberOfAois; ++i){
	        			
	        			strLine = br.readLine();
	        			split = strLine.split(" ");	        		
	        			String shape =  split[1];
	        			
	        			if (shape.equalsIgnoreCase("polygon"))
	        				newArea= new AreaOfInterest(true, split[0], AOIShape.POLYGON);
	        			else if (shape.equalsIgnoreCase("rectangle"))
	        				newArea= new AreaOfInterest(true, split[0], AOIShape.RECTANGLE);
	        			else if (shape.equalsIgnoreCase("ellipse"))
	        				newArea= new AreaOfInterest(true, split[0], AOIShape.ELLIPSE);
	        			
	        			
	    				for (int j = 2; j < split.length; ++j){
	    				
	    					newArea.addPoint(new Point2D(Float.parseFloat(split[j]),Float.parseFloat(split[j+1])));
	    					newArea.setScreen(screen);
	    					newArea.setTrial(trial);
	    					++j;
	    				}
	    				study.addAoi(newArea, newArea.getTrial(), newArea.getScreen());
	        		}
	        					
	        	}
	        }
	    } finally {
	        br.close();
	    }
		return study;
	}
}
