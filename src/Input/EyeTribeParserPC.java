package Input;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import Data.AreaOfInterest;
import Data.Point2D;
import Data.AreaOfInterest.AOIShape;
import Data.Study.Study;
import Data.Subject.EventScreen;
import Data.Subject.Experiment;
import Data.Subject.Experiment.PCCondition;
import Data.Subject.Screen;
import Data.Subject.Subject;
import Data.Subject.Trial;
import Events.Custom.PCEvent;
import Events.Custom.PCEvent.EventType;
import Events.Default.Blink;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.MouseClick;
import Events.Default.MousePosition;
import Events.Default.Saccade;


public class EyeTribeParserPC extends OEParser{

	@Override
	protected Experiment readExperiment(String path, String imagePath,
			boolean enable, int id) throws IOException {
		
		Experiment newExperiment = new Experiment(path, enable, id, false);
		EventScreen screen = null;
		PCEvent newEvent = null;
		Trial trial = null;
		String strLine;
		
		File file = new File(path);
		
		String [] nameSplit = path.split("_");
		newExperiment.setName(nameSplit[2]+"_"+nameSplit[3]);
		//newExperiment.setName(file.getName());
		
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		long currentTimeStamp = 0;
		long oldTimeStamp = 0;
		
		//PropertyParser driftParser = new PropertyParser();
		
		//read file line by line
		while ((strLine = br.readLine()) != null)   {
								
					
			// get screen resolution
			if (strLine.contains("Resolution")){
			
				String[] line = strLine.split(" "); //split by space
				newExperiment.setScreenWidth(Integer.parseInt(line[1]));
				newExperiment.setScreenHeight(Integer.parseInt(line[2]));
				
			}
					
			else if (strLine.contains("Condition")){
				
				String[] line = strLine.split(" "); //split by space
				
				if (line[1].equals("SINGLE_AXIS_ZOMMING")){
					newExperiment.setCondition(PCCondition.SINGLE_AXIS_ZOOM);
				}
				else if (line[1].equals("DIMENION_ZOOMING")){
					newExperiment.setCondition(PCCondition.MULTI_AXIS_ZOOM);
				}
				else if (line[1].equals("SINGLE_AXIS_ZOMMING_WITH_CONTEXT")){
					newExperiment.setCondition(PCCondition.SINGLE_AXIS_ZOOM_OVERVIEW);
				}
				else if (line[1].equals("DIMENION_ZOOMING_WITH_CONTEXT")){
					newExperiment.setCondition(PCCondition.MULTI_AXIS_ZOOM_OVERVIEW);
				}
			}
			
			// find start of trial		
			else if (strLine.contains("Start_trial")){
				
				String[] line = strLine.split(" "); //split by space
				currentTimeStamp = Long.parseLong(line[2]);
				
				if (oldTimeStamp == 0)
					oldTimeStamp = currentTimeStamp -1;
				
				
				
				if (trial != null){
					
					if (isCurrentTimeStampValid(oldTimeStamp, currentTimeStamp))
						trial.setEndTime(currentTimeStamp-1);
					else 
						trial.setEndTime(oldTimeStamp-1);
					
					trial.addScreen(screen);
					newExperiment.addTrial(trial);
				}
				
				
				if (isCurrentTimeStampValid(oldTimeStamp, currentTimeStamp)){
					trial = new Trial(currentTimeStamp);
					newExperiment.setPCTask(Integer.parseInt(line[1]));
					oldTimeStamp = currentTimeStamp;
				}
				else {
					trial = new Trial(oldTimeStamp);
					newExperiment.setPCTask(Integer.parseInt(line[1]));
				}
				
			}
				
			else if ((strLine.contains("Activate_Zoom_Mode") || strLine.contains("Zoom_in")|| strLine.contains("Zoom_out") ||
					 strLine.contains("Activate_Brush_Mode") || strLine.contains("Brush_data") || strLine.contains("Removed_brush") || strLine.contains("Reset")) && trial != null && newEvent == null){
				
				String[] line = strLine.split(" "); //split by space
				currentTimeStamp = Long.parseLong(line[1]);
				
				if (oldTimeStamp == 0)
					oldTimeStamp = currentTimeStamp -1;
				
				if (isCurrentTimeStampValid(oldTimeStamp, currentTimeStamp)){
				
					newEvent = new PCEvent(currentTimeStamp,line[0]);
					oldTimeStamp = currentTimeStamp;
				}
				else
					newEvent = new PCEvent(oldTimeStamp,line[0]);
			}
			
			
			else if (strLine.contains("Screen") && trial != null){
				
				String[] line = strLine.split(" "); //split by space
				
				if (screen != null){	
					screen.setTimestampEnd(Long.parseLong(line[1])-1);
					trial.addScreen(screen);
				}
				
				screen = new EventScreen(Long.parseLong(line[1]), imagePath+"\\"+line[2] ); // check if ending is correct
				
				if (newEvent == null){
					newEvent = new PCEvent(Long.parseLong(line[1]), "Default");
				}
				screen.setTriggerEvent(newEvent);
				currentTimeStamp = Long.parseLong(line[1]);
				newEvent = null;
				
			}
					
			
			else if (strLine.contains("End_trial") && trial != null){
				
				String[] line = strLine.split(" ");
				screen.setTimestampEnd(Long.parseLong(line[1]));
				trial.setEndTime(Long.parseLong(line[1]));
				newExperiment.addTrial(trial);
				if (screen != null)
					trial.addScreen(screen);
				
				trial = null;
				screen = null;
				currentTimeStamp = Long.parseLong(line[1]);
				
			}
					
			// gazes 
			else if(strLine.contains("Gaze") && screen != null){
				
				//new gaze
				String[] line = strLine.split(" "); 
				currentTimeStamp = Long.parseLong(line[1]);
			
				if (oldTimeStamp == 0)
					oldTimeStamp = currentTimeStamp -1;
				
				
				//System.out.println(currentTimeStamp + " " + oldTimeStamp);
				
				if (isCurrentTimeStampValid(oldTimeStamp, currentTimeStamp)){
					Gaze newGaze = new Gaze(Long.parseLong(line[1]),(int) Double.parseDouble(line[2]),(int) Double.parseDouble(line[3]),(int) Double.parseDouble(line[4]),
													 (int) Double.parseDouble(line[5]), Float.parseFloat(line[6]),Float.parseFloat(line[7]));
			
					screen.addGaze(newGaze);
					oldTimeStamp = currentTimeStamp;
				}
			
			}
			
			/*
			else if (strLine.contains("Mouse_Position") && screen != null){
				
				String[] line = strLine.split(" "); 
				currentTimeStamp = Long.parseLong(line[1]);
				
				if (oldTimeStamp == 0)
					oldTimeStamp = currentTimeStamp -1;
				
				if (isCurrentTimeStampValid(oldTimeStamp, currentTimeStamp)){
					MousePosition newPos = new MousePosition(currentTimeStamp, Integer.parseInt(line[2]), Integer.parseInt(line[3]));
					screen.addMousePosition(newPos);
					oldTimeStamp = currentTimeStamp;
				}
			}
			
			else if (strLine.contains("Mouse_Click") && screen != null){
				
				String[] line = strLine.split(" "); 
				MouseClick newClick = new MouseClick(Long.parseLong(line[1]));
				screen.addMouseClick(newClick);
				
			}
			*/
			
			else if (strLine.contains("Geistige_Anforderung")){
				
				String[] line = strLine.split(" "); 
				newExperiment.getCognitiveLoad().setMentalDemand(Integer.parseInt(line[1]));
			}
			
			else if (strLine.contains("Körperliche_Anforderung")){
				
				String[] line = strLine.split(" "); 
				newExperiment.getCognitiveLoad().setPhysicalDemand(Integer.parseInt(line[1]));
			}
			
			else if (strLine.contains("Zeitliche_Anforderung")){
				
				String[] line = strLine.split(" "); 
				newExperiment.getCognitiveLoad().setTemporalDemand(Integer.parseInt(line[1]));
			}
			
			else if (strLine.contains("Leistung")){
				
				String[] line = strLine.split(" "); 
				newExperiment.getCognitiveLoad().setPerformance(Integer.parseInt(line[1]));
			}
			
			else if (strLine.contains("Anstrengung")){
	
				String[] line = strLine.split(" "); 
				newExperiment.getCognitiveLoad().setEffort(Integer.parseInt(line[1]));
			}
			
			else if (strLine.contains("Frustration")){
				
				String[] line = strLine.split(" "); 
				newExperiment.getCognitiveLoad().setFrustration(Integer.parseInt(line[1]));
			}
		}
				
		//Close the input stream
		in.close();
		
			
		//push last screen/trial
		if (screen != null && trial != null){
		
			trial.setEndTime(currentTimeStamp);
			screen.setTimestampEnd(currentTimeStamp);
			trial.addScreen(screen);
			newExperiment.addTrial(trial);
			//System.out.println("End Trial ");
		}
			
	//		if (newExperiment.hasDrift(path.replace(".asc", "_drift.txt")))
	//			newExperiment = driftParser.parseAttributes(path.replace(".asc", "_drift.txt"), newExperiment);
		
				
		return newExperiment;
	}

	private boolean isCurrentTimeStampValid(long oldT, long newT){
		
		return oldT < newT && Math.abs(oldT - newT) < 5000;
	}
	
	@Override
	public Study loadStudy(String path) throws IOException {
		
		Study study = new Study(path, true);
		File file = new File(path);
		
		int subjectId = 0;
		for (File f : file.listFiles()){ // every folder is one subject, each task has its own log file
			
			String filename = null;
			
			// subject
			if (f.isDirectory()){
				
				imagePath = f.getAbsolutePath() +  "\\screens\\";
				study.addSubject(loadSubject(f,imagePath, subjectId));
				++subjectId;
				//for (File subs : f.listFiles()){
					
					
					
					//System.out.println(subs.getName());
					//if (subs.getAbsolutePath().contains(".txt"))
						//filename = ex.getAbsolutePath();
						
				//}
			}
			else{
				
				if (f.getAbsolutePath().contains("aois.oe")){ //aois for all screens of the study
					
					loadAois(f, study);
				}
			}
			
		}
		study.updateScreensWithStudyAois();
		
		return study;
	}
	
	private Study loadAois(File file, Study study) throws IOException{
		
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		while ((strLine = br.readLine()) != null){
			
			
			String[] split = strLine.split(" ");
			AreaOfInterest newAoi = null;
			
			if (split[1].equals("rectangle")){
				newAoi = new AreaOfInterest(true, split[0],AOIShape.RECTANGLE);
			}
			else if (split[1].equalsIgnoreCase("ellipse")){
				newAoi = new AreaOfInterest(true, split[0],AOIShape.ELLIPSE);
			}
			else if (split[1].equalsIgnoreCase("polygon")){
				newAoi = new AreaOfInterest(true, split[0],AOIShape.POLYGON);
			}
			
			for (int j = 2; j < split.length; ++j){
				
				newAoi.addPoint(new Point2D(Float.parseFloat(split[j]),Float.parseFloat(split[j+1])));
				++j;
			}
			
			study.addAoi(newAoi);
		}
			
		return study;
	}
	
	
	private Subject loadSubject(File sub, String imagePath, int id){
		
		Subject newSubject = new Subject(sub.getName(), id);
		
		int experimentId = 0;
		for (File s : sub.listFiles()){
			
			if (!s.isDirectory() && s.getName().contains(".txt")){
				
				try {
					newSubject.addExperiment(readExperiment(s.getAbsolutePath(), imagePath, true, experimentId));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				++experimentId;
			}
		}
		
		return newSubject;
	}

}
