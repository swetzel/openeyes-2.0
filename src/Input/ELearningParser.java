package Input;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import Data.AreaOfInterest;
import Data.Study.Study;
import Data.Subject.Experiment;
import Data.Subject.Screen;
import Data.Subject.Subject;
import Data.Subject.Trial;
import Events.Default.Blink;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.Saccade;


/**
 * 
 * @author Stefanie Wetzel <stefanie.wetzel@gmail.com>
 *
 *  Class to parse the ASCII version of an edf file. 
 */
public class ELearningParser extends OEParser {

	
	public ELearningParser(){
		
	}
	
		
	/*
	 * Example implementation for the eLearning study. One trial contains one Screen.
	 */
	@Override
	protected Experiment readExperiment(String path, String imagePath, boolean enable, int id) throws IOException{
		
		System.out.println("experiment " + path);
		
		Experiment newExperiment = new Experiment(path, enable, id, true);
		Screen screen = null;
		Trial trial = null;
		String strLine;
		
		File file = new File(path);
		newExperiment.setName(file.getName());
		
		file = cleanFile(file);
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		
		boolean readData = false;
		boolean openFixation = false;
		boolean ignoreFixation = false;
		boolean openSaccade = false;
		boolean ignoreSaccade = false;
		Fixation currentFixation = null;
		Saccade currentSaccade = null;
		
		PropertyParser driftParser = PropertyParser.getInstance();
		
		
		//read file line by line
		while ((strLine = br.readLine()) != null)   {
						
			
			// get screen resolution
			if (strLine.contains("DISPLAY_COORDS")){
			
				//System.out.println(newExperiment.getName());
				String[] line = strLine.split(" "); //split by space
				newExperiment.setScreenWidth(Integer.parseInt(line[5])+1);
				newExperiment.setScreenHeight(Integer.parseInt(line[6])+1);
			}
			
			// second version of screen resolution 
			if (strLine.contains("GAZE_COORDS")){
				
				String[] line = strLine.split(" "); //split by space
				newExperiment.setScreenWidth((int)(Float.parseFloat(line[5]))+1);
				newExperiment.setScreenHeight((int)(Float.parseFloat(line[6]))+1);
			}
			
			// for eLearning example --> one trial == one screen
			// find screen id --> new screen will be displayed
			if (strLine.contains("Showing Screen")){
				
				String[] line = strLine.split(" "); //split by space
				
				if (trial != null){
					
					trial.addScreen(screen);
					newExperiment.addTrial(trial);
				}
						
				// ignore saccades and fixations that start at one screen and end the following
				if (openSaccade)
					ignoreSaccade = true;
				if (openFixation)
					ignoreFixation = true;
				
				
				
				trial = new Trial(Integer.parseInt(line[1]));
				screen = new Screen(Integer.parseInt(line[1]), imagePath + line[4]+".jpg"); // check if ending is correct
			}
			
			
			// get AOIs (same AOIs for all screens and trials of this experiment)
			/*
			else if (strLine.contains("IAREA")){
				
				String[] line = strLine.split(" "); //split by space
				AreaOfInterest newArea = new AreaOfInterest(true);
				for (int i = 6; i < line.length-1; ++i){
				
					String[] point = line[i].split(",");
					
					newArea.addPoint(new Point2D(Integer.parseInt(point[0]),Integer.parseInt(point[1])));
				}
				newArea.setName(line[line.length-1]);
				newExperiment.addAOIToExperment(newArea);
			}
			*/
			//find start of eye data
			else if (strLine.contains("SAMPLES GAZE")){
				readData = true;
				strLine = br.readLine();
			}
			
			//find end of eye data
			else if (readData && (strLine.contains("Stop tracking") || (strLine.contains("END") && strLine.contains("SAMPLES EVENTS")))){
				
				readData = false;
				
				String[] line = strLine.split(" ");
				screen.setTimestampEnd(Integer.parseInt(line[1]));
				trial.setEndTime(Integer.parseInt(line[1]));
			}
			
			//start saccade
			else if(strLine.contains("SSACC")){
				
				String[] line = strLine.split(" "); //split by space
				currentSaccade = new Saccade(Integer.parseInt(line[2]));
				openSaccade = true;
			}
			
			//end saccade
			else if(strLine.contains("ESACC")){
				
				//if(!ignoreSaccade && openSaccade){
				if(openSaccade){
					
					String[] line = strLine.split(" "); //split by space
					
					if(!line[5].toString().equalsIgnoreCase(".") &&
					   !line[6].toString().equalsIgnoreCase(".")&&
					   !line[7].toString().equalsIgnoreCase(".")&&
					   !line[8].toString().equalsIgnoreCase(".")){
						
						currentSaccade.setTimestampEnd(Integer.parseInt(line[3]));
						currentSaccade.setX((int) Double.parseDouble(line[5]));
						currentSaccade.setY((int) Double.parseDouble(line[6]));
						currentSaccade.setEndX((int) Double.parseDouble(line[7]));
						currentSaccade.setEndY((int) Double.parseDouble(line[8]));
						
						screen.addSaccade(currentSaccade);
					}
				}
				openSaccade = false;
				ignoreSaccade = false;
				currentSaccade = null;
			}
			
			//start fixation
			else if(strLine.contains("SFIX")){
				
				String[] line = strLine.split(" "); //split by space
				currentFixation = new Fixation(Integer.parseInt(line[2]));
				openFixation = true;
			}
			
			//end fixation
			else if(strLine.contains("EFIX")){
				
				//if(!ignoreFixation && openFixation){
				if(openFixation){
					
					String[] line = strLine.split(" "); //split by space
					
					
					if(!line[5].toString().equalsIgnoreCase(".") &&
					   !line[6].toString().equalsIgnoreCase(".") &&
					   (int) Double.parseDouble(line[5]) <= newExperiment.getScreenWidth() && // is fixation on screen?
					   (int) Double.parseDouble(line[5]) > 0 &&
					   (int) Double.parseDouble(line[6]) <= newExperiment.getScreenHeight() &&
					   (int) Double.parseDouble(line[6]) > 0){
						
						currentFixation.setTimestampEnd(Integer.parseInt(line[3]));
						currentFixation.setX((int) Double.parseDouble(line[5]));
						currentFixation.setY((int) Double.parseDouble(line[6]));
						screen.addFixation(currentFixation);
						
					}
				}
				openFixation = false;
				ignoreFixation = false; 
				currentFixation = null;
			}
			
			//blinks
			else if(strLine.contains("SBLINK")){
				
				String[] line = strLine.split(" "); //split by space
				Blink newBlink = new Blink(Integer.parseInt(line[2]));
				
				while (!strLine.contains("EBLINK"))	
					strLine = br.readLine();
				
				line = strLine.split(" "); //split by space
				newBlink.setTimestampEnd(Long.parseLong(line[3]));
				newBlink.setDuration(Integer.parseInt(line[4]));
				
				screen.addBlink(newBlink);
				
			}
			
			// gazes 
			else if(readData  && !strLine.contains("SBLINK") 
					&& !strLine.contains("EBLINK") 
					&& !strLine.contains("SSACC")
					&& !strLine.contains("ESACC")
					&& !strLine.contains("SFIX")
					&& !strLine.contains("EFIX")
					&& !strLine.contains("MSG")){
			
				//new gaze
				String[] line = strLine.split(" "); 
			
				if(!line[1].toString().equalsIgnoreCase(".") &&
				   (int) Double.parseDouble(line[1]) <= newExperiment.getScreenWidth() &&	// is fixation on screen?
				   (int) Double.parseDouble(line[1]) > 0 &&
				   (int) Double.parseDouble(line[2]) <= newExperiment.getScreenHeight() &&
				   (int) Double.parseDouble(line[2]) > 0 ){
					
				
					Gaze newGaze = new Gaze(Integer.parseInt(line[0]),(int) Double.parseDouble(line[1]),
														 (int) Double.parseDouble(line[2]));
					screen.addGaze(newGaze);
					
					if (openSaccade){
						currentSaccade.addGaze(newGaze);
					}	
				}
			}
		}
		
		//Close the input stream
		in.close();
		file.delete();
		br.close();
		fstream.close();
		
		//push last screen/trial
		if (screen != null){
		
			trial.addScreen(screen);
			newExperiment.addTrial(trial);
		}
		
		if (newExperiment.hasDrift(path.replace(".asc", "_drift.txt")))
			newExperiment = driftParser.parseAttributes(path.replace(".asc", "_drift.txt"), newExperiment);
		
		
		return newExperiment;
	}


	@Override
	public Study loadStudy(String path) throws IOException {

		Study study = new Study(path, false);
		File file = new File(path);
		
		ArrayList<AreaOfInterest> aois = null;

		imagePath = file.getAbsolutePath() +  "\\screens\\";

		ArrayList<File> files = new ArrayList<File>(Arrays.asList(file.listFiles()));
//		Arrays.stream(file.listFiles()).parallel()
//									   .filter(f -> f.isDirectory())
//									   .filter(f -> !f.getAbsolutePath().contains("\\screens"))
//									   .forEach(f -> Arrays.stream(f.listFiles(new OEFileFilter()))
//											   		 
//											   		 .forEach(d -> addExperiment(d, study)));

		
		
		int subjectId = 0;
		for (File f : file.listFiles()){
			
			// subject
			if (!f.getAbsolutePath().contains("\\screens") && f.isDirectory()){
				
				//for (File d : f.listFiles(new OEFileFilter())){
					
					study.addSubject(loadSubject(f,imagePath, subjectId));
					//++subjectId;
					
					/*Subject newSubject = new Subject(name, id)
					Experiment newEx = readExperiment(d.getAbsolutePath(), imagePath, false, id);
					study.addExperiment(newEx);
					*/
				//}
				
			}
			// read aois
			/*
			if (f.getAbsolutePath().contains("\\aois.txt")){
				
				AoiReader aoiReader = new AoiReader();
				study = aoiReader.readFile(study, file.getAbsolutePath() + "\\aois.txt");
			}*/
			++subjectId;
		}
		
		//study.setStudyTrials(study.getExperiments().get(0).getTrials());
		
		
		return study;
	}
	
	private Subject loadSubject(File sub, String imagePath, int id){
		
		Subject newSubject = new Subject(sub.getName(), id);
		
		int experimentId = 0;
		for (File s : sub.listFiles()){
			
			if (!s.isDirectory() && s.getName().contains(".asc") && s.getName().contains("w_")){
				
				try {
					newSubject.addExperiment(readExperiment(s.getAbsolutePath(), imagePath, true, experimentId));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				++experimentId;
			}
		}
		
		return newSubject;
	}
	
}
