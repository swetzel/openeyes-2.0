package Input;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;





import Data.AreaOfInterest;
import Data.Study.Study;
import Data.Subject.Experiment;

public abstract class OEParser {

	
	String imagePath;
	
	/*
	public Experiment loadExperiment(String logPath,String imagePath, int id) throws IOException{
		
		//File file = new File(logPath);
		//imagePath = file.getParentFile().getParentFile() +  "\\screens\\";
		
		Experiment experiment = readExperiment(logPath,imagePath, true, id);
		
		return experiment;
	}
	*/
	
	public abstract Study loadStudy(String path) throws IOException ;
		
		
	
	
	/*
	private void addExperiment(File f, Study study, int id) {
		
		Experiment newEx = null;
		try {
			newEx = readExperiment(f.getAbsolutePath(), imagePath, false, id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		study.addExperiment(newEx);
	}
	*/
	
	protected abstract Experiment readExperiment(String path, String imagePath, boolean enable, int id) throws IOException;
	
	/**
	 * Clean the edf file of the EyeLink 2 from tabs and spaces. 
	 * 
	 * @param edf file
	 * @return clean edf file
	 * @throws IOException
	 */
	public File cleanFile(File file) throws IOException{
		
		// in
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		// out
		File newFile = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf('.'))+"_clean.asc");
		PrintWriter writer = new PrintWriter(newFile);
		
		while ((strLine = br.readLine()) != null)   {
			
				writer.println(strLine.replace("\t", " ").replaceAll(" +", " "));
		}
		br.close();
		in.close();
		fstream.close();
		writer.close();
		return newFile;
	}
	
	
}
