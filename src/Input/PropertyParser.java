package Input;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import View.GUI;
import Data.ExperimentFilter.Quality;
import Data.FilteringProperty;
import Data.Subject.DriftCorrection;
import Data.Subject.ELearningExperimentProperties;
import Data.Subject.Experiment;
import Data.Subject.ExperimentProperties.ExperimentType;
import Data.SortingProperty;


public class PropertyParser {

	
	private static PropertyParser instance = new PropertyParser();
	
	private  PropertyParser(){
		
	}
	
	public static PropertyParser getInstance() {
        return instance;
    }
	
	public Experiment parseAttributes(String path, Experiment ex) throws IOException{
		
		File file = new File(path);
		
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
				
		while ((strLine = br.readLine()) != null)   {
			
			if (strLine.contains("Prop")){
				
				String[] split = strLine.split(" ");
				
				if (split[2].contains("Quality")){
					
					ex.setExperimentProperties(new ELearningExperimentProperties(ExperimentType.ELEARNING, 1, Integer.parseInt(split[3])));
				}
				
				/*
				if (split[1].equals("1")) //filter
					ex.addProperty(new FilteringProperty(PropertyType.FILTER,split[2], split[3]));
				else if(split[1].equals("2"))
					ex.addProperty(new SortingProperty(PropertyType.SORTING,split[2], Float.parseFloat(split[3])));
				*/
			}
			
			
			if (!strLine.contains("Trial") && !strLine.contains("Prop")){
							
				String[] split = strLine.split(" ");
				
				int trial = Integer.parseInt(split[0]);
				int screen = Integer.parseInt(split[1]);
				int driftX = Integer.parseInt(split[2]);
				int driftY = Integer.parseInt(split[3]);
				
				DriftCorrection drift = new DriftCorrection(driftX, driftY);
				ex.getTrials().get(trial).getScreens().get(screen).setDriftCorrection(drift);
			}	
		}
		fstream.close();
		in.close();
		br.close();
		System.gc();
		return ex;
	}
	
}
