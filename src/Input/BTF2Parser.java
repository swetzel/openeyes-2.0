package Input;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import Data.Study.Study;
import Data.Subject.BTFExperimentProperties;
import Data.Subject.BTFSubjectProperties;
import Data.Subject.BTFTrialProperties;
import Data.Subject.EventScreen;
import Data.Subject.Experiment;
import Data.Subject.ExperimentProperties.ExperimentType;
import Data.Subject.Screen;
import Data.Subject.Subject;
import Data.Subject.Trial;
import Data.Subject.TrialProperties;
import Events.Custom.PCEvent;
import Events.Default.Blink;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.Saccade;

public class BTF2Parser extends OEParser{

	@Override
	public Study loadStudy(String path) throws IOException {
		
		Study study = new Study(path, false);
		File file = new File(path);
		
		imagePath = file.getAbsolutePath() +  "\\screens\\";
		int subjectId = 0;
		for (File f : file.listFiles()){
			
			// subject
			if (!f.getAbsolutePath().contains("\\screens") && f.isDirectory()){
				
					
				for (File sub : f.listFiles()){
					
					if (!sub.getAbsolutePath().contains(".ini")){
						study.addSubject(loadSubject(sub,imagePath, subjectId));
						++subjectId;
					}
				}	
			}
			
			
		}
		return study;
	}
	
	private Subject loadSubject(File sub, String imagePath, int id){
		
		Subject newSubject = new Subject(sub.getName(), id);
		
		
		if (sub.getAbsolutePath().contains("Gestalter"))
			newSubject.setSubjectProperties(new BTFSubjectProperties("gestalter", 1));
		else 
			newSubject.setSubjectProperties(new BTFSubjectProperties("mintler", 2));
		
		int experimentId = 0;
		
		
		
		
			for (File s : sub.listFiles()){
				
				
				if (!s.isDirectory() && s.getName().contains(".oey")){
					
					try {
						System.out.println(s.getAbsolutePath());	
						newSubject.addExperiment(readExperiment(s.getAbsolutePath(), imagePath, true, experimentId));
						++experimentId;
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}			
			}
		
		return newSubject;
	}
	
	private Experiment readTrialProperties(String path, Experiment ex) throws IOException{
		
		File file = new File(path.replace(".oey", ".csv"));
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		int trialNumber = 0; //24 trials per condition
		
		while ((strLine = br.readLine()) != null){
			
			String [] split = strLine.split(";");
			
			if (!split[0].contains("TrialName") && !split[0].contains("Familiarization") && !split[0].contains("Experiment") &&!strLine.isEmpty()){
			
				ex.getTrials().get(trialNumber).setTrialProperties(new BTFTrialProperties(split[0], split[6], split[7], split[12], Integer.parseInt(split[13]), Boolean.parseBoolean(split[14])));
				++trialNumber;
			}
		}		
		return ex;
	}
	

	@Override
	protected Experiment readExperiment(String path, String imagePath,
			boolean enable, int id) throws IOException {

		//System.out.println(path);
		Experiment newExperiment = new Experiment(path, enable, id, false);
		
			
		Screen screen = null;
		PCEvent newEvent = null;
		Trial trial = null;
		String strLine;
		
		File file = new File(path);
		
		String [] nameSplit = path.split("_");
		newExperiment.setName(path);
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		
		boolean readData = false;
		boolean openFixation = false;
		boolean openSaccade = false;
		Fixation currentFixation = null;
		Saccade currentSaccade = null;
		
		int offsetX = 0;
		int offsetY = 0;
		
		
		
		//read file line by line
		while ((strLine = br.readLine()) != null)   {
					
			// get screen resolution
			if (strLine.contains("SCREEN_COORDS")){
			
				String[] line = strLine.split(" "); //split by space
				newExperiment.setScreenWidth(Integer.parseInt(line[4]));
				newExperiment.setScreenHeight(Integer.parseInt(line[5]));
			}
						
			
			
			
			// trails contain multiple screens
			// new trial
			if (strLine.contains("TRIAL_START")){
				
				String[] line = strLine.split(" "); //split by space
				
								
				trial = new Trial(Long.parseLong(line[0]));
				trial.setName(line[2]);
				
				// always start with blank screen
				screen = new Screen(Long.parseLong(line[0]), imagePath + "blank.jpg");
				readData = true;
			}
			
			//end of trial
			if (strLine.contains("TRIAL_STOP")){
				
				readData = false;
				String[] line = strLine.split(" ");
				
				screen.setTimestampEnd(Long.parseLong(line[0]));
				trial.setEndTime(Long.parseLong(line[0]));
				
				trial.addScreen(screen);
				screen = null;
				newExperiment.addTrial(trial);
				trial = null;
			}
			
			else if(!strLine.contains("#") && strLine.contains("DRIFT_CORRECTION")){
				
				String[] line = strLine.split(" ");
				offsetX = Integer.parseInt(line[6]);
				offsetY = Integer.parseInt(line[7]);
				//System.out.println("offset" + offsetX + " " + offsetY);
			}
			
			// show sofa
			else if (readData && strLine.contains("Showed")){
				
				String[] line = strLine.split(" ");
				
				screen.setTimestampEnd(Long.parseLong(line[0])-1);
				trial.addScreen(screen);	//add last screen
				
				if (line[5].contains("Good")){
					
					screen = new Screen(Long.parseLong(line[0]), imagePath + "Cord-256X256.jpg");
				}
				else if (line[5].contains("Mid")){
					screen = new Screen(Long.parseLong(line[0]), imagePath + "Cord-128X128.jpg");
				}
				else if (line[5].contains("Bad")){
					screen = new Screen(Long.parseLong(line[0]), imagePath + "Cord-64X64.jpg");
				}				
			}
			
			// hide sofa
			else if (readData && strLine.contains("Hid")){
				
				//System.out.println(strLine);
				
				String[] line = strLine.split(" ");
				screen.setTimestampEnd(Long.parseLong(line[0]));
				trial.addScreen(screen);
				
				screen = new Screen(Long.parseLong(line[0]), imagePath + "blank.jpg");
			}
				
			//start saccade
			else if(readData && strLine.contains("SACCADE_START")){
				
				String[] line = strLine.split(" "); //split by space
				currentSaccade = new Saccade(Long.parseLong(line[0]));
				openSaccade = true;
			}
			
			//end saccade
			else if(readData && strLine.contains("SACCADE_END")){
				
				//if(!ignoreSaccade && openSaccade){
				if(openSaccade){
					
					String[] line = strLine.split(" "); //split by space
					
					if(!line[3].toString().equalsIgnoreCase(".") &&
					   !line[4].toString().equalsIgnoreCase(".") &&
					   !line[6].toString().equalsIgnoreCase(".") &&
					   !line[7].toString().equalsIgnoreCase(".") &&
					   Long.parseLong(line[8]) != 0){
						
						currentSaccade.setTimestampEnd(Long.parseLong(line[2]));
						currentSaccade.setX((int) Double.parseDouble(line[3]));
						currentSaccade.setY((int) Double.parseDouble(line[4]));
						currentSaccade.setEndX((int) Double.parseDouble(line[6]));
						currentSaccade.setEndY((int) Double.parseDouble(line[7]));
						
						screen.addSaccade(currentSaccade);
					}
				}
				openSaccade = false;
				currentSaccade = null;
			}
			
			//start fixation
			else if(readData && strLine.contains("FIXATION_START")){
				
				String[] line = strLine.split(" "); //split by space
				currentFixation = new Fixation(Long.parseLong(line[0]));
				openFixation = true;
			}
			
			//end fixation
			else if(readData && strLine.contains("FIXATION_END")){
				
				//if(!ignoreFixation && openFixation){
				if(openFixation){
					
					String[] line = strLine.split(" "); //split by space
					
					
					if(!line[9].toString().equalsIgnoreCase(".") &&
					   !line[10].toString().equalsIgnoreCase(".") &&
					   (int) Double.parseDouble(line[9])+offsetX <= newExperiment.getScreenWidth() && // is fixation on screen?
					   (int) Double.parseDouble(line[9])+offsetX > 0 &&
					   (int) Double.parseDouble(line[10])+offsetY <= newExperiment.getScreenHeight() &&
					   (int) Double.parseDouble(line[10])+offsetY > 0){
						
						currentFixation.setTimestampEnd(Long.parseLong(line[3]));
						currentFixation.setX((int) Double.parseDouble(line[9])+offsetX);
						currentFixation.setY((int) Double.parseDouble(line[10])+offsetY);
						screen.addFixation(currentFixation);
						
					}
				}
				openFixation = false;
				currentFixation = null;
			}
			
						
			// gazes 
			else if(readData && strLine.contains("GAZE")){
			
				//new gaze
				String[] line = strLine.split(" "); 
			
				//System.out.println(newExperiment.getScreenWidth());
				
				if((int) Integer.parseInt(line[2])+offsetX <= newExperiment.getScreenWidth() &&	// is fixation on screen?
				   (int) Integer.parseInt(line[2])+offsetX > 0 &&
				   (int) Integer.parseInt(line[3])+offsetY <= newExperiment.getScreenHeight() &&
				   (int) Integer.parseInt(line[3])+offsetY > 0 ){
					
				//System.out.println("gaze");	
					
				//try {
					Gaze newGaze = new Gaze(Long.parseLong(line[0]),(int) Integer.parseInt(line[2])+offsetX,
							 (int) Integer.parseInt(line[3])+offsetY, (float) Float.parseFloat(line[4]), (float) Float.parseFloat(line[5]));
					screen.addGaze(newGaze);
					if (openSaccade){
						
						currentSaccade.addGaze(newGaze);
					}	
				//} catch (Exception e) {
					//System.out.println("");
				//}
				}
			}
		}
		
		//Close the input stream
		in.close();
		//file.delete();
		
		//push last screen/trial
		if (screen != null){
		
			trial.addScreen(screen);
			newExperiment.addTrial(trial);
		}
		return readTrialProperties(path, newExperiment);
	}

}
