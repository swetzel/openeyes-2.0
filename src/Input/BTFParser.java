package Input;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import Data.Study.Study;
import Data.Subject.Experiment;
import Data.Subject.Screen;
import Data.Subject.Subject;
import Data.Subject.Trial;
import Events.Default.Blink;
import Events.Default.Fixation;
import Events.Default.Gaze;
import Events.Default.Saccade;

/**
 * 
 * @author Stefanie Wetzel <stefanie.wetzel@gmail.com>
 *
 */
public class BTFParser extends OEParser{
	
	/*
	 * Example implementation for the BTF-Experiment with EyeTribe
	 * 
	 * @see InputOutput.OEParser#readExperiment(java.lang.String, java.lang.String, boolean)
	 */
	@Override
	protected Experiment readExperiment(String path, String imagePath,
			boolean enable, int id) throws IOException {


		Experiment newExperiment = new Experiment(path, enable, id, true);
		Screen screen = null;
		Trial trial = null;
		String strLine;
		
		File file = new File(path);
		newExperiment.setName(file.getName());
		
		file = cleanFile(file);
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		
		boolean readData = false;
		boolean openFixation = false;
		boolean openSaccade = false;
		Fixation currentFixation = null;
		Saccade currentSaccade = null;
		
		//read file line by line
		while ((strLine = br.readLine()) != null)   {
					
			// get screen resolution
			if (strLine.contains("DISPLAY_COORDS")){
			
				String[] line = strLine.split(" "); //split by space
				newExperiment.setScreenWidth(Integer.parseInt(line[5])+1);
				newExperiment.setScreenHeight(Integer.parseInt(line[6])+1);
			}
			
			// second version of screen resolution 
			if (strLine.contains("GAZE_COORDS")){
				
				String[] line = strLine.split(" "); //split by space
				newExperiment.setScreenWidth((int)(Float.parseFloat(line[5]))+1);
				newExperiment.setScreenHeight((int)(Float.parseFloat(line[6]))+1);
			}
			
			
			// trails contain multiple screens
			// new trial
			if (strLine.contains("TRIALID")){
				
				String[] line = strLine.split(" "); //split by space
				
				if (trial != null){
					
					trial.addScreen(screen);
					newExperiment.addTrial(trial);
				}
				
				trial = new Trial(Integer.parseInt(line[1]));
				trial.setName(line[3]);
				
				// always start with blank screen
				screen = new Screen(Integer.parseInt(line[1]), imagePath + "blank.png");
			}
			
			//find start of eye data
			else if (strLine.contains("SAMPLES GAZE")){
				readData = true;
				strLine = br.readLine();
			}
			
			//find end of eye data
			else if (readData && (strLine.contains("Stop tracking"))){
				
				readData = false;
				
				String[] line = strLine.split(" ");
				screen.setTimestampEnd(Integer.parseInt(line[1]));
				trial.setEndTime(Integer.parseInt(line[1]));
			}
			
			
			// show two sofas
			/*
			else if (readData && strLine.contains("Showed sofas")){
				System.out.println("sofas");

				trial.addScreen(screen);
				String[] line = strLine.split(" ");
				screen = new Screen(Integer.parseInt(line[1]), imagePath + ".png");
			}
			*/
			// show sofa
			else if (readData && strLine.contains("Showed")){
				
				String[] line = strLine.split(" ");
				trial.addScreen(screen);
				// two sofas
				if (line[4].contains("left")){
					
					screen = new Screen(Integer.parseInt(line[1]), imagePath + line[5].replace("," ,"" ) +"-Left-" +line[7].replace(")", "") +  "-Right.png");
				}
				// one sofa	
				else	{
					screen = new Screen(Integer.parseInt(line[1]), imagePath + line[5].replace("(", "").replace(").", "")+".png");
				}
			}
			
			// hide sofa
			else if (readData && strLine.contains("Hid")){
				
				//System.out.println(strLine);
				trial.addScreen(screen);
				String[] line = strLine.split(" ");
				screen = new Screen(Integer.parseInt(line[1]), imagePath + "blank.png");
			}
				
			//start saccade
			else if(strLine.contains("SSACC")){
				
				String[] line = strLine.split(" "); //split by space
				currentSaccade = new Saccade(Integer.parseInt(line[2]));
				openSaccade = true;
			}
			
			//end saccade
			else if(strLine.contains("ESACC")){
				
				//if(!ignoreSaccade && openSaccade){
				if(openSaccade){
					
					String[] line = strLine.split(" "); //split by space
					
					if(!line[5].toString().equalsIgnoreCase(".") &&
					   !line[6].toString().equalsIgnoreCase(".")&&
					   !line[7].toString().equalsIgnoreCase(".")&&
					   !line[8].toString().equalsIgnoreCase(".")){
						
						currentSaccade.setTimestampEnd(Integer.parseInt(line[3]));
						currentSaccade.setX((int) Double.parseDouble(line[5]));
						currentSaccade.setY((int) Double.parseDouble(line[6]));
						currentSaccade.setEndX((int) Double.parseDouble(line[7]));
						currentSaccade.setEndY((int) Double.parseDouble(line[8]));
						
						screen.addSaccade(currentSaccade);
					}
				}
				openSaccade = false;
				currentSaccade = null;
			}
			
			//start fixation
			else if(strLine.contains("SFIX")){
				
				String[] line = strLine.split(" "); //split by space
				currentFixation = new Fixation(Integer.parseInt(line[2]));
				openFixation = true;
			}
			
			//end fixation
			else if(strLine.contains("EFIX")){
				
				//if(!ignoreFixation && openFixation){
				if(openFixation){
					
					String[] line = strLine.split(" "); //split by space
					
					
					if(!line[5].toString().equalsIgnoreCase(".") &&
					   !line[6].toString().equalsIgnoreCase(".") &&
					   (int) Double.parseDouble(line[5]) <= newExperiment.getScreenWidth() && // is fixation on screen?
					   (int) Double.parseDouble(line[5]) > 0 &&
					   (int) Double.parseDouble(line[6]) <= newExperiment.getScreenHeight() &&
					   (int) Double.parseDouble(line[6]) > 0){
						
						currentFixation.setTimestampEnd(Integer.parseInt(line[3]));
						currentFixation.setX((int) Double.parseDouble(line[5]));
						currentFixation.setY((int) Double.parseDouble(line[6]));
						screen.addFixation(currentFixation);
						
					}
				}
				openFixation = false;
				currentFixation = null;
			}
			
			//blinks
			else if(strLine.contains("SBLINK")){
				
				String[] line = strLine.split(" "); //split by space
				Blink newBlink = new Blink(Integer.parseInt(line[2]));
				
				while (!strLine.contains("EBLINK"))	
					strLine = br.readLine();
				
				line = strLine.split(" "); //split by space
				newBlink.setTimestampEnd(Long.parseLong(line[3]));
				newBlink.setDuration(Integer.parseInt(line[4]));
				
				screen.addBlink(newBlink);
				
			}
			
			// gazes 
			else if(readData  && !strLine.contains("SBLINK") 
					&& !strLine.contains("EBLINK") 
					&& !strLine.contains("SSACC")
					&& !strLine.contains("ESACC")
					&& !strLine.contains("SFIX")
					&& !strLine.contains("EFIX")
					&& !strLine.contains("MSG")
					&& !strLine.contains("END")
					&& !strLine.contains("START")
					&& !strLine.contains("PRESCALER")
					&& !strLine.contains("VPRESCALER")
					&& !strLine.contains("PUPIL")
					&& !strLine.contains("EVENTS")
					&& !strLine.contains("SAMPLES")
					&& !strLine.contains("CALIBRATION)")){
			
				//new gaze
				String[] line = strLine.split(" "); 
			
				//System.out.println(strLine + " " + newExperiment.getName());
				
				if(!line[1].toString().equalsIgnoreCase(".") &&
				   (int) Double.parseDouble(line[1]) <= newExperiment.getScreenWidth() &&	// is fixation on screen?
				   (int) Double.parseDouble(line[1]) > 0 &&
				   (int) Double.parseDouble(line[2]) <= newExperiment.getScreenHeight() &&
				   (int) Double.parseDouble(line[2]) > 0 ){
					
				try {
					Gaze newGaze = new Gaze(Integer.parseInt(line[0]),(int) Double.parseDouble(line[1]),
							 (int) Double.parseDouble(line[2]));
					screen.addGaze(newGaze);
					if (openSaccade){
						
						currentSaccade.addGaze(newGaze);
					}	
				} catch (Exception e) {
					System.out.println("");
				}
				}
			}
		}
		
		//Close the input stream
		in.close();
		file.delete();
		
		//push last screen/trial
		if (screen != null){
		
			trial.addScreen(screen);
			newExperiment.addTrial(trial);
		}
		return newExperiment;
	}

	@Override
	public Study loadStudy(String path) throws IOException {
		
		Study study = new Study(path, false);
		File file = new File(path);
		
		imagePath = file.getAbsolutePath() +  "\\screens\\";
		int subjectId = 0;
		for (File f : file.listFiles()){
			
			// subject
			if (!f.getAbsolutePath().contains("\\screens") && f.isDirectory()){
				
					
					study.addSubject(loadSubject(f,imagePath, subjectId));
					
			}
			
			++subjectId;
		}
		return study;
	}

	private Subject loadSubject(File sub, String imagePath, int id){
		
		Subject newSubject = new Subject(sub.getName(), id);
		
		int experimentId = 0;
		for (File s : sub.listFiles()){
			
			if (!s.isDirectory() && s.getName().contains(".asc")){
				
				try {
					newSubject.addExperiment(readExperiment(s.getAbsolutePath(), imagePath, true, experimentId));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				++experimentId;
			}
		}
		
		return newSubject;
	}
	
}
