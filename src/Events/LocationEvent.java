package Events;

import java.awt.Graphics2D;

public abstract class LocationEvent extends Event{

	protected int x;
	protected int y;
	protected int x_original; // original values from log file
	protected int y_original;
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
		x_original = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
		y_original = y;
	}
	
	public void drift(int driftX, int driftY){
		
		x += driftX;
		y += driftY;
	}
	
	public void restorePosition(){
		
		x = x_original;
		y = y_original;
	}

	public void select(){
		selected = true;
	}
	
	public void deselect(){
		selected = false;
	}
	
	
	abstract public void draw(Graphics2D g2d, double scaleX, double scaleY);
	
	
}
