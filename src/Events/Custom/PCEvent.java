package Events.Custom;

import java.awt.Color;

import Events.Event;

public class PCEvent extends Event{

	private EventType eventType;
	private Color color;
	
	public enum EventType{
		
		ActivateZoom,
		ZoomIn,
		ZoomOut,
		ActivateBrush,
		Brush,
		RemovedBrush,
		Reset,
		Default
	}
	
	public PCEvent(long timeStamp, String type){
		
		
		if (type.contains("Activate_Zoom_Mode")){
			eventType = EventType.ActivateZoom;
			color = Color.magenta;

		}
		else if (type.contains("Zoom_in")){
			eventType = EventType.ZoomIn;
			color = Color.BLUE;
		}
		else if (type.contains("Zoom_out")){
			
			eventType = EventType.ZoomOut;
			color = Color.green;
		}
		else if (type.contains("Activate_Brush_Mode")){
			
			eventType = EventType.ActivateBrush;
		}
		else if (type.contains("Brush_data")){
			
			eventType = EventType.Brush;
			color = Color.RED;
		}
		else if (type.contains("Removed_brush")){
			
			eventType = EventType.RemovedBrush;
			color = Color.cyan;
		}
		else if (type.contains("Reset")){
			
			eventType = EventType.Reset;
			color = Color.cyan;
		}
		else {
			
			eventType = EventType.Default;
			color = Color.black;
		}
				
		
		
	}
	
	public EventType getEventType(){
		return eventType;
	}
	
	public Color getColor(){
		return color;
	}
}
