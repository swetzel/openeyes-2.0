package Events;

import java.awt.Color;

import Events.Default.Gaze;

public abstract class Event implements Comparable<Gaze> {
	
	protected long timestampStart;
	protected long timestampEnd;
	protected int duration;	// in ms
 	protected boolean selected = false;
 	protected Color color; 
	
	public void setTimestampStart(long t) {
		timestampStart = t;
	}
	
	public long getTimestampStart() {
		return timestampStart;
	}
	
	public void setTimestampEnd(long t) {
		timestampEnd = t;
		duration = (int) Math.abs(timestampEnd - timestampStart);
	}
	
	public long getTimestampEnd() {
		return timestampEnd;
	}
	
	public Color getColor(){
		return color;
	}
	
	public void setColor(Color c){
		color = c;
	}
	
	public int getDuration() {
		return duration;
	}

	public void setDuration(int d) {
		duration = d;
	}
	
	/**
	 * Compare Timestamps.
	 */
	@Override
	public int compareTo(Gaze gaze) {
		
		if (this.getTimestampStart() > gaze.getTimestampStart()) 
		    return 1;
		if (this.getTimestampStart() < gaze.getTimestampStart()) 
		    return -1;
		else 
			return 0;
	}
	
}
