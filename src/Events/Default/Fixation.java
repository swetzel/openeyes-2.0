package Events.Default;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Deque;
import java.util.Queue;

import Events.LocationEvent;


/**
 * Fixations have start and end timestamps and a duration.
 * 
 * @author Stefanie Wetzel
 *
 */
public class Fixation extends LocationEvent {

		
	public Fixation(int time){
		this.timestampStart = time;
		color =  new Color(255,0,0,200);
	}
	
	public Fixation(long time){
		this.timestampStart = time;
		color =  new Color(255,0,0,200);
	}
	
	public Fixation (Deque<Gaze> gazes){
		
		setTimestampStart(gazes.peekFirst().getTimestampStart());
		setTimestampEnd(gazes.peekLast().getTimestampEnd());
		color =  new Color(255,0,0,200);
		
		float xFix = 0;
		float yFix = 0;
		for (Gaze g : gazes){
			xFix += g.getX();
			yFix += g.getY();
		}
		
		xFix = xFix/gazes.size();
		yFix = yFix/gazes.size();
		setX((int)xFix);
		setY((int)yFix);
		
		//System.out.println(getDuration() + " " + getTimestampStart() + " " + getTimestampEnd());
	}
	
	@Override
	public void draw(Graphics2D g2d, double scaleX, double scaleY){
		
		if(selected)
			g2d.setColor(new Color(0, 0, 255));
		else
			g2d.setColor(color);
		
		g2d.fillOval((int) ((x - (3 + duration * 0.05) / 2) *scaleX),
					 (int) ((y - (3 + duration * 0.05) /2)*scaleY),
					 (int) ((3 + duration * 0.05)*scaleX),
					 (int) ((3 + duration * 0.05)*scaleY));
	}
	

}
