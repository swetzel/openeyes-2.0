package Events.Default;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

import Events.LocationEvent;


/**
 * Saccades have start and end Timestamps and a duration.
 * 
 * @author Stefanie Wetzel
 *
 */
public class Saccade extends LocationEvent{

	private int endX;
	private int endY;
	private int endXOriginal;
	private int endYOriginal;

	private ArrayList<Gaze> gazes = new ArrayList<Gaze>();
	
	 
	public void addGaze(Gaze g){
		gazes.add(g);
	}
	
	public double getDistance(){
		
		return Math.sqrt(Math.pow((endX - x), 2) + Math.pow((endY - y), 2));
	}
	
	public Gaze getGaze(int i){
		return gazes.get(i);
	}
	
	public int getEndX() {
		return endX;
	}

	public void setEndX(int endX) {
		this.endX = endX;
		this.endXOriginal = endX;
	}

	public int getEndY() {
		return endY;
	}

	public void setEndY(int endY) {
		this.endY = endY;
		this.endYOriginal = endY;
	}
	
	public Saccade(long time){
		timestampStart = time;
		color = Color.BLACK;
	}

	public Saccade(Fixation start, Fixation end){
		
		this.timestampStart = start.getTimestampStart();
		this.timestampEnd = end.getTimestampEnd();
		this.x = start.getX();
		this.y = start.getY();
		this.endX = end.getX();
		this.endY = end.getY();
		color = Color.BLACK;
	}
	
	
	public Saccade(Gaze start, Gaze end){
		
		this.timestampStart = start.getTimestampStart();
		this.timestampEnd = end.getTimestampEnd();
		this.x = start.getX();
		this.y = start.getY();
		this.endX = end.getX();
		this.endY = end.getY();
		color = Color.BLACK;
	}
	
	@Override
	public void draw(Graphics2D g2d, double scaleX, double scaleY){
		
		if(selected)
			g2d.setColor(new Color(0, 0, 255));
		else
			g2d.setColor(color);
		
		g2d.drawLine( (int) (x *scaleX),(int)(y *scaleY), 
				  (int) (endX *scaleX), (int) (endY*scaleY));
	
	}
	
	public void drift(int driftX, int driftY){
		
		x += driftX;
		y += driftY;
		endX += driftX;
		endY += driftY;
	}
	
	public void restorePosition(){
		
		x = x_original;
		y = y_original;
		endX = endXOriginal;
		endY = endYOriginal;
	}
	
}
