package Events.Default;

import java.awt.Color;
import java.awt.Graphics2D;

import Events.Event;
import Events.LocationEvent;

public class MousePosition extends LocationEvent {

	public MousePosition(long timeStamp, int x, int y){
		
		this.timestampStart = timeStamp;
		color = Color.green;
		this.x = x;
		this.y = y;
		
	}
	
	
	

	@Override
	public void draw(Graphics2D g2d, double scaleX, double scaleY) {
		// TODO Auto-generated method stub
		
		g2d.fillOval((int) ((x ) *scaleX),
				 (int) ((y)*scaleY),
				 (int) ((5 )*scaleX),
				 (int) ((5 )*scaleY));
	}
}
