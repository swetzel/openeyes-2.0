package Events.Default;

import java.awt.Color;
import java.awt.Graphics2D;

import Data.Point2D;
import Events.Event;
import Events.LocationEvent;



/**
 * 
 * Base class for eye movements. It implements the Comparable Interface to make 
 * Gaze comparable based on their timestamp.
 * 
 * @author Stefanie Wetzel
 *
 */
public class Gaze extends LocationEvent 
{

 	protected float pupilSizeRight = 0;
 	protected float pupilSizeLeft = 0;
 	protected int xSmooth;
 	protected int ySmooth;
	
	//for cluster analysis
	public int clusterNumber = -1;
	public int clusterID = -1;
	public Visited visited = Visited.UNCLASSIFIED ;
	
	public enum Visited{
		
		UNCLASSIFIED,
		NOISE,
		VISITED,
	}
	
	public Gaze(){}
	
	
	public Gaze(long time, int x, int y){
		
		timestampStart = time;
		this.x = x;
		this.y = y;
		x_original = x;
		y_original = y;
		duration = 4;
		timestampEnd = timestampStart + duration;
		color = new Color(139,0,0);
	}
	
	public Gaze(long time, int x, int y, float leftPupil, float rightPupil){
		
		timestampStart = time;
		this.x = x;
		this.y = y;
		x_original = x;
		y_original = y;
		pupilSizeLeft = leftPupil;
		pupilSizeRight = rightPupil;
		duration = 30;
		timestampEnd = timestampStart + duration;
		color = new Color(0,255,0);
	}

	public Gaze(long time, int x, int y, int xSmooth, int ySmooth, float pupilLeft, float pupilRight){
		
		timestampStart = time;
		this.x = x;
		this.y = y;
		this.xSmooth = xSmooth;
		this.ySmooth = ySmooth;
		x_original = x;
		y_original = y;
		duration = 30;
		timestampEnd = timestampStart + duration;
		pupilSizeLeft = pupilLeft;
		pupilSizeRight = pupilRight;
		color = new Color(139,0,0);
	}
	
	
	
	public void print(){	
		System.out.println("Gaze x: " + this.getX() + " y: " + this.getY());
	}
	
	@Override
	public void draw(Graphics2D g2d, double scaleX, double scaleY){
		
		if(selected)
			g2d.setColor(new Color(0,0,255));
			
		else
			g2d.setColor(color);
		
		g2d.fillRect((int) (x *scaleX), (int) (y *scaleY), 2, 2);
	}
	
	public void drawInVideo(Graphics2D g2d, double scaleX, double scaleY){
		
		//Color c = new Color(255, 0, 0, 255);
		g2d.setColor(color);
		g2d.fillOval((int) ((x - 5) * scaleX), 
					 (int) ((y - 5) * scaleY), 
					 20, 20);//((int) ( x * scaleX), (int) (y * scaleY), 10, 10);	
	}
	
	public float getDistance(Gaze g){
		
		return new Point2D(this.x, this.y).getDistance(new Point2D(g.getX(), g.getY())); 
	}
	
	public boolean isValid(int width, int height){
		
		return this.x > 0 && this.y > 0 && this.x <= width && this.y <= height;
		
	}
	

	public boolean isInRange(float min, float max){
		
		return (this.timestampStart >= min && this.timestampEnd >= min && this.timestampStart <= max  && this.timestampEnd <= max);
	}
	
	public int getClusterNumber() {
		return clusterNumber;
	}
	
	public void setClusterNumber(int clusterNumber) {
		this.clusterNumber = clusterNumber;		
	}
	
	public float getPupilSizeLeft(){
		return pupilSizeLeft;
	}
	
	public float getPupilSizeRight(){
		return pupilSizeRight;
	}
	
	public int getDuration(){
		return duration;
	}
}
