package Events.Default;

import Events.Event;

public class Blink extends Event{

	
	public Blink(long t){
		timestampStart = t;
	}

	public void setEnd(long t){
		timestampEnd = t;
		duration = (int) (timestampEnd - timestampStart);
	}
	
}
