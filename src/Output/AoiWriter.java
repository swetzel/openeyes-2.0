package Output;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import Data.AreaOfInterest;
import Data.Point2D;
import View.GUI;

public class AoiWriter {

	public void writeFile(String path) throws IOException{
		
		GUI gui = GUI.getInstance();
				
		FileWriter fstream = new FileWriter(path+"\\aois.txt", false);
		BufferedWriter out = new BufferedWriter(fstream);
		
	
		/*
		for (int trial = 0; trial < gui.study.getExperiments().get(0).getTrials().size(); ++trial){
	
			
			for (int screen = 0; screen < gui.study.getExperiments().get(0).getTrials().get(trial).getScreens().size(); screen++)
				
				if (!gui.study.getExperiments().get(0).getTrials().get(trial).getScreens().get(screen).getAois().isEmpty()){
					
					out.write("Trial " + trial +" Screen " + screen + " Aois " + gui.study.getExperiments().get(0).getTrials().get(trial).getScreens().get(screen).getAois().size());
					out.newLine();
					
					
					for (AreaOfInterest aoi : gui.study.getExperiments().get(0).getTrials().get(trial).getScreens().get(screen).getAois()){
						
						out.write(aoi.getName() + " " + aoi.getShape().toString().toLowerCase() + " " );
						for (Point2D p : aoi.getPoints())
							out.write(p.getX() +" " + p.getY() + " ");
						out.newLine();
					}
				}
		}
		*/
		//Close the output stream
		out.close();
	}
	
}
