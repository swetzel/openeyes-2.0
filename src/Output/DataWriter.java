package Output;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import View.Menu.Menu;
import Analysis.Statistics;
import Data.Study.Study;
import Data.Subject.ELearningExperimentProperties;
import Data.Subject.Experiment;
import Data.Subject.Subject;
import Events.Custom.PCEvent.EventType;

/**
 * 
 * @author Stefanie Wetzel <stefanie.wetzel@gmail.com>
 *
 * 
 */
public class DataWriter {

	Statistics stats;
	private static DataWriter instance = new DataWriter();
	
	// pc stuff
	private static final int DIAGRAM = 0;
	private static final int OVERVIEW = 1;
	private static final int TOOLBOX = 2;
	private static final int TASK = 3;
	private static final int ANSWER = 4;
	private static final int BUTTON = 5;
	private static final int PLOT = 6;
	
	private DataWriter(){
		
		//stats = new Statistics();
	}
	
	public static DataWriter getInstance() {
        return instance;
    }
	
	/**
	 * Transforms float to String and replaces "." with "," to make it excel compatible.  
	 * 
	 * @param float 
	 * @return string variant of the float
	 */
	private String fToS(float f){
		
		return String.valueOf(f).replace(".", ",");
	}
	
	private String fToS(double d){
		
		return String.valueOf(d).replace(".", ",");
	}
	
	public void writeData(Study study) throws FileNotFoundException, UnsupportedEncodingException{
	
		System.out.println("exporting file");
		
		File f = new File(study.getPath() + "\\eLearning.csv");
		
		if(f.exists()){
			f.delete();
		}
		

		Statistics stats = Statistics.getInstance();		
		PrintWriter writer = new PrintWriter(study.getPath() + "\\eLearning.csv", "UTF-8");
		
		
		// eLearning 
		
		writer.println("Subject;Condition;Quality;OverallTime;TimeScreen1;TimeScreen2;TimeScreen3;TimeScreen4;TimeScreen5;TimeScreen6;TimeScreen7;");
		
		for (Subject s : study.getSubjects()){
						
			
			ELearningExperimentProperties props = (ELearningExperimentProperties) s.getExperiments().get(0).getExperimentProperties();
			
			writer.println(s.getExperiments().get(0).getName() + ";" + props.getCondition() + ";" + props.getQuality() +";" + fToS(stats.getExperimentDuration(s.getExperiments().get(0)))+ ";" +
					fToS(stats.getScreenDuration(s.getExperiments().get(0), 0)) + ";"+ fToS(stats.getScreenDuration(s.getExperiments().get(0), 1)) + ";" + fToS(stats.getScreenDuration(s.getExperiments().get(0), 2)) + ";" + fToS(stats.getScreenDuration(s.getExperiments().get(0), 3)) + ";" + fToS(stats.getScreenDuration(s.getExperiments().get(0), 4)) + ";" + fToS(stats.getScreenDuration(s.getExperiments().get(0), 5)) + ";" + fToS(stats.getScreenDuration(s.getExperiments().get(0), 6)) + ";" );
		}
		
		
		// between
			
		/*
		writer.println("Subject;Group;CorrectAnswers_short;CorrectAnswers_medium;CorrectAnswers_long;DecisionTime_short;DecisionTime_medium;DecisionTime_long;FixRate_short;FixRate_medium;FixRate_long");
		
		for (Subject s : study.getSubjects()){
		
			writer.println(s.getName() + ";" + s.getSubjectProperties().getName() + ";" + stats.getRightAnswersPerTrial(s, "short") + ";" + stats.getRightAnswersPerTrial(s, "medium")+ ";" + stats.getRightAnswersPerTrial(s, "long")+
										 ";" + fToS(stats.getDecisionTimePerTrial(s, "short")) +  ";" + fToS(stats.getDecisionTimePerTrial(s, "medium")) + ";" + fToS(stats.getDecisionTimePerTrial(s, "long")) + 
										 //";" + stats.getAbsoluteFixationNumber(s, "short") + ";" + stats.getAbsoluteFixationNumber(s, "medium") + ";" + stats.getAbsoluteFixationNumber(s, "long") +
										 ";" + fToS(stats.getFixationRatePerTrial(s, "short")) + ";" + fToS(stats.getFixationRatePerTrial(s, "medium")) + ";" + fToS(stats.getFixationRatePerTrial(s, "long")));
		}
			*/	
		
		
		
		// between
		
		/*
		writer.println("subject;condition;task;pupil_size_ralative;");
		
		for (Subject s : study.getSubjects()){
			
			for (Experiment ex: s.getExperiments()){
				
				writer.println(s.getName() + ";" + ex.getCondition() + ";" + ex.getPCTask() + ";" + fToS(ex.getExperimentDuration()/1000f) + ";" 
				+ fToS(stats.getBlinkRate(ex)) + ";" + fToS(ex.getPupilSizes().getMean()) + ";" + fToS(ex.getPupilSizes().getVariance()) + ";" 
				+ fToS(ex.getPupilSizes().getStandardDeviation()) + ";" + fToS(stats.getFixationRate(ex)) + ";" + fToS(stats.getMeanFixationDuration(ex)));
				
				writer.println(s.getName() + ";" + ex.getCondition() + ";" + ex.getPCTask() + ";" + stats.getNumberOfEvents(ex, EventType.Brush)
				+ ";" + stats.getNumberOfEvents(ex, EventType.RemovedBrush)+";" + stats.getNumberOfEvents(ex, EventType.ZoomIn)+";" + stats.getNumberOfEvents(ex, EventType.ZoomOut)+";" 
				+ stats.getNumberOfEvents(ex, EventType.Reset));
				
				writer.println(s.getName() + ";" + ex.getCondition() + ";" + ex.getPCTask() + ";" + fToS(stats.getFixationRateInAoi(ex, PLOT)) + ";" 
						 	   + fToS(stats.getFixationRateInAoi(ex, TOOLBOX)) + ";" + fToS(stats.getFixationRateInAoi(ex, TASK)) );
				
				writer.println(s.getName() + ";" + ex.getCondition() + ";" + ex.getPCTask() + ";" + fToS(stats.getTransitionRate(ex)) + ";" 
					 	   + fToS(stats.getTransitionRateInAoi(ex, PLOT,TASK)) + ";" + fToS(stats.getTransitionRateInAoi(ex, TASK, TOOLBOX)) + ";" + fToS(stats.getTransitionRateInAoi(ex, TOOLBOX, PLOT)));
				
				writer.println(s.getName() + ";" + ex.getCondition() + ";" + ex.getPCTask() + ";" + fToS(stats.getEventRate(ex)) + ";" + fToS(stats.getEventRate(ex, EventType.Brush))
						+ ";" +  fToS(stats.getEventRate(ex, EventType.RemovedBrush))+";" +  fToS(stats.getEventRate(ex, EventType.ZoomIn))+";" +  fToS(stats.getEventRate(ex, EventType.ZoomOut))+";" 
						+  fToS(stats.getEventRate(ex, EventType.Reset)));
				
				writer.println(s.getName() + ";" + ex.getCondition() + ";" + ex.getPCTask() + ";"+ fToS(stats.getMeanPupilSizeRelative(s,ex)));
			}
		}
		*/
		
		// within
		/*
		writer.println("Subject;PupilMean_T1;PupilMean_T2;PupilMean_T3;PupilMean_T4;PupilMean_T5;PupilMean_T6;PupilMean_T7;PupilMean_T8;");
		
		for (Subject s : study.getSubjects()){
		
			writer.println(s.getName() +";" + fToS(stats.getTaskPupilMean(s, 1)) + ";"+ fToS(stats.getTaskPupilMean(s, 2)) + ";"+ fToS(stats.getTaskPupilMean(s, 3)) + ";"+ fToS(stats.getTaskPupilMean(s, 4)) + ";"+ fToS(stats.getTaskPupilMean(s, 5)) + ";"+ fToS(stats.getTaskPupilMean(s, 6)) + ";"+ fToS(stats.getTaskPupilMean(s, 7)) + ";"+ fToS(stats.getTaskPupilMean(s, 8)) + ";");
		}
		*/
		
		/*
		writer.println("Subject;Condition;Task;Duration;FixationNumberTotal;FixationRateDiagram;FixationRateOverview;FixationRateToolbox;FixationRateTask;FixationRateAnswer;FixationRateButton;"
					 + "TransDiagram_Toolbox;TransDiagram_Overview;TransDiagram_Task;TransDiagram_Answer;TransDiagram_Button;TransToolbox_Overview;TransToolbox_Task;TransToolbox_Answer;"
					 + "TransToolbox_Button;TransOverview_Task;TransOverview_Answer;TransOverview_Button;TransTask_Answer;TransTask_Button;");
		
		for (Subject s : study.getSubjects()){
			
			for (Experiment ex: s.getExperiments()){
				
				writer.println(s.getName() + ";" + ex.getCondition() + ";" + ex.getPCTask() + ";" + fToS(ex.getExperimentDuration()) + ";" + 
							   stats.getNumberOfFixations(ex) + ";" + fToS(stats.getFixationRate(ex, DIAGRAM)) + ";" + fToS(stats.getFixationRate(ex, OVERVIEW))
							   + ";" + fToS(stats.getFixationRate(ex, TOOLBOX))  + ";" + fToS(stats.getFixationRate(ex, TASK)) + ";" + fToS(stats.getFixationRate(ex, ANSWER))  + ";" + fToS(stats.getFixationRate(ex, BUTTON))
							   + ";" + stats.getTransitionsBetweenAOIs(ex, DIAGRAM, TOOLBOX) + ";" + stats.getTransitionsBetweenAOIs(ex, DIAGRAM, OVERVIEW) + ";" + stats.getTransitionsBetweenAOIs(ex, DIAGRAM, TASK)
							   + ";" + stats.getTransitionsBetweenAOIs(ex, DIAGRAM, ANSWER) + ";" + stats.getTransitionsBetweenAOIs(ex, DIAGRAM, BUTTON) 
							   + ";" + stats.getTransitionsBetweenAOIs(ex, TOOLBOX, OVERVIEW) + ";" + stats.getTransitionsBetweenAOIs(ex, TOOLBOX, OVERVIEW) +";"+stats.getTransitionsBetweenAOIs(ex, TOOLBOX, ANSWER)
							   + ";" + stats.getTransitionsBetweenAOIs(ex, TOOLBOX, BUTTON) + ";" + stats.getTransitionsBetweenAOIs(ex, OVERVIEW, TASK) + ";" + stats.getTransitionsBetweenAOIs(ex, OVERVIEW, ANSWER)
							   + ";" + stats.getTransitionsBetweenAOIs(ex, OVERVIEW, BUTTON)+ ";" + stats.getTransitionsBetweenAOIs(ex, TASK, ANSWER) + ";" +stats.getTransitionsBetweenAOIs(ex, TASK, BUTTON));
				
			}
		}
		*/
		
		writer.close();
	}
	
//	public void writeData(Study study, String settings) throws FileNotFoundException, UnsupportedEncodingException{
//				
//		File f = new File(study.getPath() + "\\data_"+settings.toLowerCase()+".csv");
//		
//		System.out.println(f.getAbsolutePath());
//		
//		if(f.exists())
//			f.delete();
//			
//		PrintWriter writer = new PrintWriter(study.getPath() + "\\data_" + settings.toLowerCase()+".csv", "UTF-8");
//		
//		if (settings.equals("Fixations_per_subject_main_aois")){
//		
//			writer.println("Versuchsperson;Setup;Dauer der Lernphase;Fixationsdauer;Fixationsanzahl;Fixationszeit auf Bildern;Fixationszeit auf Text;Fixationsanzahl auf Bildern;Fixationsanzahl auf Text;Transitionen zwischen Bild und Text");
//		
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getExperimentDuration(ex)).replace(".", ",") + 
//							";" +String.valueOf(stats.getFixationTime(ex)).replace(".", ",")  + ";" + String.valueOf(stats.getAverageFixationNumber(ex)).replace(".", ",") +
//							";" + String.valueOf(stats.getFixationTimePerAoi(ex,1)).replace(".", ",") +
//							";" + String.valueOf(stats.getFixationTimePerAoi(ex,0)).replace(".", ",") +
//							";" + String.valueOf(stats.getNumberOfFixationsPerAoi(ex, 1)).replace(".", ",") + ";" + String.valueOf(stats.getNumberOfFixationsPerAoi(ex, 0)).replace(".", ",") +
//							";" + String.valueOf(stats.getTransitions(ex,0,1)).replace(".", ","));
//				}
//			}
//			
//		}
//		
//		// transitions between all aois
//		/*
//		if (settings.equals("Transitions_per_subject_all_aois")){
//			
//			writer.println("Versuchsperson;Setup;Transitionen zwischen Bild und Text");
//		
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					//writer.println(ex.getName() + ";" + "1" + ";" + String.valueOf(stats.getTransitions(ex)).replace(".", ","));
//				}
//			}
//		}
//		*/
//		if (settings.equals("Time_per_screen")){
//			
//			writer.println("Testperson;Setup;Zeit auf Screen2;Zeit auf Screen3;Zeit auf Screen4;Zeit auf Screen5;Zeit auf Screen6;Zeit auf Screen7");
//		
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnScreen(1, ex)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnScreen(2, ex)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnScreen(3, ex)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnScreen(4, ex)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnScreen(5, ex)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnScreen(6, ex)).replace(".", ","));
//				}
//			}
//		}
//
//		// hirn
//		if (settings.equals("Screen2")){
//			/*
//			writer.println("Testperson;Setup;2_Dauer_aoi0;2_Fixdauer_aoi0;2_Fixanzahl_aoi0;2_Dauer_aoi1;2_Fixdauer_aoi1;2_Fixanzahl_aoi1;2_Dauer_aoi2;2_Fixdauer_aoi2;2_Fixanzahl_aoi2;2_Dauer_aoi3;2_Fixdauer_aoi3;2_Fixanzahl_aoi3;2_aoi0_1;2_aoi0_2;2_aoi0_3;2_aoi2_3");
//		
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(1, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(1, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(1, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(1, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(1, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(1, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(1, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(1, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(1, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(1, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(1, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(1, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(1, ex,0,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(1, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(1, ex,0,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(1, ex,2,3)).replace(".", ","));
//				}
//			} 
//			*/
//			// wetter 
//			
//			writer.println("Testperson;Setup;2_Dauer_aoi0;2_Fixdauer_aoi0;2_Fixanzahl_aoi0;2_Dauer_aoi1;2_Fixdauer_aoi1;2_Fixanzahl_aoi1;2_aoi0_1");
//
//			for (Experiment ex : study.getExperiments()){
//			
//				if (ex.isEnabled()){
//			
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(1, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(1, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(1, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(1, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(1, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(1, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(1, ex,0,1)).replace(".", ","));
//				}
//			}
//			
//		}
//		
//		// hirn + wetter
//		if (settings.equals("Screen3")){
//			
//			writer.println("Testperson;Setup;3_Dauer_aoi0;3_Fixdauer_aoi0;3_Fixanzahl_aoi0;3_Dauer_aoi1;3_Fixdauer_aoi1;3_Fixanzahl_aoi1;3_Dauer_aoi2;3_Fixdauer_aoi2;3_Fixanzahl_aoi2;3_Dauer_aoi3;3_Fixdauer_aoi3;3_Fixanzahl_aoi3;3_aoi0_1;3_aoi0_2;3_aoi0_3;3_aoi2_3");
//		
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(2, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(2, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(2, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(2, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(2, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(2, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(2, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(2, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(2, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(2, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(2, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(2, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(2, ex,0,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(2, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(2, ex,0,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(2, ex,2,3)).replace(".", ","));
//				}
//			} 
//		}
//		
//		// hirn
//		if (settings.equals("Screen4")){
//			/*
//			writer.println("Testperson;Setup;4_Dauer_aoi0;4_Fixdauer_aoi0;4_Fixanzahl_aoi0;4_Dauer_aoi1;4_Fixdauer_aoi1;4_Fixanzahl_aoi1;4_Dauer_aoi2;4_Fixdauer_aoi2;4_Fixanzahl_aoi2;4_Dauer_aoi3;4_Fixdauer_aoi3;4_Fixanzahl_aoi3;4_aoi0_1;4_aoi0_2;4_aoi0_3;4_aoi2_3");
//		
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(3, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(3, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(3, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(3, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(3, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(3, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(3, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(3, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(3, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(3, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(3, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(3, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex,0,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex,0,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex,2,3)).replace(".", ","));
//				}
//			} 
//			*/
//			// wetter 
//			
//			writer.println("Testperson;Setup;4_Dauer_aoi0;4_Fixdauer_aoi0;4_Fixanzahl_aoi0;4_Dauer_aoi1;4_Fixdauer_aoi1;4_Fixanzahl_aoi1;4_aoi0_1");
//
//			for (Experiment ex : study.getExperiments()){
//			
//				if (ex.isEnabled()){
//			
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(3, ex,0)).replace(".", ",")+ ";"
//															 	 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(3, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(3, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(3, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(3, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(3, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex,0,1)).replace(".", ","));
//				}
//			}
//			
//		}
//		
//		
//		if (settings.equals("Screen5")){
//			/*
//			// hirn
//			writer.println("Testperson;Setup;5_Dauer_aoi0;5_Fixdauer_aoi0;5_Fixanzahl_aoi0");
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(4, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(4, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(4, ex,0)).replace(".", ","));
//				}
//			}
//			*/
//			//wetter
//			
//			writer.println("Testperson;Setup;5_Dauer_aoi0;5_Fixdauer_aoi0;5_Fixanzahl_aoi0;5_Dauer_aoi1;5_Fixdauer_aoi1;5_Fixanzahl_aoi1;5_Dauer_aoi2;5_Fixdauer_aoi2;5_Fixanzahl_aoi2;5_Dauer_aoi3;5_Fixdauer_aoi3;5_Fixanzahl_aoi3;5_aoi0_1;5_aoi0_2;5_aoi0_3;5_aoi2_3");
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(4, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(4, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(4, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(4, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(4, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(4, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(4, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(4, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(4, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(4, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(4, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(4, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(4, ex,0,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(4, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(4, ex,0,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(4, ex,2,3)).replace(".", ","));
//				}
//			} 
//			
//		}
//		
//		if (settings.equals("Screen6")){
//			/*
//			// hirn
//			writer.println("Testperson;Setup;6_Dauer_aoi0;6_Fixdauer_aoi0;6_Fixanzahl_aoi0;6_Dauer_aoi1;6_Fixdauer_aoi1;6_Fixanzahl_aoi1;6_Dauer_aoi2;6_Fixdauer_aoi2;6_Fixanzahl_aoi2;6_Dauer_aoi4;6_Fixdauer_aoi4;6_Fixanzahl_aoi4;6_aoi0_1;6_aoi0_2;6_aoi0_4;6_aoi2_4");
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(5, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(5, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(5, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(5, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(5, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(5, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(5, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(5, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(5, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(5, ex,4)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(5, ex,4)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(5, ex,4)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(5, ex,0,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(5, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(5, ex,0,4)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(5, ex,2,4)).replace(".", ","));
//				}
//			} 
//			*/
//			// wetter 
//			
//			writer.println("Testperson;Setup;6_Dauer_aoi0;6_Fixdauer_aoi0;6_Fixanzahl_aoi0;6_Dauer_aoi1;6_Fixdauer_aoi1;6_Fixanzahl_aoi1;6_aoi0_1");
//
//			for (Experiment ex : study.getExperiments()){
//			
//				if (ex.isEnabled()){
//			
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(5, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(5, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(5, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(5, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(5, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(5, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(5, ex,0,1)).replace(".", ","));
//				}
//			}
//			
//		}
//		if (settings.equals("Screen7")){
//			
//			/*
//			// hirn
//			writer.println("Testperson;Setup;7_Dauer_aoi0;7_Fixdauer_aoi0;7_Fixanzahl_aoi0");
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(6, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(6, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(6, ex,0)).replace(".", ","));
//				}
//			}
//			*/
//			// wetter 
//			
//			writer.println("Testperson;Setup;7_Dauer_aoi0;7_Fixdauer_aoi0;7_Fixanzahl_aoi0;7_Dauer_aoi1;7_Fixdauer_aoi1;7_Fixanzahl_aoi1;7_Dauer_aoi2;7_Fixdauer_aoi2;7_Fixanzahl_aoi2;7_Dauer_aoi3;7_Fixdauer_aoi3;7_Fixanzahl_aoi3;7_aoi0_1;7_aoi0_2;7_aoi0_3;7_aoi2_3");
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTimeOnAoiPerScreen(6, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(6, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(6, ex,0)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(6, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(6, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(6, ex,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(6, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(6, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(6, ex,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTimeOnAoiPerScreen(6, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getAvarageTimeOfFixationsInAoiPerScreen(6, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getFixationNumberInAoiPerScreen(6, ex,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(6, ex,0,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(6, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(6, ex,0,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(6, ex,2,3)).replace(".", ","));
//				}
//			} 
//		}
//		
//		if (settings.equals("Screen2_du")){
//			/*
//			writer.println("Testperson;Setup;2_aoi0_2;2_aoi0_3;2_aoi0_4;2_aoi1_2;2_aoi1_3;2_aoi1_4");
//			
//			//hirn
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(1, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(1, ex,0,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(1, ex,0,4)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(1, ex,1,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(1, ex,1,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(1, ex,1,4)).replace(".", ","));
//				}
//			}
//			*/
//			// wetter
//			writer.println("Testperson;Setup;2_aoi0_1");
//						
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(1, ex,0,1)).replace(".", ","));
//				}
//			}
//			
//		}
//		
//		if (settings.equals("Screen3_du")){
//			/*
//			writer.println("Testperson;Setup;3_aoi0_1");
//			
//			//hirn
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(2, ex,0,1)).replace(".", ","));
//				}
//			}
//			*/
//			// wetter
//			writer.println("Testperson;Setup;3_aoi0_1;3_aoi0_2;3_aoi0_3");
//			
//			
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(2, ex,0,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(2, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(2, ex,0,3)).replace(".", ","));
//				}
//			}
//			
//		}
//		
//		if (settings.equals("Screen4_du")){
//			
//			/*
//			writer.println("Testperson;Setup;4_aoi0_2;4_aoi0_3;4_aoi0_4;4_aoi1_2;4_aoi1_3;4_aoi1_4");
//			
//			//hirn
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(3, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex,0,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex,0,4)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex,1,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex,1,3)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex,1,4)).replace(".", ","));
//				}
//			}
//			*/
//			// wetter
//			writer.println("Testperson;Setup;4_aoi0_1");
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(3, ex,0,1)).replace(".", ","));
//				}
//			}
//			
//		}
//		if (settings.equals("Screen5_du")){
//			
//			// wetter
//			writer.println("Testperson;Setup;5_aoi0_1;5_aoi0_2;5_aoi0_3");
//			
//			
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(4, ex,0,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(4, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(4, ex,0,3)).replace(".", ","));
//				}
//			}
//			
//		}
//		
//		if (settings.equals("Screen6_du")){
//			/*
//			writer.println("Testperson;Setup;6_aoi0_1");
//			
//			//hirn
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(5, ex,0,1)).replace(".", ","));
//				}
//			}
//			*/
//			// wetter
//			writer.println("Testperson;Setup;6_aoi0_1");
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(5, ex,0,1)).replace(".", ","));
//				}
//			}
//			
//		}
//		
//		if (settings.equals("Screen7_du")){
//			
//			
//			// wetter
//			writer.println("Testperson;Setup;7_aoi0_1;7_aoi0_2;7_aoi0_3");
//			
//			
//			
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(6, ex,0,1)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(6, ex,0,2)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(6, ex,0,3)).replace(".", ","));
//				}
//			}
//			
//		}
//		/*
//		if (settings.equals("Transitions_per_screen")){
//	
//			writer.println("Versuchsperson;Setup;Transitionen Screen2;Transitionen Screen3;Transitionen Screen4;Transitionen Screen5;Transitionen Screen6;Transitionen Screen7");
//		
//			for (Experiment ex : study.getExperiments()){
//				
//				if (ex.isEnabled()){
//				
//					writer.println(ex.getName() + ";" + "2"+ ";" + String.valueOf(stats.getTransitionsPerScreen(1, ex)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(2, ex)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(3, ex)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(4, ex)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(5, ex)).replace(".", ",")+ ";"
//																 + String.valueOf(stats.getTransitionsPerScreen(6, ex)).replace(".", ","));
//				}
//			}
//		}*/
//		writer.close();
//		
//	}
	
}
