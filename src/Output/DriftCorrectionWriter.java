package Output;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import Data.Subject.Experiment;
import Data.Subject.Screen;
import Data.Subject.Trial;



public class DriftCorrectionWriter {

	
	public void writeDriftFile(Experiment ex) throws FileNotFoundException, UnsupportedEncodingException{
		
		File f = new File(ex.getPath().replace(".asc", "")+"_drift.txt");
		
		System.out.println(f.getAbsolutePath());
		
		if(f.exists())
			f.delete();
			
		PrintWriter writer = new PrintWriter(ex.getPath().replace(".asc", "")+"_drift.txt", "UTF-8");
		//writer.println("Quality " + ex.getQuality().toString());
		writer.println("Trial | Screen | xDrift | yDrift");
		
		int trialCounter = 0;
		
		
		for (Trial t: ex.getTrials()){
			
			int screenCounter = 0;
			for (Screen s: t.getScreens()){
			
				writer.println(trialCounter + " " + screenCounter + " " + s.getDriftCorrection().getDriftX() + " " + s.getDriftCorrection().getDriftY());
				++screenCounter;
			}
			++trialCounter;
		}
		writer.close();
	}


}
