**OpenEyes** is a tool for visualizing and analyzing eye movement data from the EyeLink 2 eye tracker. 

Replay of recorded eye tracking data from the [Antagonists project](https://www.researchgate.net/publication/291165924_Antagonists_for_Visuo-Spatial_Games)  
![oe_hex.PNG](https://bitbucket.org/repo/MaEE84/images/213649763-oe_hex.PNG)

Scan path visualization from the [eLearning experiment](https://www.researchgate.net/publication/283582526_Does_Personalisation_Promote_Learners%27_Attention_An_Eye-Tracking_Study)
![oe_elearning_fixations.PNG](https://bitbucket.org/repo/MaEE84/images/3503659944-oe_elearning_fixations.PNG)

Heatmap visualization from my [Master's Thesis](https://www.researchgate.net/publication/267332421_Can_Eye_Movements_Reveal_Cognitive_Load_Parallel_Coordinates_-_Establishing_a_Cognitive_Load_Measure_for_Information_Visualizations)
![oe_pc_heatmap.PNG](https://bitbucket.org/repo/MaEE84/images/3105783199-oe_pc_heatmap.PNG)

Contact: stefanie.wetzel@gmail.com